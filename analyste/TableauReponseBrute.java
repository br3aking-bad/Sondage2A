import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;


/**
 * @author elleuch
 *
 */
public class TableauReponseBrute extends JTable {
	//Le this
	//	  public  JTable this;
	//Les titres des colonnes

	private TableRowSorter<TableModelQuestion> sorter;
	ArrayList<Object[]> data;
	
	TableModelQuestion model;
	//	  private ComboRenderer combo;
boolean triCategorie;
	String[]  title ;


	//2 bouton dans le this
	JButton supp=new JButton("");
	JButton modif=new JButton("");






	public TableauReponseBrute(){


		triCategorie =true;
	
		//	    JComboBox comboBoxType =new  JComboBox(comboDataTri);


		//Données de notre this
		//	    Object[][] data = {
		//	      {"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	      {"2", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	    };
		data=new ArrayList<Object[]>();
		//	    Object[] a={"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)};
		//	    data.add(a);

		//Les titres des colonnes
		String[] str = {"Num Q","Intitule", "Type","Tri","Mise en forme","Etat"};
	
		
		// modèle d'affichage
		model = new TableModelQuestion(data, title);
		this.setModel(model);
		//	    this = new JTable(model);




		TableColumn col = this.getColumnModel().getColumn(0);
		TableColumn col2 = this.getColumnModel().getColumn(2);
		TableColumn col6 = this.getColumnModel().getColumn(1);
		col6.setPreferredWidth(280);
		col.setPreferredWidth(120);
		col2.setPreferredWidth(100);

		this.setBackground(new Color(230, 230, 230));
		this.setBorder(BorderFactory.createLoweredBevelBorder());




		// triage

		sorter = new TableRowSorter<TableModelQuestion>(this.model);
		this.setRowSorter(sorter);
		this.setPreferredScrollableViewportSize(new Dimension(500, 70));
		this.setFillsViewportHeight(true);


		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setColumnSelectionAllowed(true);
		this.setLayout(new GridLayout(0, 1, 0, 0));



	}

	public TableRowSorter<TableModelQuestion> getSorter(){
		return sorter;
	}
	
	
	public void changerTri(){
		if(triCategorie){
			triCategorie=false;
			
			
		}
		else{
			
			triCategorie=true;
		}
	}
	


}
