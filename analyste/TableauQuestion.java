import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;


/**
 * @author elleuch
 *
 */
public class TableauQuestion extends JTable {
	//Le this
	//	  public  JTable this;
	//Les titres des colonnes
	private String[] comboDataForme = {"Anneaux","Barres","Barres 3D","Camembert","Camembert 3D","Lignes","Lignes 3D","Aire"};
	private String[] comboDataTri = {"Age", "P.C.S"};
	//	  private String[] comboDataType = {"par", "parr"};
	private TableRowSorter<TableModelQuestion> sorter;
	ArrayList<Object[]> data;
	ArrayList<Question> listeQuestion;
	Question lastQSelect;
ApplicationAnalyste jframe;
	Integer lastIndexQselect;




	TableModelQuestion model;
	//	  private ComboRenderer combo;




	//2 bouton dans le this
	JButton supp=new JButton("");
	JButton modif=new JButton("");






	/**
	 * @param app
	 * 
	 */
	public TableauQuestion(ApplicationAnalyste app){
		lastQSelect=null;
		lastIndexQselect=null;
		listeQuestion= new ArrayList<Question>();
this.jframe=app;

		JComboBox comboBoxForme =new  JComboBox(comboDataForme);
		JComboBox comboBoxTri =new  JComboBox(comboDataTri);
		comboBoxForme.addItemListener(new ComboBoxChangeListenerForme(jframe));
		comboBoxTri.addItemListener(new ComboBoxChangeListenerTri(jframe));

//		comboBoxTri.addFocusListener(new ComboBoxChangeListenerTri(jframe));

		//	    JComboBox comboBoxType =new  JComboBox(comboDataTri);


		//Données de notre this
		//	    Object[][] data = {
		//	      {"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	      {"2", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	    };
		data=new ArrayList<Object[]>();
		//	    Object[] a={"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)};
		//	    data.add(a);

		//Les titres des colonnes
		String  title[] = {"Num","Intitule", "Type","Tri","Mise en forme","Validee"};

		// modèle d'affichage
		model = new TableModelQuestion(data, title);
		this.setModel(model);
		//	    this = new JTable(model);




//		TableColumn col = this.getColumnModel().getColumn(0);
//		TableColumn col2 = this.getColumnModel().getColumn(2);
//		TableColumn col6 = this.getColumnModel().getColumn(1);
		TableColumn num=this.getColumn("Num");

		TableColumn etat=this.getColumn("Validee");
		TableColumn tri=this.getColumn("Tri");


//		TableColumn col6 = this.getColumnModel().getColumn(5);
		

		etat.setMaxWidth(50);
		tri.setMaxWidth(70);
		num.setPreferredWidth(50);
		this.setRowHeight(25);
//this.resizeColumnWidth(this);
//		col6.setPreferredWidth(200);
//		col.setPreferredWidth(120);
//		col2.setPreferredWidth(100);

		this.setBackground(new Color(230, 230, 230));
		this.setBorder(BorderFactory.createLoweredBevelBorder());




		// triage

//		sorter = new TableRowSorter<TableModelQuestion>(this.model);
//		this.setRowSorter(sorter);
//		this.setPreferredScrollableViewportSize(new Dimension(500, 70));
		this.setFillsViewportHeight(true);


		// définire l'éditeur par défaut pour la cellule
		//en lui spécifiant quel type d'affichage 
		//	  this.getColumn("Mod").setCellRenderer(new ButtonRenderer());

		//	  this.getColumn("Sup").setCellRenderer(new ButtonRenderer());
		//	  this.getColumn("Sup").setCellEditor(new DeleteButtonEditor(new JCheckBox()));


		//cell renderer
		//	    this.getColumn("Type").setCellRenderer(new triComboListRenderer());
		this.getColumn("Tri").setCellRenderer( new DefaultTableCellRenderer());
		this.getColumn("Mise en forme").setCellRenderer(new MyComboBoxRenderer(comboDataForme));

		//cell editor 
		this.getColumn("Mise en forme").setCellEditor(new DefaultCellEditor(comboBoxForme));
		//	    this.getColumn("Type").setCellEditor(new DefaultCellEditor(comboBoxType));
		this.getColumn("Tri").setCellEditor(new DefaultCellEditor(comboBoxTri));
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setToolTipText("Cliquer pour un trie different des donnees brutes");
		this.getColumn("Tri").setCellRenderer(new MyComboBoxRenderer(comboDataTri));

		//	    this.setRowHeight(20);

		//		this.getColumnModel().getColumn(2).setPreferredWidth(200);
		//		this.getColumnModel().getColumn(2).setMaxWidth(200);
		//		this.getColumnModel().getColumn(3).setPreferredWidth(300);
		//		this.getColumnModel().getColumn(3).setMaxWidth(500);
		//	    
		// JButton ajouter = new JButton("Ajouter une ligne");
		// ajouter.addActionListener(new MoreListener());
		// this.add(ajouter, BorderLayout.SOUTH);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setColumnSelectionAllowed(false);
		//		this.setBounds(100, 100, 450, 300);
//		this.setLayout(new GridLayout(0, 1, 0, 0));


		//		this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);


		//		this.add(this);
		//		this.setPreferredSize(new Dimension(40, 0));
	}

	public TableRowSorter<TableModelQuestion> getSorter(){
		return sorter;
	}
	public void addQuestion(Question q) {
		Object[] a={Integer.toString(q.getNumQ()),q.getTexteQ(),q.getTypeQuestion().getIntituleT(),comboDataTri[0],comboDataForme[0],new Boolean(false)};
		//		    System.out.println(q.getPanel());
		data.add(a);
	}


	public void addQuestion(List<Question> liste){
		for(Question q:liste){
			this.addQuestion(q);
			listeQuestion.add(q);
		}
		if(listeQuestion.size()>0)
			lastQSelect=listeQuestion.get(0);

	}


public ArrayList<Question>  getQuestionValidee(){
//	System.out.println("début question validee");
	ArrayList<Question> res=new ArrayList<Question>();
	System.out.println(listeQuestion.size());
	for(int i=0;i<listeQuestion.size();i++){
		if(this.getValueAt(i, 5).equals(true)){
			res.add(listeQuestion.get(i));	
		}	
	}
	return res;
}
//
//
//public void resizeColumnWidth(JTable table) {
//    final TableColumnModel columnModel = table.getColumnModel();
//    for (int column = 0; column < table.getColumnCount(); column++) {
//        int width = 50; // Min width
//        for (int row = 0; row < table.getRowCount(); row++) {
//            TableCellRenderer renderer = table.getCellRenderer(row, column);
//            Component comp = table.prepareRenderer(renderer, row, column);
//            width = Math.max(comp.getPreferredSize().width, width);
//        }
//        columnModel.getColumn(column).setPreferredWidth(width);
//    }
//}

}
