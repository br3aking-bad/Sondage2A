import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

	  public class triComboListRenderer extends JComboBox implements TableCellRenderer {

		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean isFocus, int row, int col) {
		    this.addItem("classe d’âge");
		    this.addItem("catégorie socio-professionnelle");
		  
		    return this;
		  }
		}
