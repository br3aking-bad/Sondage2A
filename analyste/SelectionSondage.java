/*
 * 
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * @author elleuch
 *
 */
public class SelectionSondage  extends JPanel{
	
	ApplicationAnalyste jframe;
	// les données contenues dans la liste
	DefaultListModel<String> lesOperations;
	// la JList elle même
	JList<String> listeOperations;
	
	//bouton bAcceder
	JButton bAcceder;
	JButton bDeconnection;
	
	JLabel jlClient;
	JLabel jlPanel;
	
	
     JTextField filterText;
    JTextField statusText;
	
	TableauSondage tableau;

	
 //rajouter Bouton mettre ne pdf
	
	public SelectionSondage(ApplicationAnalyste jframe) {
		this.jframe=jframe;
		this.setLayout(new BorderLayout());
		
		
		
		//Liste sondage
//		lesOperations=new DefaultListModel<String>();
//		lesOperations.addElement("aucun sondee");
//		
//		listeOperations=new JList<String>(lesOperations);
//		this.listeOperations.setLayoutOrientation(this.listeOperations.VERTICAL_WRAP);
//		
//		
//		JScrollPane scrollPane=new JScrollPane(this.listeOperations);
//		scrollPane.setPreferredSize(new Dimension(50,200));
//		scrollPane.setBorder(new TitledBorder ("Operations Sauvegardardes "));
//		
		
		
		//Jpanel pour selection sondage
		JPanel panelSondage=new JPanel();
//		panelSondage.setLayout(new BorderLayout());


		
		
		//Ajout scrollpane dans un jpanel
//		JPanel panelScrolPane=new JPanel();
//		panelScrolPane.setLayout(new BorderLayout());
//		panelScrolPane.add(scrollPane);
		
//		ajout panel au panel Sondage
//		panelSondage.add(panelScrolPane);
//		JButton bEffacer=new JButton("effacer");
//		this.add(bEffacer);
		
		
		
		//ajout JLabel panel et client dans le panel sondage
		JPanel panelLabel=new JPanel();
        SpringLayout layout = new SpringLayout();
//	    BoxLayout layout = new BoxLayout(panelLabel, BoxLayout.Y_AXIS);

//		panelLabel.setLayout(layout);


		jlPanel = new JLabel("Panel : XXXXXXX");
		jlClient = new JLabel("Client : XXXXXXX");
		
//		layout.putConstraint(SpringLayout.NORTH, jlClient,
//                5,
//                SpringLayout.NORTH, panelSondage);
		
		panelLabel.add(jlPanel);
		panelLabel.add(jlClient);
		
		
		panelSondage.add(panelLabel,BorderLayout.EAST);


		
	// ajout du tableau
		
		
		panelSondage.setLayout(new BorderLayout());
		this.jframe=jframe;
		this.tableau=new TableauSondage();
		this.setQuestionnaires(jframe.accesBD.getQuestionnaires('A'));
		if(tableau.listeQuestionnaire.size()>0)
			this.tableau.changeSelection(0, 0, false, false);
				
		JScrollPane jscroll=new JScrollPane(tableau);
		panelSondage.add(jscroll,BorderLayout.CENTER);
		tableau.setFillsViewportHeight(true);
		
		

        

        JPanel form = new JPanel(new SpringLayout());
        JLabel l1 = new JLabel("Filtrer par numero de sondage :", SwingConstants.TRAILING);
        form.add(l1);
        filterText = new JTextField();
        //Whenever filterText changes, invoke newFilter.
        filterText.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }
                });
        l1.setLabelFor(filterText);
        form.add(filterText);
        JLabel l2 = new JLabel("Sondage selectionne", SwingConstants.TRAILING);
//        l2.setVisible(false);
        form.add(l2);
        statusText = new JTextField();
//        statusText.setVisible(false);

        l2.setLabelFor(statusText);
        form.add(statusText);
        SpringUtilities.makeCompactGrid(form, 2, 2, 6, 6, 6, 6);
        add(form,BorderLayout.SOUTH);
        
        this.setOpaque(true);
		
		
		
		
		
		
        tableau.getSelectionModel().addListSelectionListener(new ListenerSelectionSondage(this));
		
		
		
		
		
		
		
		//ajout bouton "acceder au question en bas de la frame
		
			//encapsulation jpanel
		JPanel panelAcceder=new JPanel();
		panelAcceder.setLayout(new BorderLayout());
		bAcceder=new JButton("Acceder aux questions");
		bDeconnection=new JButton("Deconnexion");
		bDeconnection.addActionListener(new CloseListener());
		bAcceder.addActionListener(new ActionBoutonQuestion(jframe,this));
		panelAcceder.add(bAcceder);
		panelAcceder.add(bDeconnection,BorderLayout.EAST);
		this.add(panelAcceder,BorderLayout.NORTH); // bouton acc�der question
		
		this.add(panelSondage);
		
		
		
		
		
	
		
		
		
		
		
		
		
		
		
	}

    private void newFilter() {
        RowFilter<TableModelQuestion, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(filterText.getText(), 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        tableau.getSorter().setRowFilter(rf);
    }
    
    public void setQuestionnaires(List<Questionnaire> liste){
    	for(Questionnaire q:liste){
    		tableau.addQuestionnaire(q);
    	}
    	
    }

}