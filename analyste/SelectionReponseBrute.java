import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableRowSorter;




/**
 * @author elleuch
 *
 */
public class SelectionReponseBrute  extends JPanel{
	
	ApplicationAnalyste jframe;
//    private JTable tableau;
    private JTextField filterText;
    private JTextField statusText;
	
	TableauQuestion tableau;

	
	
	
	public SelectionReponseBrute(ApplicationAnalyste jframe) {
		this.setLayout(new BorderLayout());
		this.jframe=jframe;
		this.tableau=new TableauQuestion(jframe);
		
		
		
		JScrollPane jscroll=new JScrollPane(tableau);
		this.add(jscroll,BorderLayout.CENTER);
		tableau.setFillsViewportHeight(true);
		
		
		
        

        JPanel form = new JPanel(new SpringLayout());
        JLabel l1 = new JLabel("Filtrer par question :", SwingConstants.TRAILING);
        form.add(l1);
        filterText = new JTextField();
        //Whenever filterText changes, invoke newFilter.
        filterText.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }
                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }
                });
        l1.setLabelFor(filterText);
        form.add(filterText);
        JLabel l2 = new JLabel("Question selectionee :", SwingConstants.TRAILING);
//        l2.setVisible(false);
        form.add(l2);
        statusText = new JTextField();
//        statusText.setVisible(false);

        l2.setLabelFor(statusText);
        form.add(statusText);
        SpringUtilities.makeCompactGrid(form, 2, 2, 6, 6, 6, 6);
        add(form,BorderLayout.SOUTH);
        
        this.setOpaque(true);
        
        tableau.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent event) {
                        int viewRow = tableau.getSelectedRow();
                        if (viewRow < 0) {
                            //Selection got filtered away.
                            statusText.setText("");
                        } else {
                            int modelRow = 
                                tableau.convertRowIndexToModel(viewRow);
                            statusText.setText((String) tableau.getValueAt(viewRow,0));
//                                  viewRow, modelRow));
                            		
                            		
                            		
//                                String.format("Selected Row in view: %d. " +
//                                    "Selected Row in model: %d.", 
//                                    viewRow, modelRow));
                        }
                    }
                }
        );

        

        
    }

    public void setQuestion(List<Question> liste ){
    	for(Question q:liste){
    		tableau.addQuestion(q);
    	}
    	
    }
    /** 
     * Update the row filter regular expression from the expression in
     * the text box.
     */
    private void newFilter() {
        RowFilter<TableModelQuestion, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(filterText.getText(), 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        tableau.getSorter().setRowFilter(rf);
    }
		
		
    
    
    
    

    
    
    
	}




