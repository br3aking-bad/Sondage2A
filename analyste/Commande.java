
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.itextpdf.text.Image;


// TODO: Auto-generated Javadoc
/**
 * La Class Commande permet de crée un JPanel contenant un jTextArea pour entrer des commentaires,
 * de retourner à la selection des sondages, et de générer un pdf de l'analyse.
 * 
 * @author Elleuch
 * 
 */
public class Commande extends JPanel {
	
	JButton annulerSortir;
	
	JButton genererPDF;
	
	private ApplicationAnalyste jframe;
	
	JTextArea commentaire;
	
	JPanel panelCommentaire;
	
	JScrollPane scroll;
	
	ArrayList<JTextArea> listeTextArea;
	
	Integer QuestEnCour;

	/**
	 * Instancie une boite de commande
	 *
	 * @param jframe the jframe
	 */
	Commande(ApplicationAnalyste jframe){
		QuestEnCour=null;
		
		
		listeTextArea=new ArrayList<JTextArea>();
		
		this.jframe=jframe;	
		annulerSortir=new JButton("Retour");
		annulerSortir.addActionListener(new ActionBoutonRetournerSondage(jframe));
		

//		
//		genererPDF=new JButton(pdfIcon);
//		
//genererPDF.setSize(new Dimension(5,5));


		genererPDF=new JButton("generer un PDF");
		genererPDF.addActionListener(new ActionBoutonGenererPDF(jframe));


		panelCommentaire=new JPanel();
		panelCommentaire.setBorder(new TitledBorder(new EtchedBorder(), "Saisie commentaire"));

		// create the middle panel components

		commentaire = new JTextArea(16, 45);
//		commentaire.setEditable(false); // set textArea non-editable
		scroll = new JScrollPane(commentaire);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		//Add Textarea in to middle panel
		panelCommentaire.add(scroll);

		

		//panel bouton
		JPanel bouton=new JPanel();
		
		this.setLayout(new BorderLayout());
		
		
		this.add(panelCommentaire);
		bouton.add(annulerSortir);
		bouton.add(genererPDF);
		this.add(bouton,BorderLayout.SOUTH);
		
		

	}
	
	
}
