
public class RoleUtil {

		private Character idR;
		private String nomR;
		
		/**
		 * @param idR
		 * @param nomR
		 */
		public RoleUtil(Character idR,String nomR)
		{
			this.idR=idR;
			this.nomR=nomR;
		}

		/**
		 * @return
		 */
		public Character getIdR() {
			return idR;
		}

		/**
		 * @param idR
		 */
		public void setIdR(Character idR) {
			this.idR = idR;
		}

		/**
		 * @return
		 */
		public String getNomR() {
			return nomR;
		}

		/**
		 * @param nomR
		 */
		public void setNomR(String nomR) {
			this.nomR = nomR;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "RoleUtil [idR=" + idR + ", nomR=" + nomR + "]";
		}
		
		
}
