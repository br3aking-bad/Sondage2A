import java.awt.Font;
import java.awt.print.PageFormat;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFrame;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.orsonpdf.Page;


public class Pdf {

	
	public Pdf(ApplicationAnalyste jframe) {
		try{
		Document d=new Document(PageSize.A4.rotate());
		PdfWriter.getInstance(d,new FileOutputStream("Sondage_1 Client_1.pdf"));
		d.open();
		d.addLanguage("UTF-8");
		d.addAuthor("Rapid'Sond");
		d.addCreator("Fast Probing");
		d.addCreationDate();
		d.addTitle("Sondage_1 Client_1");
		Image image=Image.getInstance("chart.jpg");
		image.setAlignment(Element.ALIGN_CENTER);
		Paragraph p =new Paragraph("Commentaires :",FontFactory.getFont(FontFactory.HELVETICA,20,Font.TRUETYPE_FONT, BaseColor.BLACK));
		Paragraph c =new Paragraph("         "+jframe.Commande.commentaire.getText(),FontFactory.getFont(FontFactory.HELVETICA,14,Font.TRUETYPE_FONT, BaseColor.BLACK));
		System.out.println(jframe.Commande.commentaire.getText());
		p.setAlignment(Element.ALIGN_LEFT);
		d.add(image);
		d.add(p);
		d.add(c);
		
		d.close();
		}
		catch(DocumentException e){System.out.println(jframe.Commande.commentaire.getText()+"1");}
		catch(MalformedURLException e){System.out.println(jframe.Commande.commentaire.getText()+"2");}
		catch(IOException e){System.out.println(jframe.Commande.commentaire.getText()+"3");}
	}
//	public void genererPDF(List<String> listeQuestion, List<String> listeCommentaires, )
//	{
//		
//	}

}
