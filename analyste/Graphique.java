import java.io.File;
import java.util.TreeMap;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
public class Graphique extends JPanel {

		String type;
		int max;
		JFreeChart chart;
	
		
		//
		
		public Graphique(ReponsesBD r,Question q, int miseenforme,boolean tri, boolean en3d){
			TreeMap<String, TreeMap<String,Integer>> donnees =r.getDicoDonnees(q, tri);
			TreeMap<String, TreeMap<String,Float>> graphique= new TreeMap<String, TreeMap<String,Float>>();
			this.type=q.getTypeQuestion().getIntituleT();
			if (type.equals("Classement"))
			{
				this.max=q.getMaxVal();
				TreeMap<String, TreeMap<String,Integer>>g= new TreeMap<String, TreeMap<String,Integer>>();
					for (String cat : donnees.keySet())
					{	TreeMap<String,Integer> di=donnees.get(cat);
						TreeMap<String,Integer> d=new TreeMap<String, Integer>();
						for (String val: di.keySet())
						{
							String[] lval=val.split(" ");
							if(d.containsKey(lval[0]))
								d.put(lval[0], d.get(lval[0])+Integer.parseInt(lval[1]));
							else
								d.put(lval[0],Integer.parseInt(lval[1]));

						}
						g.put(cat,d);
					}
			
				for (String cat : g.keySet())
				{
					TreeMap<String,Integer> di=g.get(cat);
					TreeMap<String,Float> d=new TreeMap<String, Float>();
					int somme=sommeval(di);
					for(String val : di.keySet())
					{
							d.put(val,(float)di.get(val)/somme);
					}
					graphique.put(cat, d);
				}
			}
			else
			{	
				for (String cat : donnees.keySet())
				{
					TreeMap<String,Integer> di=donnees.get(cat);
					TreeMap<String,Float> d=new TreeMap<String, Float>();
					int somme=sommeval(di);
					for(String val : di.keySet())
					{
						if(!val.equals("Total"))
						{
							d.put(val,(float)di.get(val)/somme);
						}
					}
					graphique.put(cat, d);
				}
			}
			String nomtri;
			if(tri)
				nomtri="Tranche d'âge";
			else
				nomtri="Catégorie socio-professionnelle";
			
			String Titre=q.getTexteQ();
			
			
			switch (miseenforme) {
			case 1:
				this.add(new ChartPanel(RingChart(Titre,nomtri,graphique,en3d)));
				break;
			case 2:
				this.add(new ChartPanel(BarChart3D(Titre,nomtri,graphique,en3d)));
				break;
			case 3:
				this.add(new ChartPanel(PieChart3D(Titre,nomtri,graphique,en3d)));
				
				break;
			case 4:
				this.add(new ChartPanel(LineChart3D(Titre,nomtri,graphique,en3d)));
				break;
			case 5:
				this.add(new ChartPanel(StackedAreaChart(Titre,nomtri,graphique,en3d)));
				break;
			default:
				break;
			}
			
			this.setVisible(true);
			
		}

private JFreeChart PieChart3D(String titre, String nomtri, TreeMap<String, TreeMap<String,Float>> categories, boolean en3d)
{
	DefaultPieDataset pieDataset= new DefaultPieDataset();
	for (String cat: categories.keySet())
	{	TreeMap<String,Float> d=categories.get(cat);
		for(String val : d.keySet())
		pieDataset.setValue(val,d.get(val)*100);}
	if (en3d)
		chart=ChartFactory.createPieChart3D(titre,pieDataset,true,true,false);

	else
	chart=ChartFactory.createPieChart(titre,pieDataset,true,true,false); 
	return chart;
	     
}
private JFreeChart BarChart3D(String titre, String nomtri, TreeMap<String, TreeMap<String,Float>> categories, boolean en3d)
{
	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	for (String cat: categories.keySet())
	{	TreeMap<String,Float> d=categories.get(cat);
		for(String val : d.keySet())
			dataset.setValue(d.get(val)*100,cat,val);}
	if (en3d)
	chart=ChartFactory.createBarChart3D(titre, nomtri,"Nombre de réponses (en %)",dataset,PlotOrientation.VERTICAL,true,true,false); 
	else
	chart=ChartFactory.createBarChart(titre,nomtri,"Nombre de réponses (en %)",dataset,PlotOrientation.VERTICAL,true,true,false);
	return chart;
}
private JFreeChart LineChart3D(String titre, String nomtri, TreeMap<String, TreeMap<String,Float>> categories, boolean en3d)
{
	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	for (String cat: categories.keySet())
	{	TreeMap<String,Float> d=categories.get(cat);
		for(String val : d.keySet())
			dataset.setValue(d.get(val)*100,cat,val);}
	if (en3d)
		chart=ChartFactory.createLineChart3D(titre, nomtri, "Nombre de réponses (en %)", dataset,PlotOrientation.VERTICAL,true,true,false); 
	else
		chart=ChartFactory.createLineChart(titre, nomtri, "Nombre de réponses (en %)", dataset,PlotOrientation.VERTICAL,true,true,false); 
	return chart;
}
private JFreeChart RingChart(String titre, String nomtri, TreeMap<String, TreeMap<String,Float>> categories, boolean en3d)
{
	DefaultPieDataset pieDataset= new DefaultPieDataset();
	for (String cat: categories.keySet())
	{	TreeMap<String,Float> d=categories.get(cat);
		for(String val : d.keySet())
		pieDataset.setValue(val,d.get(val)*100);}
	chart=ChartFactory.createRingChart(titre,pieDataset,true,true,false);
	return chart;
}
private JFreeChart StackedAreaChart(String titre, String nomtri, TreeMap<String, TreeMap<String,Float>> categories, boolean en3d)
{
	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	for (String cat: categories.keySet())
	{	TreeMap<String,Float> d=categories.get(cat);
		for(String val : d.keySet())
			dataset.setValue(d.get(val)*100,cat,val);}
	chart=ChartFactory.createStackedAreaChart(titre, nomtri,"Nombre de réponses (en %)",dataset,PlotOrientation.VERTICAL,true,true,false); 
	return chart;
}

public void sauveChart(String chemin, String nomFic)
{
	try {ChartUtilities.saveChartAsJPEG(new File(chemin+nomFic+".jpg"), chart, 500, 300);} 
	catch (Exception e) {System.out.println("Problem occurred creating chart.");}
	}

private int sommeval(TreeMap<String,Integer> dico)
{
	int res=0;
	for (String s:dico.keySet())
	{
		if (!s.equals("Total"))
		res+=dico.get(s);
	}
	return res;
	}




public static void main(String[] args)
{
	ConnexionMySQL c=new ConnexionMySQL("servinfo-db", "dbgfroger", "dbgfroger", "/home/gfroger/");
	AccesBD a=new AccesBD(c);
	ReponsesBD r=new ReponsesBD(c);
	Graphique g=new Graphique(r,a.getQuestion(1, 1),2,false,false);
}

}
