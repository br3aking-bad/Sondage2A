import java.util.ArrayList;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

class TableModelDonneeBrute extends AbstractTableModel{
	  
//	   private Object[][] data;
	   private String[] title;
	   
	   ArrayList<Object[]> data;
	    
	   //Constructeur
//	   public TableModelQuestion(Object[][] data, String[] title){
//	      this.data = data;
//	      this.title = title;
//	   }
	   public TableModelDonneeBrute(ArrayList<Object[]> data2, String[] title2) {
		// TODO Auto-generated constructor stub
		   this.data = data2;
		      this.title = title2;
	}

	   
	   
	   
		public TableModelDonneeBrute(String[] title) {
			// TODO Auto-generated constructor stub

			   this.data = new 	   ArrayList<Object[]>();
			      this.title = title;
		}
		
		   
				public TableModelDonneeBrute(Set<String> myset) {
					// TODO Auto-generated constructor stub

					   this.data = new ArrayList<Object[]>();
					      this.title = myset.toArray(new String[myset.size()]);
				}

	//Retourne le titre de la colonne à l'indice spécifié
	   public String getColumnName(int col) {
	     return this.title[col];
	   }
	 
	   //Retourne le nombre de colonnes
	   public int getColumnCount() {
	      return this.title.length;
	   }
	    
	   //Retourne le nombre de lignes
	   public int getRowCount() {
	      return this.data.size();
	   }
	    
	   //Retourne la valeur à l'emplacement spécifié
	   public Object getValueAt(int row, int col) {
	      return this.data.get(row)[col];
	   }
	    
	   //Définit la valeur à l'emplacement spécifié
	   public void setValueAt(Object value, int row, int col) {
	      //On interdit la modification sur certaines colonnes !
//	      if(!this.getColumnName(col).equals("Age")
//	         && !this.getColumnName(col).equals("Suppression"))
	         this.data.get(row)[col] = value;
	   }
	          
	  //Retourne la classe de la donnée de la colonne
	   public Class getColumnClass(int col){
	      //On retourne le type de la cellule à la colonne demandée
	      //On se moque de la ligne puisque les données sont les mêmes
	      //On choisit donc la première ligne
	      return this.data.get(0)[col].getClass();
	   }
	 
	   //Méthode permettant de retirer une ligne du tableau
	   public void removeRow(int position){
	       
//	      int indice = 0, indice2 = 0;
//	     int  nbRow = this.getRowCount()-1, nbCol = this.getColumnCount();
//	      Object temp[][] = new Object[nbRow][nbCol];
//	       
//	      for(Object[] value : this.data){
//	         if(indice != position){
//	            temp[indice2++] = value;		model = new TableModelDonneeBrute(data, title);

//	         }
//	         System.out.println("Indice = " + indice);
//	         indice++;
//	      }
//	      this.data = temp;
//	      temp = null;
	     this.data.remove(position);
	      //Cette méthode permet d'avertir le tableau que les données
	      //ont été modifiées, ce qui permet une mise à jour complète du tableau
	      this.fireTableDataChanged();
	   }
	    
	   //Permet d'ajouter une ligne dans le tableau
	   public void addRow(Object[] leData){
	      int indice = 0, nbRow = this.getRowCount(), nbCol = this.getColumnCount();
	       
//	      Object temp[][] = this.data;
//	      this.data = new Object[nbRow+1][nbCol];
//	       
//	      for(Object[] value : temp)
//	         this.data[indice++] = value;
//	       
//	          
//	      this.data[indice] = data;
//	      temp = null;
	      this.data.add(leData);
	      //Cette méthode permet d'avertir le tableau que les données
	      //ont été modifiées, ce qui permet une mise à jour complète du tableau
	      this.fireTableDataChanged();
	   }
	    
	    
	   public boolean isCellEditable(int row, int col){
		   switch (col)
		   {
		     case 0:
		       return false;
		     case 1:
			       return false;
		     case 2:
			       return false;
//		     case 5:
//			       return false;

		     default:
		    	 return true;
		   } 
		 
	   }
	   
}