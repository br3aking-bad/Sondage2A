import java.sql.*;
import java.util.List;
import java.util.TreeMap;


public class ReponsesBD {

	private Statement statement;
	private Connection c;
	private AccesBD a;
	public ReponsesBD(ConnexionMySQL cMySQL)

	{
		try{
			statement=cMySQL.getConnection().createStatement();
			this.c=cMySQL.getConnection();
			this.a= new AccesBD(cMySQL);}
		catch(SQLException e){}
	}
	    
		public TreeMap<String,TreeMap<String,Integer>> getDicoDonnees(Question q, boolean tri)// true = tri age
		{
			TreeMap<String,TreeMap<String,Integer>> dicofirst;
			TreeMap<String,TreeMap<String,Integer>> dicodonnees=new TreeMap<String, TreeMap<String,Integer>>();
			String type=q.getTypeQuestion().getIntituleT();
			if (tri)
				dicofirst=nbReponsesParTranche(q); 
			else
				dicofirst=nbReponsesParCat(q);

			
			if (type.equals("Réponse libre") || type.equals("Note"))
				return dicofirst;
			else
			{
				List<ValPossible> valpossibles=a.getValPossibles(q);
				TreeMap<Integer,String> dico3=new TreeMap<Integer,String>();
				
				for (ValPossible v : valpossibles)
					{dico3.put(v.getIdV(),v.getValeur());}
				
				if(type.equals("Choix unique"))
				{ 
					for (String cat : dicofirst.keySet())
					{	TreeMap<String,Integer> di=dicofirst.get(cat);
						TreeMap<String,Integer> d=new TreeMap<String, Integer>();
						for (String val: di.keySet())
						{	if (val.equals("Total"))
								d.put("Total", di.get(val));
							else{
								try{
								if(dico3.containsKey(Integer.parseInt(val)))
									d.put(dico3.get(Integer.parseInt(val)), di.get(val));
								}
								catch(NumberFormatException e){
									
								}
							}
						}
						dicodonnees.put(cat,d);
				}
					
					return dicodonnees;
					}
				
				else if(type.equals("Classement"))
				{
					for (String cat : dicofirst.keySet())
					{	TreeMap<String,Integer> di=dicofirst.get(cat);
						TreeMap<String,Integer> d=new TreeMap<String, Integer>();

						for (String val: di.keySet())
						{if (!val.equals("Total"))
						{	String[] lval=val.split(";");
						for (int i=0;i<lval.length;i++)
						{	String st="";
							if(lval[i].charAt(0)==' ' && lval[i].length()>=2)
								st=dico3.get(Integer.parseInt(lval[i].charAt(1)+""))+" "+i+1;
							else{
								try{
								st=dico3.get(Integer.parseInt(lval[i]))+" "+i+1;
								}
								catch(NumberFormatException e){
									
								}
								}
							
							if(d.containsKey(st))
								d.put(st, d.get(st)+di.get(val));
							else
								d.put(st, di.get(val));
							
						}}}
						dicodonnees.put(cat,d);
					}
					return dicodonnees;
				}
				else
				{
					for (String cat : dicofirst.keySet())
					{	TreeMap<String,Integer> di=dicofirst.get(cat);
						TreeMap<String,Integer> d=new TreeMap<String, Integer>();
						for (String val: di.keySet())
						{if (!val.equals("Total"))
						{String[] lval=val.split(";");
						for (int i=0;i<lval.length;i++)
						{	String st=dico3.get(Integer.parseInt(lval[i]));
							if(d.containsKey(st))
								d.put(st, d.get(st)+di.get(val));
							else
								d.put(st, di.get(val));
						}}}
						dicodonnees.put(cat,d);
					}
					return dicodonnees;
				}	
			}
		}
			
			
	
	    public TreeMap<String,TreeMap<String,Integer>> nbReponsesParCat(Question q){
	    	
	        TreeMap<String,TreeMap<String,Integer>> dico =new TreeMap<String,TreeMap<String,Integer>>();
	        
	    	try {
	                
	            PreparedStatement ps = c.prepareStatement("select intituleCat,valeur "+
	                    "from REPONDRE natural join CARACTERISTIQUE natural join CATEGORIE "+
	                    "where idQ=? and numQ=? order by idCat, intituleCat, valeur");
	            ps.setInt(1,q.getQuestionnaire().getIdQ());
	            ps.setInt(2,q.getNumQ());
	            ResultSet rs=ps.executeQuery();

	            String intitulePrec="";
	            String valeurPrec="";
	            TreeMap<String,Integer> Prec=new TreeMap<String,Integer>();

	            int cptVal=0;
	            int cptCat=0;
	                    
	            while (rs.next()){
	                String intituleCour=rs.getString(1); 
	                String valeurCour=rs.getString(2);
		            TreeMap<String,Integer> Cour=new TreeMap<String,Integer>();

	                if (intitulePrec.compareTo(intituleCour)!=0){ 
	                    if (intitulePrec!=""){
	                        Prec.put(valeurPrec, cptVal); 
	                        cptCat+=cptVal;  
	                        Prec.put("Total", cptCat); 
	                    }
	                    cptVal=0; 
	                    cptCat=0; 
	                    intitulePrec=intituleCour;
	                    Prec=Cour;
	                    valeurPrec=valeurCour;
	                    dico.put(intituleCour,Cour);
	                    
	                }
	                if (valeurPrec.compareTo(valeurCour)!=0){ 
	                    Prec.put(valeurPrec, cptVal);
	                    cptCat+=cptVal; 
	                    cptVal=0; 
	                    valeurPrec=valeurCour; 
	                }
	                cptVal+=1; 
	            }
	            if (intitulePrec!=""){ 
	                Prec.put(valeurPrec, cptVal);
	                Prec.put("Total", cptVal);
	                
	            }
	            rs.close();
	            return dico;}
	         catch (SQLException e)
	            {System.out.println("Voici le message SQL:"+e.getMessage());return dico;}
	        
	    }
	    
	    public TreeMap<String,TreeMap<String,Integer>> nbReponsesParTranche(Question q){
	    	
	    	TreeMap<String,TreeMap<String,Integer>> dico =new TreeMap<String,TreeMap<String,Integer>>();
	        
	    	try {
	                
	            PreparedStatement ps = c.prepareStatement("select valDebut,valFin,valeur "+
	                    "from REPONDRE natural join CARACTERISTIQUE natural join TRANCHE "+
	                    "where idQ=? and numQ=? order by idTr, valDebut, valeur");
	            ps.setInt(1,q.getQuestionnaire().getIdQ());
	            ps.setInt(2,q.getNumQ());
	            ResultSet rs=ps.executeQuery();

	            Integer valdebutPrec=-1;
	            Integer valfinPrec=-1;
	            String valeurPrec="";
	            TreeMap<String,Integer> Prec=new TreeMap<String,Integer>();
	            int cptVal=0;
	            int cptCat=0;
	                    
	            while (rs.next()){
		            Integer valdebutCour=rs.getInt(1);
		            Integer valfinCour=rs.getInt(2); 
	                String valeurCour=rs.getString(3);
	                TreeMap<String,Integer> Cour=new TreeMap<String, Integer>();
	                
	                if (valdebutPrec.compareTo(valdebutCour)!=0){ 
	                    if (valdebutPrec!=-1){ 
	                        
	                        Prec.put(valeurPrec, cptVal);
	                        cptCat+=cptVal; 
	                        Prec.put("Total", cptCat);
	                    }
	                    cptVal=0; 
	                    cptCat=0; 
	                    valdebutPrec=valdebutCour;
	                    valfinPrec=valfinCour;
	                    valeurPrec=valeurCour;
	                    Prec=Cour;
	                    dico.put("["+valdebutCour+"-"+valfinCour+"ans]", Cour);
	                    
	                }
	                if (valeurPrec.compareTo(valeurCour)!=0){ 
	                    Prec.put(valeurPrec, cptVal);
	                    cptCat+=cptVal; 
	                    cptVal=0; 
	                    valeurPrec=valeurCour; 
	                }
	                cptVal+=1;
	            }
	            if (valdebutPrec!=-1){ 
	                Prec.put(valeurPrec, cptVal);
	                Prec.put("Total", cptCat);
	                
	            }
	            rs.close();
	            return dico;}
	         catch (SQLException e)
	            {System.out.println("Voici le message SQL:"+e.getMessage());return dico;}
	        
	    }    
	    
}

