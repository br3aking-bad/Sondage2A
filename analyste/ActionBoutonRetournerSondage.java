import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;



// TODO: Auto-generated Javadoc
/**
 * La Class ActionBoutonRetournerSondage est un actionListener qui permet de retourner à la selection des sondages.
 */
public class ActionBoutonRetournerSondage implements ActionListener {

	/** Jframe de l'application.
	 *  @see ApplicationAnalyste */
ApplicationAnalyste jframe;


	/**
 * La Class ActionBoutonRetournerSondage permet de détecter un clic sur un bouton
 * pour changer de vue, permettant de passer du plan de travail principal
  * au panel de séleciton des sondages.
 *  .
	 *@autor elleuch
	 * @param jframe La jframe principale de l'application crée avec la class ApplicationAnalyste
	 * @see ApplicationAnalyste
	 */
	public ActionBoutonRetournerSondage(ApplicationAnalyste jframe) {
		
		this.jframe=jframe;

	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//Message de confirmation qui pop, demandant à l'utilisateur
		//si il est sur de vouloir retourner dans la selections des sondages
		Object[] options = {"Oui",
				"Non, je veux rester sur cette page",};
		int n=JOptionPane.showOptionDialog(jframe,"Retourner à la selection des sondage","Vous allez retourner à la selection des sondages, vous allez perdre toutes le travaille réaliser jusqu'à maintenant",
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[0]);


		if(n==0) // on qutie la page
		jframe.changerVue(jframe.ss);
		
		//remise a zero de la liste des commentaire
		jframe.Commande.listeTextArea.clear();
		jframe.Commande.QuestEnCour=null;
		jframe.Commande.commentaire.setText("");
	}




}
