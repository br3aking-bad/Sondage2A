import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.TreeMap;

import javax.swing.JComboBox;

// TODO: Auto-generated Javadoc
/**
 * La Class ComboBoxChangeListenerForme permet lors de la sélection 
 * d'une mise en forme dans un jcomboBox, de mettre à jour le graphique d'une question
 * avec la nouvel mise en forme en question et le tableau de donnée brute.
 */
class ComboBoxChangeListenerForme implements ItemListener{

	/** Jframe de l'application */
	ApplicationAnalyste jframe;
	
	/**
	 * Instantie un nouveau combo box listener.
	 *
	 * @param app la jframe de l'application
	 */
	ComboBoxChangeListenerForme(ApplicationAnalyste app){
		this.jframe=app;


	}



	/* 
	 * 
	 */
	public void itemStateChanged(ItemEvent event) {



			if (event.getStateChange() == ItemEvent.SELECTED);{
				int viewRow = jframe.sq.tableau.getSelectedRow();
				if (viewRow >=0) {


					{ 
						String str =(String)event.getItem(); 

						boolean bool3D =false;

					int select=1;

					switch (str)

					{

					case "Barres":
						select=2;
						break;
						
					case "Barres 3D":
						bool3D=true;
						select=2;
						break;
						
						
					case "Camembert":
						select=3;
						break;
						
					case "Camembert 3D":
						bool3D=true;
						select=3;
						break;

					case "Lignes":
						select=4;
						break;
						
					case "Lignes 3D":
						bool3D=true;
						select=4;
						break;

					case "Aire":
						select=5;
						break;



					default:
						select=1;
						break;


					}

					boolean triAge=true;


					if(!jframe.sq.tableau.getValueAt(viewRow, 3).equals("Age")){
						triAge=false;
					}

					Question q=jframe.sq.tableau.lastQSelect;
					
					
					//mise a jour tableau de donnee brute
					TreeMap<String,TreeMap<String,Integer>> data=jframe.reponseBD.getDicoDonnees(q,triAge);
					jframe.sd.tableau.insert(data,q);

					Graphique g=new Graphique(jframe.reponseBD,q,select, triAge, bool3D); // 1er bool : age /proffession 2ieme bool : 3d ou non

					jframe.graphique.removeAll();
					jframe.graphique.add(g);
					//			 jframe.graphique.repaint();
					jframe.graphe=g;
					jframe.repaint();
					Dimension d=jframe.getSize();
					jframe.setSize(d.width+1,d.height);
					jframe.setSize(d);


					}		
				
			}}
	}}