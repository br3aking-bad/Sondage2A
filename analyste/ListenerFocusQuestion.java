import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JComboBox;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/**
 * @author elleuch
 *
 */
public class ListenerFocusQuestion implements FocusListener {
	ApplicationAnalyste jframe;

	ListenerFocusQuestion(ApplicationAnalyste app) {
		jframe=app;

	}


	@Override
	public void focusGained(FocusEvent e) {
		int viewRow = jframe.sq.tableau.getSelectedRow();
		if (viewRow >=0) {

		Question q=jframe.sq.tableau.listeQuestion.get(viewRow);
		jframe.sq.tableau.lastQSelect=q;
		jframe.sq.tableau.lastIndexQselect=viewRow;

		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}



