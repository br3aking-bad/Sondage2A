/*
 * 
 * 
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;


// 
/**
 * La Class ActionBoutonQuestion permet de détecter un clic sur un bouton
 * pour changer de vue, permettant de passer de la séleciton de
 *  sondage au plan de travail principal.
 *
 * @author elleuch
 */
public class ActionBoutonQuestion implements ActionListener {

/** Jframe de l'application.
 *  @see ApplicationAnalyste */
ApplicationAnalyste jframe;
	
	/** Le tableau contenant les questions d'un sondage.
	 * @see TableauQuestion */
	TableauQuestion tableauQu;
	
	/**Le tableau contenant les sondages d'un sondage.
	 * @see TableauSondage */
	TableauSondage tableauSo;
	
	/**
	 * Instantiates a new action bouton question.
	 *
	 * @param jframe jframe crée avec la classe ApplicationAnalyste.
	 * @param ss La selection des sondages.
	 */
	public ActionBoutonQuestion(ApplicationAnalyste jframe,SelectionSondage ss) {
		
		this.jframe=jframe;
		tableauQu=jframe.sq.tableau;
		tableauSo=ss.tableau;
		
	}
	


	/* 
	 * Change la vue de la jframe, ajoute initie les liste de TextArea de la boite de commande.
	 * Vide le tableau de questions, remplie le tableau de questions des questions du sondages selectioné, selectionne par defaut le premier élément du tableau de questions si le tableau n'est pas vide.
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Integer i=tableauSo.getSelectedRow();
		List<Question> listeQ=new ArrayList<Question>();
		if (i>=0){
			
			//vide le tableau de questions
			tableauQu.data.clear();
			tableauQu.listeQuestion.clear();

			
			listeQ=jframe.accesBD.getQuestions(tableauSo.listeQuestionnaire.get(i));
			Questionnaire questionnaire=tableauSo.listeQuestionnaire.get(i);
			
			//met a jour le dernier questionnaire chargé
			tableauSo.lastQuestSelect=questionnaire;
			
			
			tableauQu.addQuestion(listeQ);
			 tableauQu.repaint();
			 //change de vue
			 jframe.changerVue(jframe.planTravail);
			 //met en plein écran la jframe
			 jframe.setExtendedState(jframe.MAXIMIZED_BOTH);
			 String str=tableauSo.listeQuestionnaire.get(i).getTitre();
			 //change bordure 
			 jframe.planTravail.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 3, true),"Analyse du sondage "+str));
		
		
			 
			 //ajout liste de textArea
			 jframe.Commande.listeTextArea.clear();
			 //met a jour listeTextArea de la boite de commande pour le sondage en questions
			 for(int j=0;j<listeQ.size();j++){
				 
				 jframe.Commande.listeTextArea.add(new JTextArea());
				 
//				listeTextArea=new ArrayList<JTextArea>();
			 }
			 
		}
			
		//si la liste de question n'est pas vide le tableau de 
		//question selectionne par defaut le premier élément
		if(listeQ.size()>0)
			jframe.sq.tableau.changeSelection(0, 0, false, false);
			jframe.Commande.QuestEnCour=0;
		
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
