// TODO: Auto-generated Javadoc
/**
 * La Class StringSizeComparator permet de trier des collections de strings de telle sorte que 
 * des String représentant des entiers soit trié dans l'ordre décroissant.
 * 
 * @author Elleuch
 */
public class StringSizeComparator implements java.util.Comparator<String> {

    /** The reference length. */
    private int referenceLength;

    /**
     * 
     *
     * @param Un String de référence.
     */
    public StringSizeComparator(String reference) {
        super();
        this.referenceLength = reference.length();
    }
    
   
    public int compare(String o1, String o2) {
        return extractInt(o1) - extractInt(o2);
    }

    /**
     * Extrait un int d'une chaine de caractères.
     *
     * @param Prend un String en paramètre.
     * @return Retourne un entier.
     */
    int extractInt(String s) {
        String num = s.replaceAll("\\D", "");
        // return 0 if no digits found
        return num.isEmpty() ? 0 : Integer.parseInt(num);
    }

}