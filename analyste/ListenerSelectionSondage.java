import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/**
 * @author elleuch
 *
 */
public class ListenerSelectionSondage implements ListSelectionListener {
SelectionSondage ss;
	
	ListenerSelectionSondage(SelectionSondage ss) {
		this.ss=ss;
	}
 
	public void valueChanged(ListSelectionEvent e) {
        int viewRow = ss.tableau.getSelectedRow();
        if (viewRow < 0) {
            //Selection got filtered away.
            ss.statusText.setText("");
        } else {
            int modelRow = 
               ss.tableau.convertRowIndexToModel(viewRow);
                ss.statusText.setText( (String) ss.tableau.getValueAt(viewRow,0));
    }

    }


}
