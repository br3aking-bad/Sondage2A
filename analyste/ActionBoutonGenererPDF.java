import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;



/**
 * controle le bouton générer un PDF
 * 
 */
public class ActionBoutonGenererPDF implements ActionListener {
ApplicationAnalyste jframe;


	/**
	 * @param jframe
	 */
	public ActionBoutonGenererPDF(ApplicationAnalyste jframe) {
		
		this.jframe=jframe;

	}
	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		Object[] options = {"Générer le PDF et revenir au menu principal ",
				"Non, je veux rester sur cette page",};
		int n=JOptionPane.showOptionDialog(jframe,"Vous allez générer un PDF et revenir au menu principal. \n      Seules les questions validées seront prises en compte. ","Message de validation",
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[0]);

		if(n==0)
		{
			if (jframe.sq.tableau.getQuestionValidee().size()!=0)
		{try
		{

			JFileChooser chooser=new JFileChooser();
			chooser.setSelectedFile(new File(jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getClient().getRaisonSoc()+"-"+jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getTitre()+".pdf"));
			int ch=chooser.showSaveDialog(null);
			File f=chooser.getSelectedFile();
			String chemin= f.getAbsolutePath();
			
		if (ch!=JFileChooser.CANCEL_OPTION)
		{
		Document d=new Document(PageSize.A4);
		PdfWriter.getInstance(d,new FileOutputStream(chemin));
		d.open();
		d.addLanguage("UTF-8");
		d.addAuthor("Rapid'Sond");
		d.addCreator("Fast Probing");
		d.addCreationDate();
		d.addTitle(jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getClient().getRaisonSoc()+"-"+jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getTitre()); //liste[0]
		
		Paragraph Titre=new Paragraph("Compte rendu d'analyse de sondage",FontFactory.getFont(FontFactory.TIMES_ROMAN,25,Font.TRUETYPE_FONT, BaseColor.BLACK));
		Titre.setAlignment(Element.ALIGN_CENTER);
		d.add(Titre);
		Paragraph Titre2=new Paragraph(jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getTitre(),FontFactory.getFont(FontFactory.TIMES_ROMAN,24,Font.TRUETYPE_FONT, BaseColor.BLACK));
		Titre2.setAlignment(Element.ALIGN_CENTER);
		d.add(Titre2);
		Paragraph l=new Paragraph("______________________________________________________________________________");d.add(l);
		for (int i=0;i< 6;i++)
		{Paragraph p=new Paragraph(" ");d.add(p);}
		Paragraph client=new Paragraph("Client : "+jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getClient().getRaisonSoc(),FontFactory.getFont(FontFactory.TIMES_ROMAN,28,Font.TRUETYPE_FONT, BaseColor.BLACK));
		client.setAlignment(Element.ALIGN_CENTER);
		d.add(client);
		for (int i=0;i< 4;i++)
		{Paragraph p=new Paragraph(" ");d.add(p);}
		Paragraph pan =new Paragraph("Panel : "+jframe.sq.tableau.getQuestionValidee().get(0).getQuestionnaire().getPanel().getNomPan(),FontFactory.getFont(FontFactory.TIMES_ROMAN,24,Font.TRUETYPE_FONT, BaseColor.BLACK));
		pan.setAlignment(Element.ALIGN_CENTER);
		d.add(pan);
		for (int i=0;i< 19;i++)
		{Paragraph p=new Paragraph(" ");d.add(p);}
		d.add(l);
		Paragraph rapid =new Paragraph("              Réalisé par Rapid'Sond avec le logiciel Fast'Probing",FontFactory.getFont(FontFactory.TIMES_ROMAN,18,Font.TRUETYPE_FONT, BaseColor.BLACK));
		rapid.setAlignment(Element.ALIGN_BOTTOM);
		d.add(rapid);
		d.setPageSize(PageSize.A4.rotate());
		d.newPage();
	
		for (Question q:jframe.sq.tableau.getQuestionValidee())
		{
			JFrame jf= new JFrame();
		Graphique g=new Graphique(jframe.reponseBD, q, 1,true, false);
		jf.setSize(800, 400);
		jf.add(g);
		jf.setVisible(false);
		
		
		g.sauveChart("./", "chart");
		Image image=Image.getInstance("chart.jpg");
		jf.dispose();

		Paragraph typetitre =new Paragraph("Type : "+q.getTypeQuestion().getIntituleT(),FontFactory.getFont(FontFactory.TIMES_ROMAN,20,Font.TRUETYPE_FONT, BaseColor.BLACK));

		Paragraph comtitre=new Paragraph("Commentaires :",FontFactory.getFont(FontFactory.TIMES_ROMAN,20,Font.TRUETYPE_FONT, BaseColor.BLACK));
		Paragraph c =new Paragraph("            "+jframe.Commande.listeTextArea.get(q.getNumQ()-1).getText(),FontFactory.getFont(FontFactory.TIMES_ROMAN,18,Font.TRUETYPE_FONT, BaseColor.BLACK));
		
		image.setAlignment(Element.ALIGN_CENTER);
		typetitre.setAlignment(Element.ALIGN_LEFT);
		comtitre.setAlignment(Element.ALIGN_LEFT);
		c.setAlignment(Element.ALIGN_LEFT);
		
		d.add(image);
		for (int i=0;i< 2;i++)
		{Paragraph p=new Paragraph(" ");d.add(p);}
		d.add(typetitre);
		for (int i=0;i< 1;i++)
		{Paragraph p=new Paragraph(" ");d.add(p);}
		d.add(comtitre);
		d.add(c);
		d.newPage();
		d.setPageSize(PageSize.A4.rotate());
		}
		d.close();
		
		jframe.changerVue(jframe.ss);
		jframe.setSize(new Dimension(850,400));
		Questionnaire q=jframe.ss.tableau.lastQuestSelect;
		q.setEtat('Z');
		jframe.accesBD.updateQuestionnaire(q);
		}


		}
		catch(DocumentException e){System.out.println(jframe.Commande.commentaire.getText()+"1");}
		catch(MalformedURLException e){System.out.println(jframe.Commande.commentaire.getText()+"2");}
		catch(IOException e){System.out.println(jframe.Commande.commentaire.getText()+"3");}
		}
	}
	}
}
	

