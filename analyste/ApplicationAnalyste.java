import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;




/**
 * La Class ApplicationAnalyste est la vue principale de l'application.
 */
@SuppressWarnings("serial")
public class ApplicationAnalyste extends JFrame {
	
	/** Permet l'accès à la  base de donnée et d'interagir avec elle.
	 * see AccesBD */
	AccesBD accesBD;
	
	/** Panel contenant le tableau de question. */
	SelectionQuestion sq;
	
	/** Panel contenant le tableau de sondage. */
	SelectionSondage ss;
	
	/** Panel contenant le tableau de donnée brute. */
	SelectionDonneeBrute sd;
	
	
	/** Panel contenant le plan travail principal de l'application 
	 * (tableau question, boite de commande, tableau de donnée brute, graphique). */
	JPanel planTravail;
	
	/** Panel contenant le graphique d'une question.
	 * */
	JPanel graphique;
	
	/** Object qui permet d'afficher un graphique 
	 * see Graphique  */
	Graphique graphe;
	
	/** Il s'agit de la boite de commande.
	 * @see Commande */
	Commande Commande;
	
	/** Permet de récupéré les réponses d'une question dans la base de donnée
	 * see ReponsesBD */
	ReponsesBD reponseBD;

	/**
	 * Permet d'instancier une application analyste.
	 * @param pseudo permet de se connecter en rentrant son identifiant de session
	 */
	
	public ApplicationAnalyste(String pseudo ){
		super("RapidSound - Analyse"); 
		
		ConnexionMySQL co = new ConnexionMySQL("servinfo-db", "db"+pseudo,"db"+pseudo, "/home/"+pseudo+"/");
//		System.out.println(co);
		accesBD=new AccesBD(co);
		reponseBD=new ReponsesBD(co);
		
		
//		System.out.println(accesBD.getQuestionnaires('P'));
		this.pack();

		this.setSize(850,400);    // taille de la fenetre
       this.setPreferredSize(new Dimension(500, 70));

       
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container c = this.getContentPane();
		
		this.setBackground(Color.WHITE);
		
		//Creation jpanel SelectionSondage 

		
		
		sq=new SelectionQuestion(this);
		
		
		ss=new SelectionSondage(this);

		sd=new SelectionDonneeBrute(this);
	
		graphe=new Graphique(reponseBD,accesBD.getQuestion(1, 1),1, true, false);
		
		 graphique=graphe; // 1er bool : age /proffession 2ieme bool : 3d ou non
//		 graphique=new JPanel();
		
		Commande=new Commande(this);

		 planTravail=new JPanel();

		 planTravail.add(sq);
	 planTravail.add(Commande);
	 planTravail.add(sd);
	 planTravail.add(graphique);
		 
		 
		this.add(ss);		
		this.setBackground(Color.WHITE);
		
		
		
		
		
		sq.setBackground(Color.LIGHT_GRAY);
		ss.setBackground(Color.LIGHT_GRAY);
		sd.setBackground(Color.LIGHT_GRAY);
		graphique.setBackground(Color.LIGHT_GRAY);
		Commande.setBackground(Color.LIGHT_GRAY);
		 planTravail.setLayout(new GridLayout(2,2));
		 
//		 


	    
		graphique.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY),"Graphique"));
		Commande.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY),"boite de commandes"));

		sq.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY),"Selectionner une question"));
		ss.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY),"Selectionner une sondage"));
		sd.setBorder(new TitledBorder(new LineBorder(Color.LIGHT_GRAY),"Donnees brutes"));

		

		
		//Création Menue non implémenter 
//		JMenuBar monMenu = creationMenu();
//		this.setJMenuBar(monMenu);
		
		
		this.setVisible(true);

		
		
	}
	
	/**
	 * Permet de changer le contenu de la fenetre principale de l'application.
	 *
	 * @param vue La nouvelle vue à afficher.
	 */
	public void changerVue(JPanel vue){
		Container c = this.getContentPane();
		c.removeAll();
		c.add(vue);
		this.revalidate();
		this.repaint();
//		this.setSize(l, h);

		
	}
	

	
public static void main(String[] args) {

	// affichage du questionnaire
	ApplicationAnalyste a=new ApplicationAnalyste("bellot");

	}



/**
 * Creation menu.
 *
 * @return Retourne une bar de menu
 */
public JMenuBar creationMenu(){
	JMenuBar MenuPrinc=new JMenuBar();
//	ControleurActionMenu am=new ControleurActionMenu(fq,q);
	
	
	
	//	ArrayList<JMenu> a= new ArrayList<JM>;
//	JMenu[] jmenu={new JMenu r,new JMenu r1};
	
	//premier menu
	JMenu truc=new JMenu("Trucs");
	truc.setMnemonic('T');
	JMenuItem connecter=new JMenuItem("Ouvrir");
	connecter.setMnemonic('a');
//	connecter.addActionListener( am);
//	JMenuItem quitter=new JMenuItem("quitter");
//	quitter.addActionListener(am);
//	quitter.setMnemonic('e');
//	
	
	JMenuItem sauvegarder=new JMenuItem("Sauvegarder");
	connecter.setMnemonic('z');
//	connecter.addActionListener((ActionListener) am);
	
	JMenuItem sauvegardersous=new JMenuItem("Sauvegarder sous");
	connecter.setMnemonic('z');
//	connecter.addActionListener((ActionListener) am);
	
truc.add(connecter);
//truc.add(quitter);
truc.add(sauvegarder);
truc.add(sauvegardersous);

//deuxieme menu

JMenu menu2=new JMenu("question");
menu2.setMnemonic('Q');
JMenuItem nouveau=new JMenuItem("Ajouter");
nouveau.setMnemonic('a');
//nouveau.addActionListener((ActionListener) am);
//quitter.addActionListener(am);
//quitter.setMnemonic('o');
menu2.add(nouveau);
//menu2.add(quitter);


MenuPrinc.add(truc);
MenuPrinc.add(menu2);
return MenuPrinc;

}



}
