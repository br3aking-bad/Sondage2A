import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableRowSorter;




/**
 * @author elleuch
 *
 */
public class SelectionQuestion  extends JPanel{
	
	ApplicationAnalyste jframe;
//    private JTable tableau;
    private JTextField filterText;
    private JTextField statusText;
	
	TableauQuestion tableau;
	Question questionseleted;
	
	
	
	public SelectionQuestion(ApplicationAnalyste jframe) {
		this.setLayout(new BorderLayout());
		this.jframe=jframe;
		this.tableau=new TableauQuestion(jframe);
		tableau.addFocusListener(new ListenerFocusQuestion(jframe));
		
		
		JScrollPane jscroll=new JScrollPane(tableau);
		this.add(jscroll,BorderLayout.CENTER);
		tableau.setFillsViewportHeight(true);
		
		
		
        

//        JPanel form = new JPanel(new SpringLayout());
//        JLabel l1 = new JLabel("Filtrer par numero de question :", SwingConstants.TRAILING);
//        form.add(l1);
//        filterText = new JTextField();
        //Whenever filterText changes, invoke newFilter.
//        filterText.getDocument().addDocumentListener(
//                new DocumentListener() {
//                    public void changedUpdate(DocumentEvent e) {
//                        newFilter();
//                    }
//                    public void insertUpdate(DocumentEvent e) {
//                        newFilter();
//                    }
//                    public void removeUpdate(DocumentEvent e) {
//                        newFilter();
//                    }
//                });
//        l1.setLabelFor(filterText);
//        form.add(filterText);
//        JLabel l2 = new JLabel("Question selectionee :", SwingConstants.TRAILING);
//        l2.setVisible(false);
//        form.add(l2);
//        statusText = new JTextField();
//        statusText.setVisible(false);

//        l2.setLabelFor(statusText);
//        form.add(statusText);
//        SpringUtilities.makeCompactGrid(form, 2, 2, 6, 6, 6, 6);
//        add(form,BorderLayout.SOUTH);
        
        this.setOpaque(true);
        
        tableau.getSelectionModel().addListSelectionListener(new ListenerSelectionQuestion(this));
        
        

        

        
    }

    public void setQuestion(List<Question> liste ){
    	for(Question q:liste){
    		tableau.addQuestion(q);
    	}
    	
    }
    /** 
     * Update the row filter regular expression from the expression in
     * the text box.
     */
    private void newFilter() {
        RowFilter<TableModelQuestion, Object> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter(filterText.getText(), 0);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        tableau.getSorter().setRowFilter(rf);
    }
		
		
    
    
    
    

    
    
    
	}




