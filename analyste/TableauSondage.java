import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;


/**
 * @author elleuch
 *
 */
public class TableauSondage extends JTable {
	//Le this
	//	  public  JTable this;
	//Les titres des colonnes
	private String[] comboDataForme = {"Anneaux","Barres","Camembert","Lignes","Aire"};
	private String[] comboDataTri = {"classe d'age", "categorie socio-professionnelle."};
	//	  private String[] comboDataType = {"par", "parr"};
	private TableRowSorter<TableModelQuestion> sorter;

	ArrayList<Object[]> data;
	ArrayList<Questionnaire> listeQuestionnaire;


	TableModelQuestion model;
	//	  private ComboRenderer combo;




	//2 bouton dans le this
	JButton supp=new JButton("");
	JButton modif=new JButton("");
	public Questionnaire lastQuestSelect;






	public TableauSondage(){
		lastQuestSelect=null;

		listeQuestionnaire= new ArrayList<Questionnaire>()  ;

		JComboBox comboBoxForme =new  JComboBox(comboDataForme);
		JComboBox comboBoxTri =new  JComboBox(comboDataTri);
		//	    JComboBox comboBoxType =new  JComboBox(comboDataTri);


		//Données de notre this
		//	    Object[][] data = {
		//	      {"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	      {"2", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)},
		//	    };

		data=new ArrayList<Object[]>();
		//	    Object[] a={"1", "        ","",comboDataTri[0],comboDataForme[0],new Boolean(false)};
		//	    data.add(a);

		//Les titres des colonnes
		String  title[] = {"Num Sondage","Intitule", "nom panel","Nom client","Crée par","Etat"};

		// modèle d'affichage
		model = new TableModelQuestion(data, title);
		this.setModel(model);
		//	    this = new JTable(model);




		TableColumn col = this.getColumnModel().getColumn(0);
		TableColumn col2 = this.getColumnModel().getColumn(2);
		TableColumn col6 = this.getColumnModel().getColumn(1);
		col6.setPreferredWidth(280);
		col.setPreferredWidth(120);
		col2.setPreferredWidth(100);

		this.setBackground(new Color(230, 230, 230));
		this.setBorder(BorderFactory.createLoweredBevelBorder());




		// triage

//		sorter = new TableRowSorter<TableModelQuestion>(this.model);
//		this.setRowSorter(sorter);
		this.setPreferredScrollableViewportSize(new Dimension(500, 70));
		this.setFillsViewportHeight(true);



	
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setColumnSelectionAllowed(true);
		//		this.setBounds(100, 100, 450, 300);
		this.setLayout(new GridLayout(0, 1, 0, 0));



		
	}

	public TableRowSorter<TableModelQuestion> getSorter(){
		return sorter;
	}

	public void addQuestionnaire(Questionnaire q) {
	    listeQuestionnaire.add(q);
		Object[] a={Integer.toString(q.getIdQ()),q.getTitre(),q.getPanel().getNomPan(),q.getClient().getRaisonSoc(),(q.getUtilisateur().getPrenomU()+" "+q.getUtilisateur().getNomU()),new Boolean(false)};
		//		    System.out.println(q.getPanel());
		data.add(a);
		
		
		
	}
	

	
	
}
