

public class Utilisateur {

	private int idU;
	private String nomU;
	private String prenomU;
	private String login;
	private String motDePasse;
	private RoleUtil roleUtil;

	/**
	 * @param idU
	 * @param nomU
	 * @param prenomU
	 * @param login
	 * @param motDePasse
	 * @param roleUtil
	 */
	public Utilisateur(int idU,String nomU,String prenomU,String login,String motDePasse,RoleUtil roleUtil)
	{
		this.idU=idU;
		this.nomU=nomU;
		this.prenomU=prenomU;
		this.login=login;
		this.motDePasse=motDePasse;
		this.roleUtil=roleUtil;
	}
	/**
	 * @param nomU
	 * @param prenomU
	 * @param login
	 * @param motDePasse
	 * @param roleUtil
	 */
	public Utilisateur(String nomU,String prenomU,String login,String motDePasse,RoleUtil roleUtil)
	{
		this.idU=-1;
		this.nomU=nomU;
		this.prenomU=prenomU;
		this.login=login;
		this.motDePasse=motDePasse;
		this.roleUtil=roleUtil;
	}

	/**
	 * @return
	 */
	public int getIdU() {
		return idU;
	}

	/**
	 * @param idU
	 */
	public void setIdU(int idU) {
		this.idU = idU;
	}

	/**
	 * @return
	 */
	public String getNomU() {
		return nomU;
	}

	/**
	 * @param nomU
	 */
	public void setNomU(String nomU) {
		this.nomU = nomU;
	}

	/**
	 * @return
	 */
	public String getPrenomU() {
		return prenomU;
	}

	/**
	 * @param prenomU
	 */
	public void setPrenomU(String prenomU) {
		this.prenomU = prenomU;
	}

	/**
	 * @return
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * @param motDePasse
	 */
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	/**
	 * @return
	 */
	public RoleUtil getRoleUtil() {
		return roleUtil;
	}

	/**
	 * @param roleUtil
	 */
	public void setRoleUtil(RoleUtil roleUtil) {
		this.roleUtil = roleUtil;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Utilisateur [idU=" + idU + ", nomU=" + nomU + ", prenomU="
				+ prenomU + ", login=" + login + ", motDePasse=" + motDePasse
				+ ", roleUtil=" + roleUtil + "]";
	}
	
	
}
