import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JComboBox;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/**
 * @author elleuch
 *
 */
public class ListenerSelectionQuestion implements ListSelectionListener {
	SelectionQuestion sq;
	ApplicationAnalyste jframe;

	ListenerSelectionQuestion(SelectionQuestion sq) {
		this.sq=sq;
		jframe=sq.jframe;

	}

	public void valueChanged(ListSelectionEvent e) {

			int viewRow = jframe.sq.tableau.getSelectedRow();
			
		
			if (viewRow >=0) {

				//JtextArea
				if(	jframe.Commande.QuestEnCour!=null){
					jframe.Commande.listeTextArea.get(jframe.Commande.QuestEnCour).setText(jframe.Commande.commentaire.getText());
					jframe.Commande.QuestEnCour=viewRow;

					jframe.Commande.commentaire.setText(jframe.Commande.listeTextArea.get(viewRow).getText());
				
				
				}

//				Question q=jframe.sq.tableau.lastQSelect;
//			
//				jframe.Commande.commentaire.setText(
//				


				sq.tableau.convertRowIndexToModel(viewRow);


				Question q=jframe.sq.tableau.listeQuestion.get(viewRow);

				boolean triAge=true;

				System.out.println(jframe.sq.tableau.getValueAt(viewRow, 3));
				if(!jframe.sq.tableau.getValueAt(viewRow, 3).equals("Age")){
					triAge=false;
				}
				TreeMap<String,TreeMap<String,Integer>> data=jframe.reponseBD.getDicoDonnees(q,triAge);

				String[]	title =data.keySet().toArray(new String[data.keySet().size()]);


				List<ValPossible> getValPossibles=new ArrayList<ValPossible>();

				jframe.sd.tableau.insert(data,q);


			}






//			System.out.println(jframe.sq.tableau.getValueAt(viewRow, 4));
			String str=(String) jframe.sq.tableau.getValueAt(viewRow, 4);
			
			boolean bool3D =false;
			
			int select=1;
			//multiple,simple et classement
			//pas pour libre
			//et note dans max val de question 
			switch (str)

			{

			case "Barres":
				select=2;
				break;
				
			case "Barres 3D":
				bool3D=true;
				select=2;
				break;
				
				
			case "Camembert":
				select=3;
				break;
				
			case "Camembert 3D":
				bool3D=true;
				select=3;
				break;

			case "Lignes":
				select=4;
				break;
				
			case "Lignes 3D":
				bool3D=true;
				select=4;
				break;

			case "Aire":
				select=5;
				break;



			default:
				select=1;
				break;


			}

			boolean triAge=true;
			if(!jframe.sq.tableau.getValueAt(viewRow, 3).equals("Age")){
				triAge=false;
			}

			//		jframe.sd.tableau.chargerNomCol(title);
			Question q=jframe.sq.tableau.listeQuestion.get(viewRow);

			Graphique g=new Graphique(jframe.reponseBD,q,select,triAge, bool3D); // 1er bool : age /proffession 2ieme bool : 3d ou non

			jframe.graphique.removeAll();
			jframe.graphique.add(g);
			//		 jframe.graphique.repaint();
			jframe.graphe=g;
			jframe.repaint();
			Dimension d=jframe.getSize();
			jframe.setSize(0,0);
			jframe.setSize(d);

			//		jframe.sd.setDonnee(data);
			//		System.out.println("setDonnee fait");
			//
			//		 System.out.println("valeur tableau");
			//
			//		 System.out.println(jframe.sd.tableau.getColumnName(0));
			//		 
			//		 
			//			
			//		 jframe.sd.tableau.repaint();
			//		 jframe.sd.tableau.invalidate();
			//		 jframe.sd.revalidate();
			//		 jframe.sd.repaint();
			//		 


		}
	
}



