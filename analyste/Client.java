public class Client {

	private int numC; 
	private String raisonSoc;
	private String adresse1;
	private String adresse2;
	private int codePostal;
	private String ville;
	private String telephone;
	private String email;
	
	/**
	 * @param numC
	 * @param raisonSoc
	 * @param adresse1
	 * @param adresse2
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 */
	public Client(int numC, String raisonSoc, String adresse1, String adresse2, int codePostal, String ville, String telephone, String email)
	{
		this.numC=numC;
		this.raisonSoc=raisonSoc;
		this.adresse1=adresse1;
		this.adresse2=adresse2;
		this.codePostal=codePostal;
		this.ville=ville;
		this.telephone=telephone;
		this.email=email;
	}
	/**
	 * @param raisonSoc
	 * @param adresse1
	 * @param adresse2
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 */
	public Client(String raisonSoc, String adresse1, String adresse2, int codePostal, String ville, String telephone, String email)
	{
		this.numC=-1;
		this.raisonSoc=raisonSoc;
		this.adresse1=adresse1;
		this.adresse2=adresse2;
		this.codePostal=codePostal;
		this.ville=ville;
		this.telephone=telephone;
		this.email=email;
	}

	/**
	 * @return
	 */
	public int getNumC() {
		return numC;
	}

	/**
	 * @param numC
	 */
	public void setNumC(int numC) {
		this.numC = numC;
	}

	/**
	 * @return
	 */
	public String getRaisonSoc() {
		return raisonSoc;
	}

	/**
	 * @param raisonSoc
	 */
	public void setRaisonSoc(String raisonSoc) {
		this.raisonSoc = raisonSoc;
	}

	/**
	 * @return
	 */
	public String getAdresse1() {
		return adresse1;
	}

	/**
	 * @param adresse1
	 */
	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	/**
	 * @return
	 */
	public String getAdresse2() {
		return adresse2;
	}

	/**
	 * @param adresse2
	 */
	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	/**
	 * @return
	 */
	public int getCodePostal() {
		return codePostal;
	}

	/**
	 * @param codePostal
	 */
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	/**
	 * @return
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Client [numC=" + numC + ", raisonSoc=" + raisonSoc
				+ ", adresse1=" + adresse1 + ", adresse2=" + adresse2
				+ ", codePostal=" + codePostal + ", ville=" + ville
				+ ", telephone=" + telephone + ", email=" + email + "]";
	}
	
	
	
}
