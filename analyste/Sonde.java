
import java.util.Date;


public class Sonde {

	private int numSond;
	private String nomSond;
	private String prenomSond;
	private Date dateNaisSond;
	private String telephoneSond;
	private Caracteristique caracteristique;
	
	/**
	 * @param numSond
	 * @param nomSond
	 * @param prenomSond
	 * @param dateNaisSond
	 * @param telephoneSond
	 * @param caracteristique
	 */
	public Sonde(int numSond,String nomSond,String prenomSond,Date dateNaisSond,String telephoneSond,Caracteristique caracteristique)
	{
		this.numSond=numSond;
		this.nomSond=nomSond;
		this.prenomSond=prenomSond;
		this.dateNaisSond=dateNaisSond;
		this.telephoneSond=telephoneSond;
		this.caracteristique=caracteristique;
	}
	/**
	 * @param nomSond
	 * @param prenomSond
	 * @param dateNaisSond
	 * @param telephoneSond
	 * @param caracteristique
	 */
	public Sonde(String nomSond,String prenomSond,Date dateNaisSond,String telephoneSond,Caracteristique caracteristique)
	{
		this.numSond=-1;
		this.nomSond=nomSond;
		this.prenomSond=prenomSond;
		this.dateNaisSond=dateNaisSond;
		this.telephoneSond=telephoneSond;
		this.caracteristique=caracteristique;
	}
	
	/**
	 * @return
	 */
	public int getNumSond() {
		return numSond;
	}

	/**
	 * @param numSond
	 */
	public void setNumSond(int numSond) {
		this.numSond = numSond;
	}

	/**
	 * @return
	 */
	public String getNomSond() {
		return nomSond;
	}

	/**
	 * @param nomSond
	 */
	public void setNomSond(String nomSond) {
		this.nomSond = nomSond;
	}

	/**
	 * @return
	 */
	public String getPrenomSond() {
		return prenomSond;
	}

	/**
	 * @param prenomSond
	 */
	public void setPrenomSond(String prenomSond) {
		this.prenomSond = prenomSond;
	}

	/**
	 * @return
	 */
	public Date getDateNaisSond() {
		return dateNaisSond;
	}

	/**
	 * @param dateNaisSond
	 */
	public void setDateNaisSond(Date dateNaisSond) {
		this.dateNaisSond = dateNaisSond;
	}

	/**
	 * @return
	 */
	public String getTelephoneSond() {
		return telephoneSond;
	}

	/**
	 * @param telephoneSond
	 */
	public void setTelephoneSond(String telephoneSond) {
		this.telephoneSond = telephoneSond;
	}

	/**
	 * @return
	 */
	public Caracteristique getCaracteristique() {
		return caracteristique;
	}

	/**
	 * @param caracteristique
	 */
	public void setCaracteristique(Caracteristique caracteristique) {
		this.caracteristique = caracteristique;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sonde [numSond=" + numSond + ", nomSond=" + nomSond
				+ ", prenomSond=" + prenomSond + ", dateNaisSond="
				+ dateNaisSond + ", telephoneSond=" + telephoneSond
				+ ", caracteristique=" + caracteristique + "]";
	}
	
	

}
