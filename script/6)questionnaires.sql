insert into QUESTIONNAIRE(idQ, Titre, Etat, numC, idU, idPan) values
(5,'Des gouts et des couleurs','A',15678,1,1);


insert into TYPEQUESTION(idT,intituleT,typeReponse) values
('u','Choix unique','Caract�res'),
('m','Choix multiple','Caract�res'),
('c','Classement','Entier'),
('n','Note','Entier'),
('l','R�ponse libre', 'Caract�res');


insert into QUESTION(idQ,numQ,texteQ,MaxVal,idT) values
(5,1,'Sur une �chelle de 0 � 10 comment noteriez vous la couleur des lampadaires de la ville?',10,'n'),
(5,2,'Sur une �chelle de 0 � 10 comment noteriez vous la couleur de la mairie?',10,'n'),
(5,3,'Parmi ces couleurs classez en trois pour repeindre la fa�ade de la mairie.',3,'c'),
(5,4,'Quelle est votre couleur pr�f�r�e?',NULL,'l');


insert into VALPOSSIBLE(idQ, numQ, idV, Valeur) values
(5,3,1,'Bleu'),
(5,3,2,'Vert'),
(5,3,3,'Rouge'),
(5,3,4,'Jaune'),
(5,3,5,'Blanc'),
(5,3,6,'Noir'),
(5,3,7,'Cyan'),
(5,4,1,'Bleu'),
(5,4,2,'Vert'),
(5,4,3,'Rouge'),
(5,4,4,'Jaune'),
(5,4,5,'Blanc'),
(5,4,6,'Noir'),
(5,4,7,'Cyan');
