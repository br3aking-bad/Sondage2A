
public class Reponse {
	
	private Question question;
	private String valeur;
	private Caracteristique caracteristique;
	
	/**
	 * @param questionnaire
	 * @param question
	 * @param valeur
	 */
	public Reponse(Question question, Caracteristique caracteristique,String valeur)
	{
		this.question=question;
		this.caracteristique=caracteristique;
		this.valeur=valeur;
	}

	/**
	 * @return
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	/**
	 * @return
	 */
	public Caracteristique getCaracteristique() {
		return caracteristique;
	}

	/**
	 * @param caracteristique
	 */
	public void setCaracteristique(Caracteristique caracteristique) {
		this.caracteristique = caracteristique;
	}

	/**
	 * @return
	 */
	public String getValeur() {
		return valeur;
	}

	/**
	 * @param valeur
	 */
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Reponse [question="
				+ question + ", valeur=" + valeur + ", caracteristique="
				+ caracteristique + "]";
	}


	
	
}
