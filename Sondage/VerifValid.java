import java.awt.*;
import javax.swing.*;
import java.awt.event.* ; 

class VerifValid implements FocusListener{

	ModuleS ms;
	ActionBoutonReponse abr;
	int nbQuestions;

	VerifValid(ModuleS ms, ActionBoutonReponse abr){
		this.ms = ms;
		this.abr = abr ;
		nbQuestions = 0;
	}

	public void focusGained(FocusEvent e){
        try{
            if (((JTextField)(e.getSource())).getText().equals("N'oubliez pas d'appuyer sur entrée pour valider la réponse")){
                ((JTextField)(e.getSource())).setText("");
                ((JTextField)(e.getSource())).setForeground(Color.BLACK);
            }
        }
        catch (ClassCastException e56){}
	}

    public void focusLost(FocusEvent e){
		Integer numQuestion = Integer.parseInt(((Component)(e.getSource())).getName());
        try{
            if (((JTextField)(e.getSource())).getText().equals("")){
                ((JTextField)(e.getSource())).setText("N'oubliez pas d'appuyer sur entrée pour valider la réponse");
                ((JTextField)(e.getSource())).setForeground(Color.GRAY);
            }
        }
        catch (ClassCastException e5){}
    	if (  numQuestion  == nbQuestions){
    		if( abr.getQuestionsNonRepondues(nbQuestions).size() != 0){
    		ms.erreurValidation.setText("Attention vous n'avez pas répondu aux questions suivantes : " + abr.getQuestionsNonRepondues(numQuestion).toString().substring(1,abr.getQuestionsNonRepondues(numQuestion).toString().length()-1));
    		ms.attentionValidation(abr.getQuestionsNonRepondues(nbQuestions));
    		}
    	}
    		else{
    			ms.erreurValidation.setVisible(false);
    		}
    }	


    void setNbQuestions(int n){
    	nbQuestions = n;
    }

}