
public class Proposition {
	private String texte;
	private boolean valide;
	
	Proposition(String t, boolean b){
		texte=t;
		valide=b;
	}
	
	public String getText(){
		return this.texte;
	}
	
	public boolean getValide(){
		return this.valide;
	}
	
	public void setText(String texte){
		this.texte=texte;
	}
	
	public void setValide(boolean valide){
		this.valide=valide;
	}
	
	@Override
	public String toString(){
		String res="";
		if(this.getValide()){
			res=res+this.getText()+"[V]";
		}
		else{
			res=res+this.getText()+"[F]";
		}
		return res;
	}

}
