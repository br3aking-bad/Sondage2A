import java.awt.*;
import java.sql.*;
import javax.swing.*;
import java.util.ArrayList;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;


class Application{

	JTextField login ;
	JTextField mdp;
	JLabel erreur ; 
  	JFrame co;
	ConnexionMySQL c;

	Application(){
  	co = new JFrame("Connexion");
	co.setSize(500,250);    // taille de la fenetre
	co.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Container c = co.getContentPane();

	JPanel champs = new JPanel();
	champs.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 2, true),"Pour vous connecter, identifiez-vous "));

	login = new JTextField(40);
	mdp = new JPasswordField(40);

	login.setBorder(new TitledBorder(new LineBorder(Color.WHITE),"Login"));
	mdp.setBorder(new TitledBorder(new LineBorder(Color.WHITE),"Mot de passe"));

	JButton quitter = new JButton("Quitter");
	JButton allezHop = new JButton("Se connecter");

	quitter.setBorderPainted(false);
	allezHop.setBorderPainted(false);

	quitter.setName("quit");
	allezHop.setName("go");
	login.setName("go");
	mdp.setName("go");

	quitter.addActionListener(new BoutonsAppli(this));
	allezHop.addActionListener(new BoutonsAppli(this));
	login.addActionListener(new BoutonsAppli(this));
	mdp.addActionListener(new BoutonsAppli(this));

	erreur = new JLabel("");

	champs.add(login);
	champs.add(mdp);
	champs.add(quitter);
	champs.add(allezHop);
	champs.add(erreur);

	champs.setBackground(new Color(202, 219, 238));
	login.setBackground(new Color(212, 229, 248));
	mdp.setBackground(new Color(212, 229, 248));

	c.add(champs);



	co.setVisible(true);
  }

  /**
 * lance le bon module
 */
void lancer(){
  	//ici on lancera la fenetre en fonction de quelque chose (mot de passe ?)
 	// test avec Sondage, vu que c'est ce que j'ai sous la main
	String log = login.getText();
	try{
		erreur.setText("Connexion en cours...");
		c = new ConnexionMySQL("servinfo-db", "db" + log,"db" + log, "/home/" + log +"/");
		AccesBD a = new AccesBD(c);

		Utilisateur u= a.getUtilisateur(2);

		ModuleS m = new ModuleS(a, u);
		
		erreur.setText("");
		
		ModuleSondConnexion msc = new ModuleSondConnexion(a,m.sondage,u,m, this);
		co.setVisible(false);
	}
	catch (NullPointerException e ){
		c = null;
		erreur.setText("Problème d'identification. Réésayez ?");
		//System.out.println(e);

	}
  }


  public static void main(String [] args) {
  	Application a = new Application();
  }
}
