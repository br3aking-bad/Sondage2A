import java.awt.event.*;
import java.awt.Component;
import java.util.ArrayList ; 


public class ActionBoutonReponse implements ActionListener{
	
	private ModuleS ms;
	private ArrayList<Integer> ontDejaUneReponse ; 


	public ActionBoutonReponse(ModuleS s){
		ms = s;
		ontDejaUneReponse = new ArrayList<Integer>();
	}

	public void actionPerformed(ActionEvent e){
		Integer numQuestion = Integer.parseInt(((Component)(e.getSource())).getName());
		if (! ontDejaUneReponse.contains(numQuestion)){
			ontDejaUneReponse.add(numQuestion);
		}
		ms.nbRepondues.setText(ontDejaUneReponse.size() +"");
	}

	/**
	 * @param n
	 * @return l'arrayList des numéros de questions non validées
	 */
	ArrayList<Integer> getQuestionsNonRepondues(int n){
		ArrayList<Integer> list = new ArrayList<Integer>();
		int i = 0;
		while ( i<n){
			i ++ ;
			if ( ! ontDejaUneReponse.contains(i)){
				list.add(i);
			}
		}
		return list ; 

	}


}