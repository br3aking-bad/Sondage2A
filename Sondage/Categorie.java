
public class Categorie {
	
	private Character idCat;
	private String intituleCat;
	
	/**
	 * @param idCat
	 * @param intituleCat
	 */
	public Categorie(Character idCat,String intituleCat)
	{
		this.idCat=idCat;
		this.intituleCat=intituleCat;
	}

	/**
	 * @return
	 */
	public Character getIdCat() {
		return idCat;
	}

	/**
	 * @param idCat
	 */
	public void setIdCat(Character idCat) {
		this.idCat = idCat;
	}

	/**
	 * @return
	 */
	public String getIntituleCat() {
		return intituleCat;
	}

	/**
	 * @param intituleCat
	 */
	public void setIntituleCat(String intituleCat) {
		this.intituleCat = intituleCat;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Categorie [idCat=" + idCat + ", intituleCat=" + intituleCat
				+ "]";
	}
}
