import java.awt.event.*;
import java.awt.Component;
import java.util.ArrayList ; 


public class ActionBoutonConnexion implements ActionListener{
	
	private ModuleS ms;
	private ModuleSondConnexion msc;

	public ActionBoutonConnexion(ModuleSondConnexion msc){
		this.msc=msc;
	}

	public void actionPerformed(ActionEvent e){
		switch (((Component)(e.getSource())).getName()){
			case "lancer" :
			msc.activerSondage();
			break;
			case "deconnexion":
			msc.retour();
			break;
		}
	}

}