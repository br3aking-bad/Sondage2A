import java.awt.*;
import javax.swing.*;
import java.util.Map;
import java.awt.Color;
import java.util.HashMap;
import javax.swing.JList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList ; 
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JCheckBox;
import java.awt.CheckboxGroup;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;


public class ModuleS extends JFrame{
	
	JPanel principal ; 
	JPanel questionnaire;
	JPanel toutEnBas ;
	JPanel partieSonde ;
	JLabel erreurValidation ;
	Questionnaire sondage ; 
	JLabel nbRepondues ; 
	ActionBoutonReponse abr ; 
	ActionBoutonTerminer abt ;
	BoutonsAppli ba; 
	AccesBD a ; 
	Utilisateur sondeur ;
	ModuleSondConnexion msc;
	JCheckBox passer;
	Sonde sonde;
	HashMap<Integer,ArrayList<Object>> dicoReponses;



	public ModuleS(AccesBD a, Utilisateur sondeur){
		//Login : " + sondeur.getPrenomU() + " "+ sondeur.getNomU());
		super("Sond - RapidSond | Sondage : " + a.getQuestionnaire(1).getTitre());
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.a = a ;
		System.out.println(a.getQuestionnaires('S'));
		if(a.getQuestionnaires('S').size()==0){
			this.sondage.setEtat('A');
			this.a.updateQuestionnaire(this.sondage);
			JButton stop = new JButton("Abandonner");
			stop.setName("stop");
			stop.addActionListener(new ActionBoutonAucunSonde());
			Object[] options = {stop};
			JOptionPane.showOptionDialog(null, "Il n'y a plus de sondages à traiter.", "Soyez vigilant ! ",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
		}
		else
		this.sondage = a.getQuestionnaires('S').get(0);

		this.sondeur=sondeur;

		if(a.getSondesNonTraites(sondage).size()==0){
			this.sondage.setEtat('A');
			this.a.updateQuestionnaire(this.sondage);
			JButton stop = new JButton("Abandonner");
			stop.setName("stop");
			stop.addActionListener(new ActionBoutonAucunSonde());
			Object[] options = {stop};
			JOptionPane.showOptionDialog(null, "Il n'y a plus de sondés non traités.", "Soyez vigilant ! ",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
		}
		else
			this.sonde=a.getSondesNonTraites(sondage).get(0);
		dicoReponses = new HashMap<Integer,ArrayList<Object>>();


		principal = new JPanel();
		questionnaire = new JPanel();
		toutEnBas = new JPanel();
	
		Container cont=this.getContentPane();

		cont.add(principal);
		this.setVisible(false);

		this.peindre();
	}


	public void peindreSonde(Sonde s){
		partieSonde.add(new JLabel("Prénom, nom sondé "));
		partieSonde.add(new JLabel(s.getPrenomSond() + " " +s.getNomSond()));
		partieSonde.add(new JLabel("N° téléphone "));
		partieSonde.add(new JLabel(s.getTelephoneSond()));
	}

	private void peindre(){
		principal.removeAll();
		questionnaire.removeAll();
		toutEnBas.removeAll();
		principal.setLayout(new BorderLayout());
		questionnaire.setLayout(new GridLayout(0,1,3,15));
		principal.add(toutEnBas, BorderLayout.PAGE_END);

		JScrollPane ascenseur = new JScrollPane(questionnaire);
		ascenseur.getVerticalScrollBar().setValue(ascenseur.getVerticalScrollBar().getMaximum());
		ascenseur.setPreferredSize(new Dimension(400,80));
		ascenseur.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 2, true),"Questions :"));
		ascenseur.setBackground(Color.WHITE);
		ascenseur.getVerticalScrollBar().setUnitIncrement(25);
		principal.add(ascenseur, BorderLayout.CENTER);
		
		partieSonde = new JPanel();
		partieSonde.setLayout(new GridLayout(2,2));
		principal.add(partieSonde,BorderLayout.NORTH);
		partieSonde.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 2, true),"Sonde :"));
		partieSonde.setBackground(new Color(202, 219, 238));
		abr = new ActionBoutonReponse(this);
		abt = new ActionBoutonTerminer(this, abr);
		peindreQuestionnaire(sondage);
		peindreToutEnBas(sondage);
		peindreSonde(sonde);


		principal.validate();
        principal.repaint();

        this.getContentPane().add(principal);
	}

	 void peindreQuestionnaire(Questionnaire sondage){
		int i = 0 ;
		VerifValid vv = new VerifValid(this, abr);
		for (Question q : a.getQuestions(sondage)){
			System.out.println(q);
			JPanel qt = new JPanel();
			qt.setBackground(Color.WHITE);
			//qt.setLayout(new BoxLayout(qt, BoxLayout.Y_AXIS));
			qt.setLayout(new GridLayout(3,1));
			i++ ;
			//TODO gerer propositions
			JPanel prop = new JPanel();
			
			prop.setBackground(Color.WHITE);
			dicoReponses.put(i,new ArrayList<Object>());
			dicoReponses.get(i).add(q.getTypeQuestion().getIdT());
			switch (q.getTypeQuestion().getIdT()){

				//libre
				case 'l' : 
					JTextField j = new JTextField("N'oubliez pas d'appuyer sur entrée pour valider la réponse", 60);
					j.setForeground(Color.GRAY);
					j.addFocusListener(vv);
					j.addActionListener(abr);
					j.setName(q.getNumQ()+ "");
					prop.add(j); 
					dicoReponses.get(i).add(j);
					break ; 

				//Classement
				case 'c' : 
				JPanel classement = new JPanel();
				classement.setBackground(Color.WHITE);
				classement.setLayout(new GridLayout(q.getMaxVal()+1,a.getValPossibles(q).size()+1,20,1));
				classement.add(new JLabel(""));
				for (ValPossible v : a.getValPossibles(q)){
					classement.add(new JLabel(v.getValeur()));
				}
				int cl = 0;
				while (cl < q.getMaxVal()){
					cl ++ ; 
					classement.add(new JLabel(cl + ""));
					ButtonGroup groupeDeLigne = new ButtonGroup();
					ArrayList<JRadioButton> listeDeLigne = new ArrayList<JRadioButton>();
					for (ValPossible v : a.getValPossibles(q)){
						JRadioButton b = new JRadioButton(v.getValeur());
						b.setVerticalTextPosition(SwingConstants.CENTER);
						b.setHorizontalTextPosition(SwingConstants.CENTER);
						b.setForeground(new Color(0,0,0,0));
						b.setName(q.getNumQ()+ "");
						b.addFocusListener(vv);
						b.addActionListener(abr);
						b.setBackground(Color.WHITE);
						groupeDeLigne.add(b);
						listeDeLigne.add(b);
						classement.add(b);
					}
					dicoReponses.get(i).add(listeDeLigne);
				}
				classement.setBorder(new LineBorder(new Color(202, 219, 238)));
				prop.add(classement);


				break ; 

				//choix multiple
				case 'm' : 
				for (ValPossible v : a.getValPossibles(q)){
					JCheckBox cb = new JCheckBox(v.getValeur());
					cb.setVerticalTextPosition(SwingConstants.TOP);
					cb.setHorizontalTextPosition(SwingConstants.CENTER);
					cb.setBackground(Color.WHITE);
					cb.addFocusListener(vv);
					cb.addActionListener(abr);
					cb.setName(q.getNumQ()+ "");
					prop.add(new JLabel("Maximum de réponses sélectionnées possibles : " + q.getMaxVal()));
					prop.add(cb);
					dicoReponses.get(i).add(cb);
				}
				break ;

				//note
				case 'n' : 
					ButtonGroup buttons = new ButtonGroup();
					int note=0;
					while ( note <= q.getMaxVal()){
						JRadioButton b = new JRadioButton(""+note);
						b.setBackground(Color.WHITE);
						b.setVerticalTextPosition(SwingConstants.TOP);
						b.setHorizontalTextPosition(SwingConstants.CENTER);
						b.setName(q.getNumQ() + "");
						b.addFocusListener(vv);
						b.addActionListener(abr);
						dicoReponses.get(i).add(b);
						buttons.add(b);
						prop.add(b);						
						note++;
					}
					break ;

				//choix unique
				case 'u' : 
				ButtonGroup boutons = new ButtonGroup();
					for (ValPossible v : a.getValPossibles(q)){
						System.out.println(v);
						JRadioButton b = new JRadioButton(v.getValeur());
						b.setVerticalTextPosition(SwingConstants.TOP);
						b.setHorizontalTextPosition(SwingConstants.CENTER);
						b.setBackground(Color.WHITE);
						b.setName(q.getNumQ()+ "");
						b.addFocusListener(vv);
						b.addActionListener(abr);
						dicoReponses.get(i).add(b);
						boutons.add(b);
						prop.add(b);
						}
					
					break ;
			}
			qt.add(new JLabel(q.getTexteQ()));
			qt.add(prop);
			qt.setBorder(new TitledBorder(new LineBorder(new Color(202, 219, 238)),"n°" + i));
			questionnaire.add(qt);
		}
		abt.setNbQuestions(i);
	}


	void peindreToutEnBas(Questionnaire sondage){
		JPanel infos = new JPanel();
		JLabel information1 = new JLabel("Nombre questions validées : ");
		nbRepondues = new JLabel("0");
		JLabel information2 = new JLabel("/" + a.getQuestions(sondage).size());
		infos.add(information1);
		infos.add(nbRepondues);
		infos.add(information2);
		infos.setBackground(new Color(202, 219, 238));


		erreurValidation = new JLabel("Attention vous n'avez pas répondu aux questions suivantes : ");
		erreurValidation.setForeground(Color.RED);
		erreurValidation.setVisible(false);
		JButton terminer = new JButton("Terminer");
      	terminer.setName("terminer");
		terminer.addActionListener(abt);
		passer = new JCheckBox("Passer au sondé suivant");
		passer.setName("passer");
		passer.setSelected(true);
		passer.setBackground(new Color(202, 219, 238));

		JPanel panelEnBas = new JPanel();
		JPanel boutonsFin = new JPanel();
		boutonsFin.setLayout(new GridLayout(2,1,5,5));
		panelEnBas.setLayout(new BorderLayout());
		panelEnBas.add(infos,BorderLayout.WEST);
		boutonsFin.add(terminer);
		boutonsFin.add(passer);
		boutonsFin.setBackground(new Color(202, 219, 238));
		panelEnBas.add(boutonsFin,BorderLayout.EAST);
		panelEnBas.setBackground(new Color(202, 219, 238));

		toutEnBas.setLayout(new BoxLayout(toutEnBas, BoxLayout.Y_AXIS));
		toutEnBas.add(panelEnBas);
		toutEnBas.add(erreurValidation);
		toutEnBas.setBackground(new Color(202, 219, 238));
	}

	public void attentionValidation(ArrayList<Integer> ontDejaUneReponse){		
		erreurValidation.setVisible(true);
	}

	public void activerConnexion(){
		msc.setVisible(true);
		changerSonde();
		this.setVisible(false);

	}

	public void setSondCo(ModuleSondConnexion msc){
		this.msc = msc ; 
	}

	public void changerSonde(){
		a.setInterroge(sondage,sondeur,sonde);
		if(a.getSondesNonTraites(sondage).size()==0)
		{	
			sondage.setEtat('A');
			this.a.updateQuestionnaire(this.sondage);
		}
		if(a.getQuestionnaires('S').size()==0){
			JButton stop = new JButton("Abandonner");
			stop.setName("stop");
			stop.addActionListener(new ActionBoutonAucunSonde());
			Object[] options = {stop};
			JOptionPane.showOptionDialog(null, "Il n'y a plus de sondages à traiter.", "Aucun sondage disponible ",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
		}
		else
		this.sondage = a.getQuestionnaires('S').get(0);

		if(a.getSondesNonTraites(sondage).size()==0){
			JButton stop = new JButton("Abandonner");
			stop.setName("stop");
			stop.addActionListener(new ActionBoutonAucunSonde());
			Object[] options = {stop};
			JOptionPane.showOptionDialog(null, "Il n'y a plus de sondés non traités.", "Tous les sondés ont été traités",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
		}
		else
			this.sonde=a.getSondesNonTraites(sondage).get(0);

		this.peindre();
	}

}