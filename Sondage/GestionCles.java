import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;

public class GestionCles {
	RSAPrivateKey prk;
	RSAPublicKey plk;
        String repSSH, repMySQL;
        public GestionCles(String _repSSH, String _repMySQL) throws Exception
	{
		super();
		this.repSSH=_repSSH;
		this.repMySQL=_repMySQL;
		this.lireClePrivee(_repSSH+"private_key.der");
		this.lireClePublique(_repSSH+"public_key.der");
	}
  public void lireClePrivee(String nomFic)
  throws Exception {

    File f = new File(nomFic);
    FileInputStream fis = new FileInputStream(f);
    DataInputStream dis = new DataInputStream(fis);
    byte[] keyBytes = new byte[(int)f.length()];
    dis.readFully(keyBytes);
    dis.close();

    PKCS8EncodedKeySpec spec =
      new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    prk= (RSAPrivateKey) kf.generatePrivate(spec);
  }
  public void lireClePublique(String nomFic)
		    throws Exception {

		    File f = new File(nomFic);
		    FileInputStream fis = new FileInputStream(f);
		    DataInputStream dis = new DataInputStream(fis);
		    byte[] keyBytes = new byte[(int)f.length()];
		    dis.readFully(keyBytes);
		    dis.close();

		    X509EncodedKeySpec spec =
		      new X509EncodedKeySpec(keyBytes);
		    KeyFactory kf = KeyFactory.getInstance("RSA");
		    plk= (RSAPublicKey) kf.generatePublic(spec);
		  }
  
  public byte[] crypter(String texte) {
	  byte[] tb=ajouter1Octet(texte.getBytes());
	  BigInteger bi=new BigInteger(tb);
	  return bi.modPow(plk.getPublicExponent(), plk.getModulus()).toByteArray();
  }
  
  public String decrypter(byte [] texte) {
	  BigInteger bi=new BigInteger(texte);
	  byte [] resb=retirer1Octet(bi.modPow(prk.getPrivateExponent(), prk.getModulus()).toByteArray());
	  return new String(resb);
	  }
  
  private byte[] ajouter1Octet(byte[] b) {
	    byte[] res = new byte[b.length+1];
	    res[0] = 1;
	    for (int i = 0; i < b.length; i++) {
	      res[i+1] = b[i];
	    }
	    return res;
	  }
	  
  private byte[] retirer1Octet(byte[] b) {
	    byte[] res = new byte[b.length-1];
	    for (int i = 0; i < res.length; i++) {
	      res[i] = b[i+1];
	    }
	    return res;
	  }
  public void sauverCle( String texte, String nomFic) throws Exception{
	  byte [] enc=crypter(texte);
	  File f = new File(this.repMySQL+nomFic);
	  FileOutputStream fos = new FileOutputStream(f);
	  DataOutputStream dos = new DataOutputStream(fos);
	  dos.write(enc);
	  dos.close();
	  f.setReadable(true, true);
  }
  
  public String lireCle(String nomFic)throws Exception{
	  File f = new File(this.repMySQL+nomFic);
	  FileInputStream fis = new FileInputStream(f);
	  DataInputStream dis = new DataInputStream(fis);
	  byte[] enc = new byte[(int)f.length()];
	  dis.readFully(enc);
	  dis.close();
	  return decrypter(enc);
  }
}
