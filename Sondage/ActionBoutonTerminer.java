import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import java.awt.Component;
import java.util.ArrayList ; 


public class ActionBoutonTerminer implements ActionListener{
	

	ModuleS ms;
	ActionBoutonReponse abr;
	int nbQuestions;

	ActionBoutonTerminer(ModuleS ms, ActionBoutonReponse abr){
		this.ms = ms;
		this.abr = abr ;
		nbQuestions = 0;
	}

	public void actionPerformed(ActionEvent e){
    	

		switch (((Component)(e.getSource())).getName()){
			case "terminer" : 
				String numQuestion = ((Component)(e.getSource())).getName();
	    		if(abr.getQuestionsNonRepondues(nbQuestions).size() != 0){
	    			ms.erreurValidation.setText("Attention vous n'avez pas répondu aux questions suivantes : " + abr.getQuestionsNonRepondues(nbQuestions).toString().substring(1,abr.getQuestionsNonRepondues(nbQuestions).toString().length()-1));
	    			ms.attentionValidation(abr.getQuestionsNonRepondues(nbQuestions));

			      	JButton go = new JButton("Revenir et compléter");
			      	go.setName("go");
			      	go.addActionListener(this);
			     	JButton stop = new JButton("Abandonner");
			      	stop.setName("stop");
			      	stop.addActionListener(this);
					Object[] options = {go ,stop};
					JOptionPane.showOptionDialog(null, "Toutes les questions n'ont pas trouvé réponse.\nRegardez au bas de la fenetre pour connaître lesquelles", "Soyez vigilant ! ",JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,null, options, options[0]);
				}
				else{
					enregistrerReponses();
					if (ms.passer.isSelected()){
						if(ms.a.getSondesNonTraites(ms.sondage).size()==0){
							ms.activerConnexion();
							JOptionPane.getRootFrame().dispose();
						}
						else{
							ms.changerSonde();
						}
					}
					else{						ms.activerConnexion();
						JOptionPane.getRootFrame().dispose();
					}
				}


				break ; 
			case "go":
				JOptionPane.getRootFrame().dispose();
				break ; 
			case "stop":
				ms.activerConnexion();
				JOptionPane.getRootFrame().dispose();
				break ; 
		}
	}


	public void enregistrerReponses(){
		//insertReponse(int idQ, int numQ, int numSond, String valeur)
		String valeur = "";
		for(int i=1;i<=nbQuestions;i++){
			valeur = "";
			ArrayList arr = (ArrayList)(ms.dicoReponses.get(i));
			switch((Character)(arr.get(0))){
				case 'l':
					JTextField c = (JTextField)(arr.get(1));
					valeur = c.getText();
					break;

				case 'c':
					int indicec = 1;
					for(int j=1;j<=arr.size()-1;j++){
						ArrayList<JRadioButton> liste = (ArrayList<JRadioButton>)arr.get(j);
						for (JRadioButton b : liste){
							if (b.isSelected()){
								if (valeur == ""){
									valeur = (indicec % liste.size())+2 + "";
								}
								else {
									valeur += ";";
									valeur += (indicec % liste.size()) +2+ "";
								}
							}
							indicec ++ ; 
						}
					}
				break;

				case 'm':
					for(int j=1;j<=arr.size()-1;j++){
						JCheckBox b = (JCheckBox)arr.get(j);
						if (b.isSelected()){
							if (valeur == ""){
								valeur = b.getText();
							}
							else {
								valeur += ";";
								valeur += b.getText();
							}
						}
					}
				break;

				case 'n':
					for(int j=1;j<=arr.size()-1;j++){
						JRadioButton b = (JRadioButton)arr.get(j);
						if (b.isSelected()){
							valeur = b.getText();
							break ; 
						}
					}
					

				break;

				case 'u':
					int indiceu = 1;
					for(int j=1;j<=arr.size()-1;j++){
						JRadioButton b = (JRadioButton)arr.get(j);
						//System.out.println(arr.get(j) + "u");
						if (b.isSelected()){
							valeur = indiceu + "";
							break;
						}
						indiceu ++ ; 
						}
				break;

			}
		ms.a.insertReponse(ms.sondage.getIdQ(),i, ms.sonde.getNumSond(), valeur);
		}
		
		}

	  


    void setNbQuestions(int n){
    	nbQuestions = n;
    }

}