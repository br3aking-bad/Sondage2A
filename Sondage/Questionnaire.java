

public class Questionnaire {

	private int idQ;
	private String titre;
	private Character etat;
	private Client client;
	private Utilisateur utilisateur;
	private Panel panel;

	
	/**
	 * @param idQ
	 * @param titre
	 * @param etat
	 * @param client
	 * @param utilisateur
	 * @param panel
	 */
	public Questionnaire(int idQ,String titre,Character etat,Client client,Utilisateur utilisateur,Panel panel)
	{
		this.idQ=idQ;
		this.titre=titre;
		this.etat=etat;
		this.client=client;
		this.utilisateur=utilisateur;
		this.panel=panel;
	}
	/**
	 * @param titre
	 * @param etat
	 * @param client
	 * @param utilisateur
	 * @param panel
	 */
	public Questionnaire(String titre,Character etat,Client client,Utilisateur utilisateur,Panel panel)
	{
		this.idQ=-1;
		this.titre=titre;
		this.etat=etat;
		this.client=client;
		this.utilisateur=utilisateur;
		this.panel=panel;
	}
	public Questionnaire(String titre,Character etat)
	{
		this.idQ=-1;
		this.titre=titre;
		this.etat=etat;	}

	/**
	 * @return
	 */
	public int getIdQ() {
		return idQ;
	}

	/**
	 * @param idQ
	 */
	public void setIdQ(int idQ) {
		this.idQ = idQ;
	}

	/**
	 * @return
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * @param titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * @return
	 */
	public Character getEtat() {
		return etat;
	}

	/**
	 * @param etat
	 */
	public void setEtat(Character etat) {
		this.etat = etat;
	}

	/**
	 * @return
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param utilisateur
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @return
	 */
	public Panel getPanel() {
		return panel;
	}

	/**
	 * @param panel
	 */
	public void setPanel(Panel panel) {
		this.panel = panel;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Questionnaire [idQ=" + idQ + ", titre=" + titre + ", etat="
				+ etat + ", client=" + client + ", utilisateur=" + utilisateur
				+ ", panel=" + panel + "]";
	}
	
	

}
