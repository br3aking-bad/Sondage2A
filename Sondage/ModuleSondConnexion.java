import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList ; 
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JCheckBox;
import java.awt.CheckboxGroup;
import javax.swing.JTextField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;


public class ModuleSondConnexion extends JFrame{

	AccesBD a ; 
	Questionnaire sondage ;
	Utilisateur sondeur;
	Sonde s;
	JButton lancer;
	JButton deconnexion;
	JPanel zoneBouton;
	JPanel principal;
	JPanel zoneIdentite;
	JLabel title;
	ModuleS ms;
	Application app;


	public ModuleSondConnexion(AccesBD a, Questionnaire sondage, Utilisateur sondeur,ModuleS ms, Application app){
		super("Sond - RapidSond | Login : " +  sondeur.getPrenomU() + " "+ sondeur.getNomU());
		this.setSize(450,275);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.sondage = sondage;
		this.sondeur = sondeur;
		this.a = a ;
		this.ms=ms;
		this.app = app ;
		ms.setSondCo(this);
		ActionBoutonConnexion abc = new ActionBoutonConnexion(this);


		lancer = new JButton("Lancer le sondage");
		lancer.setName("lancer");
		lancer.addActionListener(abc);
		deconnexion = new JButton("Déconnexion");
		deconnexion.setName("deconnexion");
		deconnexion.addActionListener(abc);

		Container cont=this.getContentPane();

		zoneBouton = new JPanel();
		zoneBouton.setBackground(Color.WHITE);
		zoneBouton.setLayout(new GridLayout(2,2,0,2));
		JLabel rien1 = new JLabel();
		JLabel rien2 = new JLabel();
		zoneBouton.add(rien1);
		zoneBouton.add(lancer);
		zoneBouton.add(rien2);
		zoneBouton.add(deconnexion);

		principal = new JPanel();
		principal.setLayout(new BorderLayout());
		principal.setBackground(Color.WHITE);
		principal.setBorder(new TitledBorder(new LineBorder(new Color(192, 194, 206), 3, true),"Vous êtes connecté(e)"));

		zoneIdentite = new JPanel();
		zoneIdentite.setLayout(new GridLayout(1,0));
		zoneIdentite.setBackground(Color.WHITE);

		title = new JLabel("Votre login : " + sondeur.getPrenomU() + " " + sondeur.getNomU());

		ImageIcon icone = new ImageIcon("icon.png");
		Image icone2 = icone.getImage().getScaledInstance(50,60,java.awt.Image.SCALE_SMOOTH);
		icone = new ImageIcon(icone2);
		JLabel b = new JLabel(icone);
		//b.setSize(new Dimension(50,60));

		zoneIdentite.add(b);
		zoneIdentite.add(title);

		principal.add(zoneIdentite, BorderLayout.CENTER);
		principal.add(zoneBouton, BorderLayout.SOUTH);

		cont.add(principal);


		this.setVisible(true);
		this.setResizable(false);
	}

	/**
	 * gère l'affichage de la fenêtre de sondage après avoir cliqué sur le bouton <Lancer le sondage>
	 */
	public void activerSondage(){
		ms.setVisible(true);
		this.setVisible(false);

	}

	/**
	 * gère le retour à l'écran de connexion 
	 */
	public void retour(){
		ms.setVisible(false);
		this.setVisible(false);
		app.co.setVisible(true);

	}
	


}