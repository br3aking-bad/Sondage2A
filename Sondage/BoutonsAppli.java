import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JLabel;


public class BoutonsAppli implements ActionListener {
	
	Application a ;

	public BoutonsAppli(Application a ) {
		this.a = a;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String nom = ((Component)e.getSource()).getName();
		switch (nom){
			case "quit" : System.exit(0);break;
			case "go" : a.lancer();break;
			case "stop": break;
		}
	}
}