import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class ConnexionMySQL
{
  private Connection connection;
  private String mdp=null;
  private GestionCles gs=null;
 
 public ConnexionMySQL(String serveur,String nombd,String loginMySQL,String repertoirePrincipal)
 {
   try 
  {
   Class.forName("com.mysql.jdbc.Driver");
  } 
  catch (ClassNotFoundException e) {
   System.out.println("Driver MySQL non trouve?");
   System.out.println(e);
   return;
  }
  System.out.println("Driver MySQL trouve");
  try{
   gs=new GestionCles(repertoirePrincipal+".ssh/",repertoirePrincipal+".cleMySQL/");
  }catch (Exception e){
   System.out.println("Impossible de retrouver les clés de cryptage");
   return;
  }
  
  try{
   mdp=gs.lireCle("sauveCle");
   System.out.println("Mot de passe decrypte");
  }catch (Exception e){
   System.out.println("Impossible de retrouver la cle MySQL");
   return;
  }
  
  try {
   this.connection = DriverManager.getConnection("jdbc:mysql://"+serveur+":3306/"+nombd,loginMySQL, mdp);
  } catch (SQLException e) {
   System.out.println("Connection echouee!" + e + "\n");
   return;
      
  }
  System.out.println("Connexion reussie");
  }

 public void reconnexion(String serveur,String nombd,String loginMySQL,String repertoirePrincipal)
 {
     String mdp=null;
  try{
   mdp=gs.lireCle("sauveCle");
       try {
   connection = DriverManager.getConnection("jdbc:mysql://"+serveur+":3306/"+nombd,loginMySQL, mdp);
    } 
    catch (SQLException ex) {
   System.out.println("Connection echouee !");
 }
  }
  catch (Exception e){
   System.out.println("Impossible de retrouver la cle MySQL");
   return;

  }
 }
 public void deconnection()
 {
     try {
   connection.close();
   System.out.println("Deconnexion réussie");
  } catch (SQLException e) {
   System.out.println("Probleme de deconnection");
 }
}
 
 public Connection getConnection()
 {
	 return connection;
	 }
 
}
