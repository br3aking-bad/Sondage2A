package Test;

public class Panel {

	private int idPan; 
	private String nomPan;

	/**
	 * @param idPan
	 * @param nomPan
	 */
	public Panel(int idPan, String nomPan)
	{
		this.idPan=idPan;
		this.nomPan=nomPan;
	}
	/**
	 * @param nomPan
	 */
	public Panel(String nomPan)
	{
		this.idPan=-1;
		this.nomPan=nomPan;
	}

	/**
	 * @return
	 */
	public int getidPan() {
		return idPan;
	}

	/**
	 * @param idPan
	 */
	public void setidPan(int idPan) {
		this.idPan = idPan;
	}

	/**
	 * @return
	 */
	public String getNomPan() {
		return nomPan;
	}

	/**
	 * @param nomPan
	 */
	public void setNomPan(String nomPan) {
		this.nomPan = nomPan;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return  "  " + nomPan ;
	}
	
	
}
