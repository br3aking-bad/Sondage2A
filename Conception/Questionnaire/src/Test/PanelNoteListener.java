package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class PanelNoteListener implements ActionListener {

	private panelNote panelNote;
	private ModelQuestionnaire modelques;
	private Tableau tab;
	
	public PanelNoteListener(panelNote panelNote, ModelQuestionnaire modelques, Tableau tab) {
		this.panelNote=panelNote;
		this.modelques=modelques;
		this.tab=tab;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object src=arg0.getSource();
		
		if(src==panelNote.getBtnsave()){
			modelques.getListeQuestion().get(tab.getTableau().getSelectedRow()).setMaxVal((int) panelNote.getNoteMax().getValue());
			JOptionPane.showMessageDialog(null, "La valeur max est enregistrer");
		}
		else if(src == panelNote.getBtnannule()){
			panelNote.getNoteMax().setValue(this.modelques.getListeQuestion().get(tab.getTableau().getSelectedRow()).getMaxVal());
		}
		
	}

}
