package Test;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Application extends JFrame {
	
	public AcceuilVue vuePrinc;
	public  ClientVue vueClient;
	public Utilisateur utilisateur;
	public AccesBD a;

	public Application(AccesBD a,Utilisateur utilisateur){
		super("RapidSond - Conception | Login : "+utilisateur.getNomU());
		this.a=a;
		this.utilisateur=utilisateur;
		this.vuePrinc = new AcceuilVue(this,a,this.utilisateur);
		this.vueClient = new ClientVue(this);
		
		this.setBounds(100, 100, 880, 401);    // taille de la fenetre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	
		Container c = this.getContentPane();
		c.add(vuePrinc);
	}
	
	public void changerVue(JPanel vue,int h,int l){
		Container c = this.getContentPane();
		this.setBounds(100,100,h,l);
		c.removeAll();
		c.add(vue);
		this.revalidate();
		this.repaint();
	}
	
	public static void main(String[] args) {
		AccesBD bd = new AccesBD(new ConnexionMySQL("servinfo-db","dbelghonnaji","dbelghonnaji","/home/elghonnaji/"));
		Utilisateur utilisateur=bd.getUtilisateur(1);
		Application application = new Application(bd,utilisateur);

		application.setVisible(true);
	}
}
