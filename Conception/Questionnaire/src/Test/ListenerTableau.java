package Test;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ListenerTableau  implements ListSelectionListener,FocusListener{
	private Tableau tableau;
	private panelQuestion panel;
	private AccesBD bd;
	private String typeQuestion;
	private CreerQuestionnaireVue creerQuestionnaireVue;
	private ModelQuestionnaire modelques;
	
	
	public ListenerTableau(Tableau tableau,panelQuestion panel,AccesBD bd,CreerQuestionnaireVue creerQuestionnaireVue, ModelQuestionnaire modelques) {
		// TODO Auto-generated constructor stub
		this.tableau=tableau;
		this.panel=panel;
		this.bd=bd;
		this.creerQuestionnaireVue=creerQuestionnaireVue;
		this.modelques=modelques;
		
		
	}

	@Override
	public void valueChanged(ListSelectionEvent event) {
		int viewRow = tableau.getTableau().getSelectedRow();
        if (viewRow < 0) {
           
        } else {
            int modelRow = 
                tableau.getTableau().convertRowIndexToModel(viewRow);
            	panel.getLblQues().setText((String) tableau.getTableau().getValueAt(viewRow,0).toString());
            	this.modelques.getListeQuestion().get(viewRow).setTexteQ((String) tableau.getTableau().getValueAt(viewRow,1).toString());
            	panel.getTextTitre().setText((String) tableau.getTableau().getValueAt(viewRow,1).toString());
            	typeQuestion=(String) tableau.getTableau().getValueAt(viewRow ,2).toString();
        		panel.setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "Type question : "+typeQuestion, TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
        		
            	
        		this.misajour();
            	
            	
        }
        }
	
	
	public void misajour( ){
		this.modelques.Affiche1();
		this.modelques.Affiche2();
		System.out.println("----------------------------------------");
		panel.ajoutePanel(typeQuestion);
		
		
		
	}

	public Tableau getTableau() {
		return tableau;
	}

	public void setTableau(Tableau tableau) {
		this.tableau = tableau;
	}

	public panelQuestion getPanel() {
		return panel;
	}

	public void setPanel(panelQuestion panel) {
		this.panel = panel;
	}

	public AccesBD getBd() {
		return bd;
	}

	public void setBd(AccesBD bd) {
		this.bd = bd;
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		
		int viewRow = tableau.getTableau().getSelectedRow();
        if (viewRow < 0) {
           
        } else {
            int modelRow = 
                tableau.getTableau().convertRowIndexToModel(viewRow);
            	panel.getLblQues().setText((String) tableau.getTableau().getValueAt(viewRow,0).toString());
            	panel.getTextTitre().setText((String) tableau.getTableau().getValueAt(viewRow,1).toString());
            	typeQuestion=(String) tableau.getTableau().getValueAt(viewRow ,2).toString();
        		panel.setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "Type question : "+typeQuestion, TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
        		
        		this.modelques.getListeQuestion().get(viewRow).setTexteQ((String) tableau.getTableau().getValueAt(viewRow,1).toString());
        		
        		
        		
            	this.misajour();
            	
            	
        }
        
       
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		
	}



}
