package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTable;

class DeleteButtonListener implements ActionListener {
    
    private int row;
    private JTable table;
     
    public void setRow(int row){this.row = row;}
    public void setTable(JTable table){this.table = table;}
     
    public void actionPerformed(ActionEvent event) {
     if(table.getRowCount() > 0){
        
    	 
        //On affecte un nouveau libellé à une celulle de la ligne
        ((ZModel)table.getModel()).removeRow(this.row);
        
         
     }
  }
}  