package Test;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import javax.swing.JOptionPane;


public class AccesBD {

	private Statement statement;
	private Connection c;
	
	public AccesBD(ConnexionMySQL cMySQL)

	{
		try{
			statement=cMySQL.getConnection().createStatement();
			this.c=cMySQL.getConnection();}
		catch(SQLException e){}
	}
	
	//CLIENT
		public int maxNumClient()
	{try{ResultSet rs=this.statement.executeQuery("select numC from CLIENT where numC >= ALL(select numC from CLIENT)");
		rs.next();
		int i=rs.getInt(1);
		rs.close();
		return i;}
		catch(SQLException e)
		{return 0;}
	}		
		public void insertClient(Client c)
		{try{
		PreparedStatement ps= this.c.prepareStatement("insert into CLIENT values(?,?,?,?,?,?,?,?)");
		ps.setInt(1,maxNumClient()+1);
		ps.setString(2,c.getRaisonSoc());
		ps.setString(3,c.getAdresse1());
		ps.setString(4,c.getAdresse2());
		ps.setInt(5,c.getCodePostal());
		ps.setString(6,c.getVille());
		ps.setString(7,c.getTelephone());
		ps.setString(8,c.getEmail());
		ps.executeUpdate();
		System.out.println("Client insere. ");
		}
		catch(SQLException e){System.out.println("Le Client n'a pas pu etre insere. ");}
		}
		public void deleteClient(int numC)
		{
			try{int i=this.statement.executeUpdate("delete from CLIENT where numC = "+numC);
				if(i==0) System.out.println("Numero client non existant");
				else System.out.println("Client efface. ");}
			catch(SQLException e){System.out.println("Numero client non existant");}
		}
		public void updateClient(Client c)
		{try{
		PreparedStatement ps= this.c.prepareStatement("update CLIENT set raisonSoc=?, adresse1=?, adresse2=?, CodePostal=?, Ville=?, Telephone=?, email=? where numC="+c.getNumC());
		ps.setString(1,c.getRaisonSoc());
		ps.setString(2,c.getAdresse1());
		ps.setString(3,c.getAdresse2());
		ps.setInt(4,c.getCodePostal());
		ps.setString(5,c.getVille());
		ps.setString(6,c.getTelephone());
		ps.setString(7,c.getEmail());
		int i=ps.executeUpdate();
		if (i!=0)System.out.println("Client modifie. ");
		else System.out.println("Le client n'a pas pu etre modifie");}
		catch(SQLException e)
		{System.out.println("Le client n'a pas pu etre modifie"); }
		}	
		public Client getClient(int numC)
		{
			try{ResultSet rs=this.statement.executeQuery("select * from CLIENT where numC="+numC);
				rs.next();
				Client c=new Client(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getInt(5),rs.getString(6),rs.getString(7),rs.getString(8));
				rs.close();
				return c;}
		catch(SQLException e)
		{System.out.println("La recherche a echouee"); return null;}
		}
		public List<Client> getClients()
	{
		{
			List<Client> clients=new ArrayList<Client>();
			try{
				ResultSet rs=this.statement.executeQuery("select * from CLIENT order by numC desc");
				while (rs.next())
					clients.add(new Client(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getInt(5),rs.getString(6),rs.getString(7),rs.getString(8)));
				rs.close();
				return clients;}
			catch(SQLException e)
				{System.out.println("La recherche a echouee"); return clients;}
		}
	}
		
		public List<Panel> getPanels()
	{
		{
			List<Panel> panels=new ArrayList<Panel>();
			try{
				ResultSet rs=this.statement.executeQuery("select * from PANEL order by IdPan desc");
				while (rs.next())
					panels.add(new Panel(rs.getInt(1),rs.getString(2)));
				rs.close();
				return panels;}
			catch(SQLException e)
				{System.out.println("La recherche a echouee"); return panels;}
		}
	}
		public List<Questionnaire> getQuestionnaires(Client c)
	{
		{
			List<Questionnaire> questionnaires=new ArrayList<Questionnaire>();
			try{
				TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
				ResultSet rs=this.statement.executeQuery("select * from QUESTIONNAIRE where numC="+c.getNumC()+" order by Etat,Titre");
				int i=0;
				while (rs.next())
				{	m.put(i, new ArrayList<Object>(Arrays.asList(rs.getInt(1),rs.getString(2),rs.getString(3).charAt(0),rs.getInt(4),rs.getInt(5),rs.getInt(6))));
					i++;
				}
				rs.close();
				for (ArrayList<Object> l: m.values())
					questionnaires.add(new Questionnaire((int)l.get(0),(String)l.get(1),(Character)l.get(2),getClient((int)l.get(3)),getUtilisateur((int)l.get(4)),getPanel((int)l.get(5))));
				return questionnaires;}
			catch(SQLException e)
				{System.out.println("La recherche a echouee"); return questionnaires;}
		}
	}
	//QUESTIONNAIRE
		public int maxNumQuestionnaire()
	{try{ResultSet rs=this.statement.executeQuery("select idQ from QUESTIONNAIRE where idQ >= ALL(select idQ from QUESTIONNAIRE)");
		rs.next();
		int i=rs.getInt(1);
		rs.close();
		return i;}
		catch(SQLException e)
		{return 0;}
	}	
		public void insertQuestionnaire(Questionnaire Q)
		{try{
		PreparedStatement ps= this.c.prepareStatement("insert into QUESTIONNAIRE values(?,?,?,?,?,?)");
		ps.setInt(1,maxNumQuestionnaire()+1);
		ps.setString(2,Q.getTitre());
		ps.setString(3,Q.getEtat()+"");
		ps.setInt(4,Q.getClient().getNumC());
		ps.setInt(5,Q.getUtilisateur().getIdU());
		ps.setInt(6,Q.getPanel().getidPan());
		
		ps.executeUpdate();
		System.out.println("Questionnaire insere. ");
		}
		catch(SQLException e){System.out.println("Le Questionnaire n'a pas pu etre insere. ");}
		}
		public void deleteQuestionnaire(int idQ)
		{
			try{int i=this.statement.executeUpdate("delete from QUESTIONNAIRE where idQ = "+idQ);
				if(i==0) System.out.println("Numero questionnaire non existant");
				else System.out.println("Questionnaire efface. ");}
			catch(SQLException e){System.out.println("Numero questionnaire non existant");}
		}
		public void updateQuestionnaire(Questionnaire Q)
		{
			try{
				PreparedStatement ps= this.c.prepareStatement("update QUESTIONNAIRE set idQ=?, Titre=?, Etat=? where idQ="+Q.getIdQ());
				ps.setInt(1,Q.getIdQ());
				ps.setString(2,Q.getTitre());
				ps.setString(3,Q.getEtat()+"");
				int i=ps.executeUpdate();
				if (i!=0)System.out.println("Questionnaire modifie. ");
				else System.out.println("Le questionnaire n'a pas pu etre modifie");}
			catch(SQLException e)
				{System.out.println("Le questionnaire n'a pas pu etre modifie"); }
		}	
		public Questionnaire getQuestionnaire(int idQ)
		{
			try{
				ResultSet rs=this.statement.executeQuery("select * from QUESTIONNAIRE where idQ="+idQ);
				rs.next();
				int s1=rs.getInt(1);String s2=rs.getString(2);String s3=rs.getString(3);int s4=rs.getInt(4);int s5=rs.getInt(5);int s6=rs.getInt(6);
				rs.close();
				return new Questionnaire((int)s1,(String)s2,(Character)s3.charAt(0),getClient((int)s4),getUtilisateur((int)s5),getPanel((int)s6));}
			catch(SQLException e)
			{System.out.println("La recherche a echouee"); return null;}
		}
		public List<Question> getQuestions(Questionnaire Q)
	{
		List<Question> questions=new ArrayList<Question>();
		try{
		TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
		ResultSet rs=this.statement.executeQuery("select * from QUESTION where idQ="+Q.getIdQ());
		int i=0;
		while (rs.next())
		{	m.put(i, new ArrayList<Object>(Arrays.asList(rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getString(5).charAt(0))));
			i++;
		}
		rs.close();
		for (ArrayList<Object> l: m.values())
			questions.add(new Question(Q,(int)l.get(0),(String)l.get(1),(int)l.get(2),getTypeQuestion((char)l.get(3))));
		return questions;}
		catch(SQLException e)
			{System.out.println("La recherche a echouee"); return questions;}
	}
		public List<Sonde> getSondes(Questionnaire Q)
		
	{
		List<Sonde> sondes=new ArrayList<Sonde>();
		try{
			TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
			ResultSet rs=this.statement.executeQuery("select numSond,nomSond,prenomSond,dateNaisSond,telephoneSond,idC from SONDE natural join PANEL natural join CONSTITUER where idPan="+Q.getPanel().getidPan());
			int i=0;
			while (rs.next())
			{	m.put(i, new ArrayList<Object>(Arrays.asList(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6))));
				i++;
			}
			rs.close();
			for (ArrayList<Object> l: m.values())
				sondes.add(new Sonde((int)l.get(0),(String)l.get(1),(String)l.get(2),(Date)l.get(3),(String)l.get(4),getCaracteristique((String)l.get(5))));
			return sondes;}
		catch(SQLException e)
			{System.out.println("La recherche a echouee"); return sondes;}
	}
		public List<Sonde> getSondesNonTraites(Questionnaire Q)
	{
		List<Sonde> sondes=new ArrayList<Sonde>();
		try{
			TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
			ResultSet rs=this.statement.executeQuery("select numSond,nomSond,prenomSond,dateNaisSond,telephoneSond,idC from SONDE natural join PANEL natural join CONSTITUER where idPan="+Q.getPanel().getidPan());
			while (rs.next())
				m.put(rs.getInt(1), new ArrayList<Object>(Arrays.asList(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDate(4),rs.getString(5),rs.getString(6))));
			rs.close();
			ResultSet rs2=this.statement.executeQuery("select numSond from SONDE natural join INTERROGER where idQ="+Q.getIdQ());
			while (rs2.next())
				if (m.containsKey(rs2.getInt(1)))
					m.remove(rs2.getInt(1));
			rs2.close();
			for (ArrayList<Object> l: m.values())
			{
				sondes.add(new Sonde((int)l.get(0),(String)l.get(1),(String)l.get(2),(Date)l.get(3),(String)l.get(4),getCaracteristique((String)l.get(5))));
			}
		return sondes;}
		catch(SQLException e)
			{System.out.println("La recherche a echouee"); return sondes;}
	}
		public List<Questionnaire> getQuestionnaires(char Etat)
				{
			List<Questionnaire> questionnaires=new ArrayList<Questionnaire>();
			try{
				TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
				ResultSet rs=this.statement.executeQuery("select * from QUESTIONNAIRE where Etat="+Etat+" order by numC desc");
				int i=0;
				while (rs.next())
				{	m.put(i, new ArrayList<Object>(Arrays.asList(rs.getInt(1),rs.getString(2),rs.getString(3).charAt(0),rs.getInt(4),rs.getInt(5),rs.getInt(6))));
					i++;
				}
				rs.close();
				for (ArrayList<Object> l: m.values())
					questionnaires.add(new Questionnaire((int)l.get(0),(String)l.get(1),(Character)l.get(2),getClient((int)l.get(3)),getUtilisateur((int)l.get(4)),getPanel((int)l.get(5))));
				return questionnaires;}
			catch(SQLException e)
				{System.out.println("La recherche a echouee"); return questionnaires;}
		}
	//QUESTION
		
		
		public void viderQuestions(int idQ){
			try{
				int i = this.statement.executeUpdate("delete from QUESTION where idQ="+idQ);
				
				
			}catch(SQLException e){e.printStackTrace();}
			
			
		}
		
		
		
		public int maxNumQuestion(int idQ)
	{try{ResultSet rs=this.statement.executeQuery("select numQ from QUESTION where idQ="+idQ+" and numQ >= ALL(select numQ from QUESTION where idQ="+idQ+")");
		rs.next();
		int i=rs.getInt(1);
		return i;}
		catch(SQLException e)
		{return 0;}
	}	
		public void insertQuestion(Question q)
		{try{
		PreparedStatement ps= this.c.prepareStatement("insert into QUESTION values(?,?,?,?,?)");
		ps.setInt(1,q.getQuestionnaire().getIdQ());
		ps.setInt(2,maxNumQuestion(q.getQuestionnaire().getIdQ())+1);
		ps.setString(3,q.getTexteQ());
		ps.setInt(4,q.getMaxVal());
		ps.setString(5,q.getTypeQuestion().getIdT()+"");
		
		ps.executeUpdate();
		System.out.println("Question inseree. ");
		}
		catch(SQLException e){System.out.println("La Question n'a pas pu etre inseree. ");}
		}
		public void deleteQuestion(int numQ)
		{
			try{int i=this.statement.executeUpdate("delete from QUESTION where idQ = "+getQuestionnaire(numQ).getIdQ()+" and numQ="+numQ);
				if(i==0) System.out.println("Numero question non existant");
				else System.out.println("Question effacee. ");}
			catch(SQLException e){System.out.println("Numero question non existant");}
		}
		public void updateQuestion(Question q)	
	
	{
		try{
			PreparedStatement ps= this.c.prepareStatement("update QUESTION set idQ=?, numQ=?, texteQ=?, MaxVal=?, idT=? where idQ="+q.getQuestionnaire().getIdQ()+" and numQ="+q.getNumQ());
			ps.setInt(1,q.getQuestionnaire().getIdQ());
			ps.setInt(2,q.getNumQ());
			ps.setString(3,q.getTexteQ());
			ps.setInt(4, q.getMaxVal());
			ps.setString(5, q.getTypeQuestion().getIdT()+"");
			
			int i=ps.executeUpdate();
			if (i!=0)System.out.println("Question modifiee. ");
			else System.out.println("La question n'a pas pu etre modifiee");}
		catch(SQLException e)
			{System.out.println("La question n'a pas pu etre modifiee"); }
	}
		
		
		
	public int getMaxQuestion(){
			
			{try{ResultSet rs=this.statement.executeQuery("select numQ from QUESTION where numQ>= all(select numQ from QUESTION)");
			rs.next();
			int i=rs.getInt(1);
			rs.close();
			return i;}
			catch(SQLException e)
			{return 0;}
			
			
			}
		}
		
		
		public Question getQuestion(int idQ,int numQ)
	{
		{
			try{
				ResultSet rs=this.statement.executeQuery("select * from QUESTION where idQ="+idQ+" and numQ="+numQ);
				rs.next();
				int s1=rs.getInt(1);int s2=rs.getInt(2);String s3=rs.getString(3);int s4=rs.getInt(4);String s5=rs.getString(5);
				rs.close();
				return new Question(getQuestionnaire(s1),s2,s3,s4,getTypeQuestion(s5.charAt(0)));}
			catch(SQLException e)
			{System.out.println("La recherche a echouee"); return null;}
		}
	}
		public List<ValPossible> getValPossibles(Question q)
	{
		List<ValPossible> valPossibles=new ArrayList<ValPossible>();
		try{
			TreeMap<Integer, ArrayList<Object>> m= new TreeMap<Integer, ArrayList<Object>>();
			ResultSet rs=this.statement.executeQuery("select idV,Valeur from VALPOSSIBLE where idQ="+q.getQuestionnaire().getIdQ()+" and numQ="+q.getNumQ()+" order by idV,Valeur");
			int i=0;
			while (rs.next())
			{	m.put(i, new ArrayList<Object>(Arrays.asList(rs.getInt(1),rs.getString(2))));
				i++;
			}
			rs.close();
			for (ArrayList<Object> l: m.values())
				{valPossibles.add(new ValPossible(q,(int)l.get(0),(String)l.get(1)));}
			return valPossibles;}
		catch(SQLException e)
			{System.out.println("La recherche a echouee"); return valPossibles;}
	}
	//VALPOSSIBLE
		public int maxNumValPossible(int idQ, int numQ)
	
	{try{ResultSet rs=this.statement.executeQuery("select idV from VALPOSSIBLE where idQ="+idQ+" and numQ="+numQ+"  and idV >= ALL(select idV from VALPOSSIBLE where idQ="+idQ+" and numQ="+numQ+")");
		rs.next();
		int i=rs.getInt(1);
		rs.close();
		return i;}
		catch(SQLException e)
		{return 0;}
	}	
		
		
		
		public int getMaxVal(){
			
			{try{ResultSet rs=this.statement.executeQuery("select idV from valpossible where idV>=all(select idV from valpossible)");
			rs.next();
			int i=rs.getInt(1);
			rs.close();
			return i;}
			catch(SQLException e)
			{return 0;}
			
			
			}
		}
		
		public void insertValPossible(ValPossible vp)
		{try{
		PreparedStatement ps= this.c.prepareStatement("insert into VALPOSSIBLE values(?,?,?,?)");
		ps.setInt(1,vp.getQuestion().getQuestionnaire().getIdQ());
		ps.setInt(2,vp.getQuestion().getNumQ());
		ps.setInt(3,maxNumValPossible(vp.getQuestion().getQuestionnaire().getIdQ(), vp.getQuestion().getNumQ())+1);
		ps.setString(4,vp.getValeur());
		
		ps.executeUpdate();
		System.out.println("ValPossible inseree. ");
		}
		catch(SQLException e){System.out.println("Le ValPossible n'a pas pu etre inseree. ");}
		}
		public void deleteValPossible(int idQ, int numQ,int idV)
		{
			try{int i=this.statement.executeUpdate("delete from VALPOSSIBLE where idQ = "+idQ+" and numQ="+numQ+" and idV="+idV);
				if(i==0) System.out.println("Numero ValPossible non existant");
				else System.out.println("ValPossible effacee. ");}
			catch(SQLException e){System.out.println("Numero ValPossible non existant");}
		}
		public void updateValPossible(ValPossible vp)	
	{
		try{
			PreparedStatement ps= this.c.prepareStatement("update VALPOSSIBLE set idQ=?, numQ=?, idV=?, Valeur=? where idQ="+vp.getQuestion().getQuestionnaire().getIdQ()+" and numQ="+vp.getQuestion().getNumQ());
			ps.setInt(1,vp.getQuestion().getQuestionnaire().getIdQ());
			ps.setInt(2,vp.getQuestion().getNumQ());
			ps.setInt(3,vp.getIdV());
			ps.setString(4,vp.getValeur());
			
			int i=ps.executeUpdate();
			if (i!=0)System.out.println("ValPossible modifiee. ");
			else System.out.println("La ValPossible n'a pas pu etre modifiee");}
		catch(SQLException e)
			{System.out.println("La ValPossible n'a pas pu etre modifiee"); }
	}
		public ValPossible getValPossible(int idQ, int numQ, int idV)

	{
		{
			try{
				ResultSet rs=this.statement.executeQuery("select * from VALPOSSIBLE where idQ="+idQ+" and numQ="+numQ+" and idV="+idV);
				rs.next();
				int s1=rs.getInt(1);int s2=rs.getInt(2);int s3=rs.getInt(3);String s4=rs.getString(4);
				rs.close();
				return new ValPossible(getQuestion(s1,s2),s3,s4);}
			catch(SQLException e)
			{System.out.println("La recherche a echouee"); return null;}
		}
	}
	//REPONSE	
		public void insertReponse(int idQ, int numQ, int numSond, String valeur)
{try{
PreparedStatement ps= this.c.prepareStatement("insert into REPONDRE values(?,?,?,?)");
ps.setInt(1,idQ);
ps.setInt(2,numQ);
ps.setString(3,getSond(numSond).getCaracteristique().getIdC()+"");
ps.setString(4,valeur);
ps.executeUpdate();
System.out.println("ValPossible inseree. ");
}
catch(SQLException e){System.out.println("Le ValPossible n'a pas pu etre inseree. ");}
}

	//AUTRE
		public Panel getPanel(int idPan)
{
	try{
		ResultSet rs=this.statement.executeQuery("select * from PANEL where idPan="+idPan);
		rs.next();
		Panel p= new Panel(rs.getInt(1),rs.getString(2));
		rs.close();
		return p;}
catch(SQLException e)
{System.out.println("La recherche a echouee"); return null;}
}
		public TypeQuestion getTypeQuestion(char idT) {
	
	try{
		ResultSet rs=this.statement.executeQuery("select * from TYPEQUESTION where idT='"+idT +"'");
		rs.next();
		TypeQuestion t=new TypeQuestion(rs.getString(1).charAt(0),rs.getString(2),rs.getString(3));
		rs.close();
		return t;}
	catch(SQLException e)
	{System.out.println("La recherche a echouee"); return null;}
}
		public Utilisateur getUtilisateur(int idU) {
	try{
		ResultSet rs=this.statement.executeQuery("select * from UTILISATEUR where idU="+idU);
		rs.next();
		int s1=rs.getInt(1);String s2=rs.getString(2);String s3=rs.getString(3);String s4=rs.getString(4);String s5= rs.getString(5);String s6=rs.getString(6);
		rs.close();
		return new Utilisateur(s1,s2,s3,s4,s5,getRoleUtil((s6).charAt(0)));}
	catch(SQLException e)
	{System.out.println("La recherche de l'utilisateur a echouee"); return null;}
}
		public RoleUtil getRoleUtil(char idR) {
	try{
		ResultSet rs=this.statement.executeQuery("select * from ROLEUTIL where idR='"+idR+"'");
		rs.next();
		RoleUtil r=new RoleUtil(rs.getString(1).charAt(0),rs.getString(2));
		rs.close();
		return r;}
	catch(SQLException e)
	{System.out.println("La recherche a echouee"); return null;}
}
		public Caracteristique getCaracteristique(String idC) {
		char sexe = idC.charAt(0);char idTr = idC.charAt(1);char idCat = idC.charAt(2);
		Categorie cat=getCategorie(idCat);
		Tranche t=getTranche(idTr);
		Caracteristique c=new Caracteristique(idC,sexe,t,cat);
		return c;}
		
		
		public Sonde getSond(int numSond)
{try{
	ResultSet rs=this.statement.executeQuery("select * from SONDE where numSond="+numSond);
	rs.next();
		int s1=rs.getInt(1);String s2=rs.getString(2);String s3=rs.getString(3);Date s4=rs.getDate(4);String s5=rs.getString(5);String s6=rs.getString(6);
		rs.close();
	return new Sonde(s1,s2,s3,s4,s5,getCaracteristique(s6));}
catch(SQLException e)
{System.out.println("La recherche pour le sonde a echouee" + e); return null;}
}
		public Categorie getCategorie(char idCat) {
	try{
		ResultSet rs=this.statement.executeQuery("select * from CATEGORIE where idCat='"+idCat+"'");
		rs.next();
		Categorie cat=new Categorie(rs.getString(1).charAt(0),rs.getString(2));
		rs.close();
		return cat;}
	catch(SQLException e)
	{System.out.println("La recherche a echouee"); return null;}
}
		public Tranche getTranche(char idTr) {
	try{
		ResultSet rs=this.statement.executeQuery("select * from TRANCHE where idTr='"+idTr+"'");
		rs.next();
		Tranche t=new Tranche(rs.getString(1).charAt(0),rs.getInt(2),rs.getInt(3));
		rs.close();
		return t;}
	catch(SQLException e)
	{System.out.println("La recherche a echouee"); return null;}
}

}
