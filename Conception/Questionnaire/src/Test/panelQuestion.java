package Test;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;


public class panelQuestion extends JPanel {
	
	
	private  JTextArea textTitre;
	private  JLabel lblQues;
	private  String title="Choix Simple";
	private AccesBD bd;
	private JPanel choix;
	private ModelQuestionnaire modelques;
	private Tableau tab;
	
	
	public panelQuestion(AccesBD bd, ModelQuestionnaire modelques) {
		
		
		this.modelques=modelques;
		
		this.setBackground(new Color(211, 211, 211));
		this.setToolTipText("");
		this.setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "Type question : "+title, TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		this.setBounds(710, 95, 399, 340);
		this.setLayout(null);
		
		//this.laQuestion = question; // initialisation du modèle
		
	

		this.bd=bd;
		
		
		textTitre = new JTextArea();
		textTitre.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		textTitre.setLineWrap(true);
		textTitre.setWrapStyleWord(true);
		textTitre.setEditable(false);
		textTitre.setBounds(130, 40,250, 50);
		add(textTitre);
		textTitre.setColumns(10);
		
		
	
		
		 lblQues = new JLabel("Question :");
		 
		lblQues.setFont(new Font("Segoe Script", Font.BOLD, 16));
		lblQues.setBounds(15, 55, 120, 18);
		add(lblQues);
		
		
		choix=new panelChoix( bd,modelques,tab);
		
	
	}
	
	
	
	
	public void ajoutePanel(String  str){
		this.remove(choix);
		int  q=this.modelques.getListeQuestion().get(this.tab.getTableau().getSelectedRow()).getNumQ();
		List<ValPossible> val=new ArrayList<ValPossible>();

		
		
		if(str.equals("Choix unique")){
			
			panelChoix c=new panelChoix(bd,this.modelques,tab);
			choix=(JPanel)c;
			this.add(choix);
			
			
		}
		else if(str.equals("Note")){
			this.modelques.getListeVal().put(q,val );
			
			panelNote c=new panelNote(this.modelques,this.tab);
			c.getNoteMax().setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			c.getNoteMax().setValue(this.modelques.getListeQuestion().get(this.tab.getTableau().getSelectedRow()).getMaxVal());
			
			choix=(JPanel)c;
			
			this.add(choix);
			
		}
		else if(str.equals("Réponse libre")){
			this.modelques.getListeVal().put(q,val );
			
		}
		else{
			
			
			
			panelChoix c=new panelChoix(bd,this.modelques,tab);
			
			choix=(JPanel)c;
			
			c.getNbrReponse().setModel(new SpinnerNumberModel(new Integer(2), new Integer(2), null, new Integer(1)));
			c.getNbrReponse().setValue(this.modelques.getListeQuestion().get(tab.getTableau().getSelectedRow()).getMaxVal());
			c.getNbrReponse().setValue(2);
			c.getNbrReponse().setEnabled(true);
			this.add(choix);
		}
		
		this.revalidate();
		this.repaint();
		
	}

	public JTextArea getTextTitre() {
		return textTitre;
	}

	public void setTextTitre(JTextArea textTitre) {
		this.textTitre = textTitre;
	}

	public JLabel getLblQues() {
		return lblQues;
	}

	public void setLblQues(JLabel lblQues) {
		this.lblQues = lblQues;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public AccesBD getBd() {
		return bd;
	}

	public void setBd(AccesBD bd) {
		this.bd = bd;
	}




	public Tableau getTab() {
		return tab;
	}




	public void setTab(Tableau tab) {
		this.tab = tab;
	}
}
