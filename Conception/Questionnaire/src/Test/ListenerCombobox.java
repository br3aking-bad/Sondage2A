package Test;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;


public class ListenerCombobox implements ActionListener,FocusListener{
	private Tableau tableau;
	private CreerQuestionnaireVue creerQuestionnaireVue;
	private boolean ok=true;
	private int viewRow;
	private ModelQuestionnaire modelques;
	
	
	
	public ListenerCombobox(Tableau t,CreerQuestionnaireVue creerQuestionnaireVue, ModelQuestionnaire modelques) {
		
		this.tableau=t;
		this.creerQuestionnaireVue=creerQuestionnaireVue;
		this.modelques=modelques;
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		
		 viewRow = tableau.getTableau().getSelectedRow();
        if (viewRow >= 0) {
        	
                tableau.getTableau().convertRowIndexToModel(viewRow);
            	
           
        
        
        }
	
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		
		
	}

	@Override
	public void focusLost(FocusEvent arg0) {

		String typeQuestion=(String) tableau.getTableau().getValueAt(viewRow ,2).toString();
		creerQuestionnaireVue.getTypeQuestion().setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "Type question : "+typeQuestion, TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		this.modelques.getListeQuestion().get(viewRow).getTypeQuestion().setIntituleT(typeQuestion);
		
		
	}




}
