package Test;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CellRenderer extends JLabel implements TableCellRenderer {
    private Icon v;
    private Icon f;
 
    public CellRenderer() {
        super();
 
        f = new ImageIcon("no.png");
        v = new ImageIcon("yes.png");
    }
 

    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean isFocus, int row, int col) {
	   	
    	
    	 Boolean c = (Boolean)value;
    	 if(c){
             setIcon(v);
         } else {
             setIcon(f);
         }
	      return this;
	   }
}