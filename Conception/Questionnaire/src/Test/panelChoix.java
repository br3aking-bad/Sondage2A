package Test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Panel;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;


public class panelChoix extends JPanel{
	
	private JButton btnhaut,btnbas,buttonSupr,btnadd,btnsave,btnannuler ;
	private JList<ValPossible> list;
	private Question question;
	private AccesBD bd;
	private  JTextField intitule;
	private JSpinner nbrReponse;
	private DefaultListModel<ValPossible> propositions;
	private ModelQuestionnaire modelques;
	private Tableau tab;
	
	public panelChoix (AccesBD bd, ModelQuestionnaire modelques,Tableau tab){
		
		this.setBackground(new Color(211, 211, 211));
		this.setBounds(30, 100, 350, 220);
		this.setLayout(null);
		this.tab=tab;
		this.modelques=modelques;
		
		if(tab!=null && tab.getTableau().getSelectedRow()>=0){
			question=this.modelques.getListeQuestion().get(tab.getTableau().getSelectedRow());
			List<ValPossible> lisp=this.modelques.getListeVal().get(question.getNumQ());
			
			
				propositions=new DefaultListModel<ValPossible>();
				
				for(ValPossible val :lisp){
					propositions.addElement(val);
				}
				
				list = new JList<ValPossible>(propositions);
			
		}
		else{
		
		list = new JList<ValPossible>();
	
			
		
		}
			
			
			 
		
		 

		
		 list.setLayoutOrientation(JList.VERTICAL_WRAP);
		 list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		
		
		JScrollPane ascenseur = new JScrollPane(list);
		
		ascenseur.setBounds(45, 65, 280, 105);
		ascenseur.setBorder(new TitledBorder("Listes des propositions"));
		// On met l'ascenseur dans une panel en FlowLayout
		
		
		
		
		
		
		this.add(ascenseur);

		 btnhaut = new JButton("");
		btnhaut.setBounds(11, 65, 33, 46);
		this.add(btnhaut);
		btnhaut.setIcon(new ImageIcon("Fleche-haut-vert-icon.png"));
		
		
		 btnbas = new JButton("");
		btnbas.setBounds(11, 124, 33, 46);
		this.add(btnbas);
		btnbas.setIcon(new ImageIcon("Fleche-bas-vert-icon.png"));
		
		 buttonSupr = new JButton("");
		buttonSupr.setBounds(50, 178, 59, 23);
		this.add(buttonSupr);
		buttonSupr.setIcon(new ImageIcon("Actions-edit-delete-icon.png"));
		
		 btnadd = new JButton("");
		btnadd.setBounds(150, 178, 59, 23);
		this.add(btnadd);
		btnadd.setIcon(new ImageIcon("Actions-list-add-icon.png"));
		
		
		
		
		btnsave = new JButton("");
		btnsave.setBounds(250, 178, 59, 23);
		this.add(btnsave);
		btnsave.setIcon(new ImageIcon("Apps-session-suspend-icon.png"));
		
		
		
		
		
		
		
		intitule = new JTextField();
		intitule.setHorizontalAlignment(SwingConstants.LEFT);
	
		intitule.setColumns(20);
		intitule.setBounds(10, 20,280, 20);
		this.add(intitule);
		
		nbrReponse = new JSpinner();
		nbrReponse.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		nbrReponse.setToolTipText("Nombre de choix possible");
		nbrReponse.setEnabled(false);
		nbrReponse.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		nbrReponse.setBounds(310, 20, 40, 20);
		add(nbrReponse);
		
		
		
		
		btnhaut.addActionListener(new ControleurSaisieQuestion("Monter",this,this.question,propositions,this.modelques,tab));
		btnbas.addActionListener(new ControleurSaisieQuestion("Descendre",this,this.question,propositions,this.modelques,tab));
		btnadd.addActionListener(new ControleurSaisieQuestion("Ajouter",this,this.question,propositions,this.modelques,tab));
		buttonSupr.addActionListener(new ControleurSaisieQuestion("Retirer",this,this.question,propositions,this.modelques,tab));
		btnsave.addActionListener(new ControleurSaisieQuestion("Save",this,this.question,propositions,this.modelques,tab));
	}

	public JButton getBtnhaut() {
		return btnhaut;
	}

	public void setBtnhaut(JButton btnhaut) {
		this.btnhaut = btnhaut;
	}

	public JButton getBtnbas() {
		return btnbas;
	}

	public void setBtnbas(JButton btnbas) {
		this.btnbas = btnbas;
	}

	public JButton getButtonSupr() {
		return buttonSupr;
	}

	public void setButtonSupr(JButton buttonSupr) {
		this.buttonSupr = buttonSupr;
	}

	public JButton getBtnadd() {
		return btnadd;
	}

	public void setBtnadd(JButton btnadd) {
		this.btnadd = btnadd;
	}

	public JList<ValPossible> getList() {
		return list;
	}

	public void setList(JList<ValPossible> list) {
		this.list = list;
	}



	public AccesBD getBd() {
		return bd;
	}

	public void setBd(AccesBD bd) {
		this.bd = bd;
	}

	public JTextField getIntitule() {
		return intitule;
	}

	public void setIntitule(JTextField intitule) {
		this.intitule = intitule;
	}

	public JSpinner getNbrReponse() {
		return nbrReponse;
	}

	public void setNbrReponse(JSpinner nbrReponse) {
		this.nbrReponse = nbrReponse;
	}

}
