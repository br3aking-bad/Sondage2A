package Test;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JToggleButton;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

public class panelNote extends JPanel {
	private JTextField textField;

	private ModelQuestionnaire modelques;
	private JButton btnsave,btnannule;
	private Tableau tab;
	private JSpinner noteMax;
	
	public panelNote(ModelQuestionnaire modelques, Tableau tab) {
		this.setBackground(new Color(211, 211, 211));
		this.setToolTipText("");
		this.setBounds(5, 50, 380, 190);
		this.setLayout(null);
		setLayout(null);
		this.modelques=modelques;
		this.tab=tab;
		
		
		
		JLabel lblNoteMax = new JLabel("Note Max");
		lblNoteMax.setFont(new Font("Segoe Script", Font.PLAIN, 16));
		lblNoteMax.setBounds(10, 75, 98, 18);
		add(lblNoteMax);
		
		 noteMax = new JSpinner();
		 
		noteMax.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		noteMax.setValue(this.modelques.getListeQuestion().get(tab.getTableau().getSelectedRow()).getMaxVal());
		
		noteMax.setToolTipText("Nombre de choix possible");
		noteMax.setBounds(107, 75, 40, 20);
		add(noteMax);
		
		
		
		PanelNoteListener lst=new PanelNoteListener(this,this.modelques,this.tab);
		
		btnsave = new JButton("");
		btnsave.setBounds(190, 63, 59, 35);
		this.add(btnsave);
		btnsave.setIcon(new ImageIcon("Apps-session-suspend-icon.png"));
		
		btnannule = new JButton("");
		btnannule.setBounds(270, 63, 59, 35);
		this.add(btnannule);
		btnannule.setIcon(new ImageIcon("Go-back-icon.png"));
		
		
		btnsave.addActionListener(lst);
		btnannule.addActionListener(lst);
		this.add(btnsave);
		this.add(btnannule);
			
	}

	
	
	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public ModelQuestionnaire getModelques() {
		return modelques;
	}

	public void setModelques(ModelQuestionnaire modelques) {
		this.modelques = modelques;
	}

	public JButton getBtnsave() {
		return btnsave;
	}

	public void setBtnsave(JButton btnsave) {
		this.btnsave = btnsave;
	}

	public Tableau getTab() {
		return tab;
	}

	public void setTab(Tableau tab) {
		this.tab = tab;
	}

	public JSpinner getNoteMax() {
		return noteMax;
	}

	public void setNoteMax(JSpinner noteMax) {
		this.noteMax = noteMax;
	}



	public JButton getBtnannule() {
		return btnannule;
	}



	public void setBtnannule(JButton btnannule) {
		this.btnannule = btnannule;
	}
}
