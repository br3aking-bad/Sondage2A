package Test;

import java.awt.event.FocusEvent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;
public class ClientControleur implements FocusListener{
	ClientVue cv;
	ClientControleur(ClientVue cv){
		this.cv=cv;
	}
	public void focusGained(FocusEvent arg0){
    //System.out.println((Component)arg0.getSource() + " dit : " + "Bonjour, bienvenue chez moi");
		JTextField f=(JTextField)arg0.getSource();
		if (((Component)arg0.getSource()).getName()=="raison"){
			if (f.getText().equals("ex : Rapid'Sond")){
				cv.raison.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="adrs1"){
			if (f.getText().equals("ex : 15 Route des petits champs")){
				cv.adrs1.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="adrs2"){
			if (f.getText().equals("")){
				cv.adrs2.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="code"){
			if (f.getText().equals("45000 ")){
				cv.code.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="ville"){
			if (f.getText().equals("Orléans  ")){
				cv.ville.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="tel"){
			if (f.getText().equals("ex : 02.03.04.05.06")){
				cv.tel.setText("");
			}
		}
		if (((Component)arg0.getSource()).getName()=="mail"){
			if (f.getText().equals("ex : rapidSond@gmail.com")){
				cv.mail.setText("");
			}
		}
	 
  }
  public void focusLost (FocusEvent arg0){
    //System.out.println((Component)arg0.getSource() + " dit : " + "Au revoir ! A bientot !");
	if (((Component)arg0.getSource()).getName()=="raison"){
		if(cv.raison.getText().equals("")){
			cv.raison.setText("ex : Rapid'Sond");
		}
	}
	if (((Component)arg0.getSource()).getName()=="adrs1"){
		if(cv.adrs1.getText().equals("")){
			cv.adrs1.setText("ex : 15 Route des petits champs");
		}
	}
	if (((Component)arg0.getSource()).getName()=="adrs2"){
		if(cv.adrs2.getText().equals("")){
			cv.adrs2.setText("");
		}
	}
	if (((Component)arg0.getSource()).getName()=="code"){
		if(cv.code.getText().equals("")){
			cv.code.setText("45000 ");
		}
	}
	if (((Component)arg0.getSource()).getName()=="ville"){
		if(cv.ville.getText().equals("")){
			cv.ville.setText("Orléans  ");
		}
	}
	if (((Component)arg0.getSource()).getName()=="tel"){
		if(cv.tel.getText().equals("")){
			cv.tel.setText("ex : 02.03.04.05.06");
		}
	}
	if (((Component)arg0.getSource()).getName()=="mail"){
		if(cv.mail.getText().equals("")){
			cv.mail.setText("ex : rapidSond@gmail.com");
		}
	}
  }
}
