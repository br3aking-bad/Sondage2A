package Test;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ControleurItemAccueilVue implements  ItemListener {

	static int selectedQuestion=-1;
	AcceuilVue vue;
	
	
	public ControleurItemAccueilVue(String nomBouton, AcceuilVue vue) {
		super();
		this.vue = vue; //initialisation des propriétés de la classe
	}

	@Override
	public void itemStateChanged(ItemEvent arg0) {
		if (arg0.getStateChange()==ItemEvent.SELECTED){
			// mise à jour de la question sélectionnée (propriété selectedQuestion de fQuestionnaire)
			vue.selected=Integer.parseInt((arg0.getItemSelectable().toString()));
		}
	}


}