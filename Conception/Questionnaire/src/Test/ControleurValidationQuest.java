package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class ControleurValidationQuest implements ActionListener {
	static final int TERMINER=0;
	static final int ANNULER=1;
	static final int TEXTE=4;
	private int idBouton;
	private CreerQuestionnaireVue creerQuestionnaireVue;
	private AccesBD bd;
	private Questionnaire questionnaire;
	private Application ap;
	
	
	public ControleurValidationQuest(String nomBouton, CreerQuestionnaireVue creerQuestionnaireVue, AccesBD bd, Questionnaire questionnaire, Application ap) {
		super();
		this.bd=bd;
		this.creerQuestionnaireVue=creerQuestionnaireVue;
		
		this.questionnaire=questionnaire;
		this.ap=ap;
	
		switch (nomBouton){
		case "Terminer": this.idBouton=TERMINER;break;
		case "Annuler": this.idBouton=ANNULER;break;
		case "Texte": this.idBouton=TEXTE;break;
		default: this.idBouton=-1;
	
		}
	}

	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		switch (idBouton){
		case TERMINER: terminer();break;
		case ANNULER: annuler();break;
		
		}
	}



	private void annuler() {
		// TODO Auto-generated method stub
		this.creerQuestionnaireVue.activeDesactiveHaut(true);
		this.creerQuestionnaireVue.activeDesactiveBas(false);
	}



	private void terminer() {
		// TODO Auto-generated method stub
		int indSelect=this.creerQuestionnaireVue.list_panel.getSelectedIndex();
		if(indSelect!=-1){
			this.questionnaire.setEtat('S');
			
			this.questionnaire.setPanel(this.creerQuestionnaireVue.list_panel.getSelectedValue());
			this.bd.updateQuestionnaire(this.questionnaire);
			JOptionPane.showMessageDialog(null,"Le questionnaire est envoyé pour le module sondage   ");
			this.ap.changerVue(this.ap.vuePrinc, 880,401);
		}else{
			JOptionPane.showMessageDialog(null,"Choissisez un panel");
		}
	}

}
