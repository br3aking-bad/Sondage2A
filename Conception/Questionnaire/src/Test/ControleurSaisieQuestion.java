package Test;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;



// ici le controleur va à la fois implémenter l'ActionListener des bouton et le FocusListener de JTextField
public class ControleurSaisieQuestion implements ActionListener {
	static final int AJOUTER=0;
	static final int RETIRER=1;
	static final int MONTER=2;
	static final int DESCENDRE=3;
	static final int Save=5;
	static final int TEXTE=4;
	private DefaultListModel<ValPossible> propositions;
	private ModelQuestionnaire modelques;
	private int idBouton;
	private panelChoix vue; // la vue
	private Question laQuestion; // le Modèle
	private Tableau tab;
	
	public ControleurSaisieQuestion(int idBouton, panelChoix vue, Question modele) {
		super();
		this.idBouton = idBouton;
		this.vue=vue;
		this.laQuestion=modele;
	}
	
	public ControleurSaisieQuestion(String nomBouton, panelChoix vue, Question modele, DefaultListModel<ValPossible> propositions, ModelQuestionnaire modelques, Tableau tab) {
		super();
		this.vue=vue;
		this.laQuestion=modele;
		this.propositions=propositions;
		this.modelques=modelques;
		this.tab=tab;
		switch (nomBouton){
		case "Ajouter": this.idBouton=AJOUTER;break;
		case "Retirer": this.idBouton=RETIRER;break;
		case "Monter": this.idBouton=MONTER;break;
		case "Descendre": this.idBouton=DESCENDRE;break;
		case "Save": this.idBouton=Save;break;
		case "Cancel": this.idBouton=Save;break;
		case "Texte": this.idBouton=TEXTE;break;
		default: this.idBouton=-1;
		}
	}

	// méthodes utilitaires pour les boutons
	private void ajouter(){
		
		String texte=vue.getIntitule().getText();
		if(!texte.isEmpty()){
			ValPossible val=new ValPossible(laQuestion,this.tab.getTableau().getSelectedRow(),texte);
			propositions.addElement(val);
			
			
		}
		vue.getIntitule().setText("");
		

	}
	
	private void retirer(){
		
		
		int indSelection=vue.getList().getSelectedIndex();
		if (indSelection!=-1)
			propositions.removeElementAt(indSelection);
			

	}
	private void monter(){
		
		
		int indSelection=vue.getList().getSelectedIndex();
		if (indSelection>0){
			ValPossible val=propositions.getElementAt(indSelection);
			propositions.removeElementAt(indSelection);
			propositions.add(indSelection-1, val);
			vue.getList().setSelectedIndex(indSelection-1);
			vue.getList().ensureIndexIsVisible(indSelection-1);
		}
		

	}
	private void descendre(){
		
		
		int indSelection=vue.getList().getSelectedIndex();
		if (indSelection!=-1 && indSelection < propositions.getSize()-1){
			ValPossible val=propositions.getElementAt(indSelection);
			propositions.removeElementAt(indSelection);
			propositions.add(indSelection+1, val);
			vue.getList().setSelectedIndex(indSelection+1);
			vue.getList().ensureIndexIsVisible(indSelection+1);
		}

		
	}
	
	// événement de type actionPerformed sur les boutons
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		switch (idBouton){
		case AJOUTER: ajouter();break;
		case RETIRER: retirer();break;
		case MONTER: monter();break;
		case DESCENDRE: descendre();break;
		case Save: Save();break;
		}
	}

	private void Save() {
		List<ValPossible> val=new ArrayList<ValPossible>();
		
		for(int i=0;i<propositions.size();i++){
			val.add(propositions.getElementAt(i));
		}
		
		this.modelques.getListeVal().put(laQuestion.getNumQ(), val);
		
		modelques.getListeQuestion().get(tab.getTableau().getSelectedRow()).setMaxVal((int) vue.getNbrReponse().getValue());
		
		
		JOptionPane.showMessageDialog(null, "Vos modification est enregistrer");
		
		this.modelques.Affiche1();
	}

	
}
