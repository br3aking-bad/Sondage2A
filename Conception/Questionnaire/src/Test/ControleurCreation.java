package Test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;


public class ControleurCreation implements ActionListener{

	static final int AJOUTERCL=0;
	static final int AJOUTERQU=1;

	public int idBouton;
	public AcceuilVue vue;

	public Application application;
	public Utilisateur utilisateur;
	
	public ControleurCreation(int b, Application application, AcceuilVue vue, Utilisateur utilisateur){
		super();
		this.application = application;
		this.utilisateur=utilisateur;
		this.vue = vue;
		
		this.idBouton=b;
	}
	
	
	private void ajouterCl(){
		// création d'un nouveau client
		application.changerVue(application.vueClient,480,580);
	}
	
	private void ajouterQu(){
		// création d'une nouvelle question
		
		
		
		
		if(vue.selected!=-1){
			Panel p=application.a.getPanel(1);
			Questionnaire ques =new Questionnaire("xxxxxxxxxxxxxxx", 'C',vue.lCl.get(vue.selected),utilisateur,p);
			vue.application.a.insertQuestionnaire(ques);
			CreerQuestionnaireVue q=new CreerQuestionnaireVue(application.a, ques,this.application);
			application.changerVue(q, 1150, 616);
		}
		else{
			JOptionPane.showMessageDialog(null,"choisissez un Client  ");
		}
	}
	

	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(idBouton){
		case 0: ajouterCl();break;
		case 1: ajouterQu();break;
		}
	}
	
	

}
