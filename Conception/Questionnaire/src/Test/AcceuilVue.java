package Test;


import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;



@SuppressWarnings("serial")
public class AcceuilVue extends JPanel {
	
	public JButton Supprimer;
	public JButton Modifier;
	public JLabel quesDeClient;
	
	public Application application;
	
	public JPanel panelQuest;
	public JPanel panelBoutons;
	public JPanel panelClient;
	
	public JList<Questionnaire> list_Ques;
	public JList<Client> list_Client;
	
	
	public ControleurListQuest clq;
	public ControleurCreation creation;
	public ControleurCreation creation2;
	public DefaultListModel<Client> lCl;
	public DefaultListModel<Questionnaire> lQl;
	public  Utilisateur utilisateur;
	public String etat="Creer";
	
	int selected;
	

	
	public void changerVue(JPanel vue){
		Container c = this.getRootPane();
		c.removeAll();
		c.add(vue);
		this.revalidate();
		this.repaint();
	}

	@SuppressWarnings("unchecked")
	public AcceuilVue(Application application, AccesBD a, Utilisateur utilisateur) {
		this.application=application;
		this.utilisateur=utilisateur;
		this.setBounds(0, 0, 880, 401);
		this.setLayout(null);
		this.selected=-1;
	
		this.clq = new ControleurListQuest(selected, application, this);
		this.creation = new ControleurCreation(0,application, this, this.utilisateur);
		this.creation2 = new ControleurCreation(1,application, this, this.utilisateur);
		

		// Le panel pour la liste des clients 
		panelClient = new JPanel();
		
		//la taille du panel
		panelClient.setBounds(24, 73, 374, 200);
		
		// ajouter le panel dans le fenetre
		this.add(panelClient);
		
		//specifier le type du panel
		panelClient.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// le scrole du liste du client
		JScrollPane scrollPane2 = new JScrollPane();
		
		// ajoute le scrole dans le panel
		panelClient.add(scrollPane2);
		
		
		//init mdq
	lCl=new DefaultListModel<Client>() ;
	for (Client c : a.getClients()){
		lCl.addElement(c);
	}

		// creer la liste des clients
		list_Client  = new JList<Client>(lCl);
		

		//ajout d'un listener
		list_Client.addListSelectionListener(new ListennerSelectionClient(application));
		
		//creation du listener
		list_Client.addFocusListener(clq);
		
		// la taille de la liste des clients
		list_Client.setPreferredSize(new Dimension(350,200));
		
		list_Client.setVisibleRowCount(10);
		list_Client.setFixedCellHeight(12);
		list_Client.setFixedCellWidth(250);
		
		// Le border de la liste
		list_Client.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		// ajouter la liste dans le scroll pane
		scrollPane2.setViewportView(list_Client);
		
		//ajouter Le model de la list 
//		list_Client.setModel(modeleCl);
		
		quesDeClient = new JLabel("Le client est encore lié a une question");
		quesDeClient.setForeground(Color.red);
		quesDeClient.setVisible(false);
		panelClient.add(quesDeClient);
		
		// creer Le header ou le titre de la liste
		JLabel lblNewLabel2 = new JLabel("Liste des Clients");
		scrollPane2.setColumnHeaderView(lblNewLabel2);
		lblNewLabel2.setFont(new Font("DejaVu Serif Condensed", Font.PLAIN, 17));
		lblNewLabel2.setBackground(Color.BLACK);
				
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// le panel pour mettre les boutons modifier- supprimer -se deplacer dans la liste
		panelBoutons = new JPanel();
		panelBoutons.setBounds(400, 73, 69, 188);
		
		panelBoutons.setLayout(null);
		
		// ajouter Bouton pour le haut
		//Haut = new JButton(new ImageIcon("Fleche-haut-vert-icon.png"));
		//Haut.setBounds(0, 0, 69, 50);
		//Haut.addActionListener(new ControleurListQuest("Haut",this,modeleQuest,modeleCl));
		//panelBoutons.add(Haut);
		
		// ajouter bouton supprimer
		Supprimer = new JButton("");
		
		Supprimer.setIcon(new ImageIcon("Actions-edit-delete-icon.png"));
		Supprimer.setBounds(0, 49, 69, 47);
		Supprimer.addActionListener(new ControleurListQuest("Supprimer",this,this.application));
		panelBoutons.add(Supprimer);
		
		//ajouter bouton modifier
		
		Modifier = new JButton(new ImageIcon("Actions-document-edit-icon.png"));
		Modifier.setBounds(0, 95, 69, 47);
		Modifier.addActionListener(new ControleurListQuest("Modifier",this,this.application));
		panelBoutons.add(Modifier);
		
		//ajouter bouton descendre
		//bas = new JButton(new ImageIcon("Fleche-bas-vert-icon.png"));
		//bas.setBounds(0, 141, 69, 47);
		//bas.addActionListener(new ControleurListQuest("bas",this,modeleQuest,modeleCl));
		//panelBoutons.add(bas);
		
		this.add(panelBoutons);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		// Le Panel pour la liste Questionnaire 
		panelQuest = new JPanel();
		
		//Sa taille
		panelQuest.setBounds(500, 73, 295, 188);
		
		// j'ajoute le panel dans le frame
		this.add(panelQuest);
		
		// preciser le type de layout du panel
		panelQuest.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// le scrole du liste de questionnaire
		JScrollPane scrollPane = new JScrollPane();
		
		// ajoute le scrole dans le panel
		panelQuest.add(scrollPane);
		
		lQl=new DefaultListModel<Questionnaire>();
		// creeer une liste
		
		list_Ques = new JList<Questionnaire>(lQl);
		
		//creation du listener
		list_Ques.addFocusListener(clq);
		
		//Le border de la liste
		list_Ques.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		//taille
		// ajouter la liste dans le scroll pane
		scrollPane.setViewportView(list_Ques);
		
		//ajouter Le model de la list 
		//list_Ques.setModel(modeleQuest);
				
		//definir la taille du liste
		list_Ques.setPreferredSize(new Dimension(350,200));
		list_Ques.setVisibleRowCount(10);
		list_Ques.setFixedCellHeight(12);
		list_Ques.setFixedCellWidth(250);

		// creer Le header ou le titre de la liste
		JLabel labelques = new JLabel("Liste des questionnaires");
		scrollPane.setColumnHeaderView(labelques);
		labelques.setFont(new Font("DejaVu Serif Condensed", Font.PLAIN, 17));
		labelques.setBackground(Color.BLACK);




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		//Le panel pour bouton ajoute questionnaire	
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(113, 284, 171, 39);
		this.add(panel_3);
		
		//le panel pour bouton ajoute Clients	
		JPanel panel_4 = new JPanel();
		panel_4.setBounds(557, 284, 171, 39);
		this.add(panel_4);
		
		JButton btnClients = new JButton("Client");
		
		btnClients.setIcon(new ImageIcon("Actions-list-add-icon.png"));
		btnClients.addActionListener(creation);
		panel_3.add(btnClients);
		
		JButton  btnQuestionnaire= new JButton("Questionnaire");
		btnQuestionnaire.setIcon(new ImageIcon("Actions-list-add-icon.png"));
		btnQuestionnaire.setFont(new Font("Tekton Pro Ext", Font.PLAIN, 15));
		panel_4.add(btnQuestionnaire);
		btnQuestionnaire.addActionListener(creation2);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

	}
}
