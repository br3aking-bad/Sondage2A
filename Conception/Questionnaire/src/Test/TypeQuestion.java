package Test;
public class TypeQuestion {

	private Character idT;
	private String intituleT;
	private String typeReponse;
	
	/**
	 * @param idT
	 * @param intituleT
	 * @param typeReponse
	 */
	public TypeQuestion(Character idT,String intituleT,String typeReponse)
	{
		this.idT=idT;
		this.intituleT=intituleT;
		this.typeReponse=typeReponse;
	}

	/**
	 * @return
	 */
	public Character getIdT() {
		return idT;
	}

	/**
	 * @param idT
	 */
	public void setIdT(Character idT) {
		this.idT = idT;
	}

	/**
	 * @return
	 */
	public String getIntituleT() {
		return intituleT;
	}

	/**
	 * @param intituleT
	 */
	public void setIntituleT(String intituleT) {
		this.intituleT = intituleT;
	}

	/**
	 * @return
	 */
	public String getTypeReponse() {
		return typeReponse;
	}

	/**
	 * @param typeReponse
	 */
	public void setTypeReponse(String typeReponse) {
		this.typeReponse = typeReponse;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TypeQuestion [idT=" + idT + ", intituleT=" + intituleT
				+ ", typeReponse=" + typeReponse + "]";
	}
	
	
}
