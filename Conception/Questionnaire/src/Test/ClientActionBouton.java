package Test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.BorderFactory;

public class ClientActionBouton implements ActionListener{
	//FormulaireQuestionAvecValidation fQuestionAvecVal; // c'est la vue
	ClientVue vue;
	AcceuilVue vuePrinc;
	
	
	public ClientActionBouton(ClientVue vue, AcceuilVue vuePrinc){//FormulaireQuestionAvecValidation fQuestionAvecVal,Questionnaire leQuestionnaire) {
		super();
		// initialisation des propriétés
		//this.fQuestionAvecVal = fQuestionAvecVal;
		this.vue = vue;
		this.vuePrinc = vuePrinc;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		ArrayList<String> exception = new ArrayList<String>();
		exception.add("!");exception.add("<");exception.add(">");
		exception.add("?");exception.add(":");
		exception.add("\\");exception.add("*");exception.add("|");

		if (arg0.getActionCommand()=="Valider"){
			boolean ok=true;
			try{
				if(vue.code.getText().length()==5){
					try{
						Integer.parseInt(vue.code.getText());
					}
					catch(NumberFormatException e){
						this.vue.code.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
					}
					this.vue.code.setBorder(BorderFactory.createLineBorder(Color.green));
				}
				else{
					this.vue.code.setBorder(BorderFactory.createLineBorder(Color.red));
					ok=false;
				}
				
				if(vue.tel.getText().length()==10 && vue.tel.getText().charAt(0)=='0'){
					this.vue.tel.setBorder(BorderFactory.createLineBorder(Color.green));
				}
				else{
					this.vue.tel.setBorder(BorderFactory.createLineBorder(Color.red));
					ok=false;
				}
				
				for(int i=0;i<exception.size();i++){
					if(vue.mail.getText().contains("@") && vue.mail.getText().contains(".") && !vue.mail.getText().contains(exception.get(i))){
						this.vue.mail.setBorder(BorderFactory.createLineBorder(Color.green));
					}
					else{
						this.vue.mail.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
						break;
					}
				}
				
				for(int i=0;i<exception.size();i++){
					if(!vue.ville.getText().equals("Orléans  ") && !vue.ville.getText().contains(exception.get(i))){
						this.vue.ville.setBorder(BorderFactory.createLineBorder(Color.green));
					}
					else{
						this.vue.ville.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
						break;
					}
				}
				
				for(int i=0;i<exception.size();i++){
					if(!vue.adrs1.getText().equals("ex : 15 Route des petits champs") && !vue.adrs1.getText().contains(exception.get(i))){
						this.vue.adrs1.setBorder(BorderFactory.createLineBorder(Color.green));
					}
					else{
						this.vue.adrs1.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
						break;
					}
				}
				
				for(int i=0;i<exception.size();i++){
					if(!vue.adrs2.getText().contains(exception.get(i))){
						this.vue.adrs2.setBorder(BorderFactory.createLineBorder(Color.green));
					}
					else{
						this.vue.adrs2.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
						break;
					}
				}
				
				for(int i=0;i<exception.size();i++){
					if(!vue.raison.getText().equals("ex : Rapid'Sond") && !vue.raison.getText().contains(exception.get(i))){
						this.vue.raison.setBorder(BorderFactory.createLineBorder(Color.green));
					}
					else{
						this.vue.raison.setBorder(BorderFactory.createLineBorder(Color.red));
						ok=false;
						break;
					}
				}
				
				if(ok){
					
					Client c = new Client(vue.raison.getText(), vue.adrs1.getText(), vue.adrs2.getText(), Integer.parseInt(vue.code.getText()), vue.ville.getText(), vue.tel.getText(), vue.mail.getText());
					
					
					
					
					
					if(this.vuePrinc.etat.equals("Creer")){
						
						c.setNumC(vue.application.a.maxNumClient()+1);
						vue.application.vuePrinc.lCl.add(0, c);
						vue.application.a.insertClient(c);
					}
					else{
						
						c.setNumC(vue.application.a.maxNumClient());
						vue.application.vuePrinc.lCl.set(this.vuePrinc.list_Client.getSelectedIndex(), c);
						vue.application.a.updateClient(c);
					}
				
					
					
					
					
					
					vue.application.changerVue(vuePrinc,880,401);
					vue.vuePrinc.list_Client.validate();
					vue.vuePrinc.list_Client.repaint();
				}
				else{
					ok=true;
				}
				vue.vuePrinc.list_Ques.setPreferredSize(new Dimension(350,vue.vuePrinc.list_Ques.getWidth()+10));
			}
			catch(Exception e){
			}
		}
		
		if (arg0.getActionCommand()=="Annuler"){
			// on veut tout annuler => on ne fait rien puis
			// on change la vue de l'application
			vue.application.changerVue(vuePrinc,880,401);
		}

	}

}
