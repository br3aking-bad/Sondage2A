
package Test;

public class Question {

	private Questionnaire questionnaire;
	private int numQ;
	private String texteQ;
	private int maxVal;
	private TypeQuestion typeQuestion;
	
	/**
	 * @param questionnaire
	 * @param numQ
	 * @param texteQ
	 * @param maxVal
	 * @param typeQuestion
	 */
	public Question(Questionnaire questionnaire,int numQ,String texteQ,int maxVal,TypeQuestion typeQuestion)
	{
		
		this.numQ=numQ;
		this.questionnaire=questionnaire;
		this.texteQ=texteQ;
		this.maxVal=maxVal;
		this.typeQuestion=typeQuestion;
		
	}
	/**
	 * @param questionnaire
	 * @param texteQ
	 * @param maxVal
	 * @param typeQuestion
	 */
	public Question(Questionnaire questionnaire,String texteQ,int maxVal,TypeQuestion typeQuestion)
	{
		this.numQ=-1;
		this.questionnaire=questionnaire;
		this.texteQ=texteQ;
		this.maxVal=maxVal;
		this.typeQuestion=typeQuestion;
	}

	/**
	 * @return
	 */
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	/**
	 * @param questionnaire
	 */
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	/**
	 * @return
	 */
	public int getNumQ() {
		return numQ;
	}

	/**
	 * @param numQ
	 */
	public void setNumQ(int numQ) {
		this.numQ = numQ;
	}

	/**
	 * @return
	 */
	public String getTexteQ() {
		return texteQ;
	}

	/**
	 * @param texteQ
	 */
	public void setTexteQ(String texteQ) {
		this.texteQ = texteQ;
	}

	/**
	 * @return
	 */
	public int getMaxVal() {
		return maxVal;
	}

	/**
	 * @param maxVal
	 */
	public void setMaxVal(int maxVal) {
		this.maxVal = maxVal;
	}

	/**
	 * @return
	 */
	public TypeQuestion getTypeQuestion() {
		return typeQuestion;
	}

	/**
	 * @param typeQuestion
	 */
	public void setTypeQuestion(TypeQuestion typeQuestion) {
		this.typeQuestion = typeQuestion;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Question [questionnaire=" + questionnaire + ", numQ=" + numQ
				+ ", texteQ=" + texteQ + ", maxVal=" + maxVal
				+ ", typeQuestion=" + typeQuestion + "]";
	}

	
}
