package Test;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Font; 

public class ClientVue extends JPanel {
	AcceuilVue vuePrinc;
	
	JTextField num, raison, adrs1, adrs2, code, ville, tel, mail;
	String[] saisies;
	Application application;

	public ClientVue(Application application) {
		this.application = application;
		//super("RapidSond - Conception | Création | Client : MisterX");
		//this.saisies=saisies;
		this.setBounds(0, 0, 480, 580);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Container cont=this.getContentPane();
		this.setLayout(new BorderLayout());
		// dimension de la fenetre non changeable
		//this.setResizable(false);
		this.vuePrinc=application.vuePrinc;
 		
		// Le panel pour le numéro du client
		JPanel numClient = new JPanel();
		// placement et dimension de numClient
		numClient.setBounds(50,20,300,50);
		// Titre de la case
		numClient.setBorder(new TitledBorder("Numéro du client : *"));
		// zone de texte
		
		num = new JTextField(application.a.maxNumClient()+1+" ");
		// mettre le texte en italic
		Font newTextFieldFont=new Font(num.getFont().getName(),Font.ITALIC,num.getFont().getSize());  
		// couleur de texte en gris clair
		num.setForeground(Color.gray);
		// ajout zone de texte
		num.setEditable(false);
		numClient.add(num);
		// pour que le bord du JTextField soit invisible
		num.setBorder(null);
		// zone de texte plus longue 
		num.setPreferredSize(new Dimension(291,24));
		num.setFont(newTextFieldFont);  
		num.setHorizontalAlignment(JTextField.CENTER); 
		// donne un nom au jtextfield
		num.setName("numero"); 
		// nouveau controleur 
		ClientControleur cc= new ClientControleur(this);
		// ajout du controleur au jtextfield
		num.addFocusListener(cc);
		
		
		// Le panel pour la raison sociale du client
		JPanel raisonSoc = new JPanel();
		// placement et dimension de raisonSoc
		raisonSoc.setBounds(50,80,300,50);
		// Titre de la case
		raisonSoc.setBorder(new TitledBorder("Raison sociale : *"));
		// zone de texte
		raison = new JTextField("ex : Rapid'Sond");
		// texte en italic
		Font newTextFieldFont1=new Font(raison.getFont().getName(),Font.ITALIC,raison.getFont().getSize());  
		// texte en gris
		raison.setForeground(Color.gray);
		// ajout zone de texte
		raisonSoc.add(raison);
		// pour que le bord du JTextField soit invisible
		raison.setBorder(null);
		// zone de texte plus longue 
		raison.setPreferredSize(new Dimension(291,24));
		raison.setFont(newTextFieldFont1);  
		// donne nom au jtextfield
		raison.setName("raison"); 
		// nouveau controleur
		ClientControleur cc1= new ClientControleur(this);
		// ajout du controleur au jtextfield
		raison.addFocusListener(cc1);
		
		
		// Le panel pour la 1ère adresse du client 
		JPanel Adresse1 = new JPanel();
		// placement et dimension de Adresse1
		Adresse1.setBounds(50,160,400,50);
		// Titre de la case
		Adresse1.setBorder(new TitledBorder("Adresse n°1 : *"));
		// zone de texte
		adrs1 = new JTextField("ex : 15 Route des petits champs");
		Font newTextFieldFont2=new Font(adrs1.getFont().getName(),Font.ITALIC,adrs1.getFont().getSize());  
		adrs1.setForeground(Color.gray);
		// ajout zone de texte
		Adresse1.add(adrs1);
		// pour que le bord du JTextField soit invisible
		adrs1.setBorder(null);
		// zone de texte plus longue 
		adrs1.setPreferredSize(new Dimension(391,24));
		adrs1.setFont(newTextFieldFont2);  
		// donne nom au jtextfield
		adrs1.setName("adrs1"); 
		// nouveau controleur
		ClientControleur cc2= new ClientControleur(this);
		// ajout du controleur au jtextfield
		adrs1.addFocusListener(cc2);
		
		
		// Le panel pour la 2ème adresse du client
		JPanel Adresse2 = new JPanel();
		//placement et dimension de Adresse245100
		Adresse2.setBounds(50,220,400,50);
		// Titre de la case
		Adresse2.setBorder(new TitledBorder("Adresse n°2 : "));
		// zone de texte
		adrs2 = new JTextField("");
		// texte en italic
		Font newTextFieldFont3=new Font(adrs2.getFont().getName(),Font.ITALIC,adrs2.getFont().getSize());  
		// texte en gris
		adrs2.setForeground(Color.gray);
		// ajout zone de texte
		Adresse2.add(adrs2);
		// pour que le bord du JTextField soit invisible
		adrs2.setBorder(null);
		// zone de texte plus longue 
		adrs2.setPreferredSize(new Dimension(391,24));
		adrs2.setFont(newTextFieldFont3);  
		// donne nom au jtextfield
		adrs2.setName("adrs2"); 
		// nouveau controleur
		ClientControleur cc3= new ClientControleur(this);
		// ajout du controleur au jtextfield
		adrs2.addFocusListener(cc3);

		
		// Le panel pour le code postal du client
		JPanel CodePos = new JPanel();
		//placement et dimension de CodePos
		CodePos.setBounds(50,300,150,50);
		// Titre de la case
		CodePos.setBorder(new TitledBorder("Code postal : *"));
		// zone de texte
		code = new JTextField("45000 ");
		// texte en italic
		Font newTextFieldFont4=new Font(code.getFont().getName(),Font.ITALIC,code.getFont().getSize()); 
		// texte en gris
		code.setForeground(Color.gray); 
		// ajout zone de texte
		CodePos.add(code);
		// pour que le bord du JTextField soit invisible
		code.setBorder(null);
		code.setFont(newTextFieldFont4);  
		// texte centrée par rapport a la case
		code.setHorizontalAlignment(JTextField.CENTER); 
		// donne nom au jtextfield
		code.setName("code"); 
		// nouveau controleur
		ClientControleur cc4= new ClientControleur(this);
		// ajout du controleur au jtextfield
		code.addFocusListener(cc4);
		
		
		// Le panel pour la ville du client
		JPanel Ville = new JPanel();
		// placement et dimension de Ville
		Ville.setBounds(250,300,200,50);
		// Titre de la case
		Ville.setBorder(new TitledBorder("Ville : *"));
		// zone de texte
		ville = new JTextField("Orléans  ");
		// texte en italic
		Font newTextFieldFont5=new Font(ville.getFont().getName(),Font.ITALIC,ville.getFont().getSize());  
		// texte en gris
		ville.setForeground(Color.gray);
		// ajout zone de texte
		Ville.add(ville);
		// pour que le bord du JTextField soit invisible
		ville.setBorder(null);
		// zone de texte plus longue 
		ville.setPreferredSize(new Dimension(191,24));
		ville.setFont(newTextFieldFont5); 
		// donne nom au jtextfield 
		ville.setName("ville"); 
		// nouveau controleur
		ClientControleur cc5= new ClientControleur(this);
		// ajout du controleur au jtextfield
		ville.addFocusListener(cc5);
		
		
		// Le panel pour le numéro de téléphone du client
		JPanel NumTel = new JPanel();
		// placement et dimension de NumTel
		NumTel.setBounds(50,370,150,50);
		// Titre de la case
		NumTel.setBorder(new TitledBorder("N° Téléphone : *"));
		// zone de texte
		tel = new JTextField("ex : 02.03.04.05.06");
		// texte en italic
		Font newTextFieldFont6=new Font(tel.getFont().getName(),Font.ITALIC,tel.getFont().getSize());  
		// texte en gris
		tel.setForeground(Color.gray);
		// ajout zone de texte
		NumTel.add(tel);
		// pour que le bord du JTextField soit invisible
		tel.setBorder(null);
		// zone de texte plus longue 
		tel.setPreferredSize(new Dimension(141,24));
		tel.setFont(newTextFieldFont6);  
		// donne nom au jtextfield
		tel.setName("tel"); 
		// nouveau controleur
		ClientControleur cc6= new ClientControleur(this);
		// ajout du controleur au jtextfield
		tel.addFocusListener(cc6);
		
		// Le panel pour l'adresse mail du client
		JPanel Mail = new JPanel();
		// placement et dimension de Mail
		Mail.setBounds(250,370,200,50);
		// Titre de la case
		Mail.setBorder(new TitledBorder("Adresse email : *"));
		// zone de texte
		mail = new JTextField("ex : rapidSond@gmail.com");
		// texte en italic
		Font newTextFieldFont7=new Font(mail.getFont().getName(),Font.ITALIC,mail.getFont().getSize());  
		// texte en gris
		mail.setForeground(Color.gray);
		// ajout zone de texte
		Mail.add(mail);
		// pour que le bord du JTextField soit invisible
		mail.setBorder(null);
		// zone de texte plus longue 
		mail.setPreferredSize(new Dimension(191,24));
		mail.setFont(newTextFieldFont7);  
		// donne nom au jtextfield
		mail.setName("mail"); 
		// nouveau controleur
		ClientControleur cc7= new ClientControleur(this);
		// ajout du controleur au jtextfield
		mail.addFocusListener(cc7);
		
		// panel pour tous les textes
		JPanel center= new JPanel();
		center.setLayout(null);
		// ajout des jpanel au jpanel center
		center.add(numClient);
		center.add(raisonSoc);
		center.add(Adresse1);center.add(Adresse2);
		center.add(CodePos);
		center.add(Ville);
		center.add(NumTel);
		center.add(Mail);
		// ajout du panel center au container
		this.add(center,"Center");

		// création bouton annuler 
		JButton annuler=new JButton("Annuler");
		ClientActionBouton act = new ClientActionBouton(this, vuePrinc);
		annuler.addActionListener(act);
		// jpanel bouton 
		JPanel bouton=new JPanel();
		
		// ajout du jpanel au container
		this.add(bouton,"South");
		// ajout du bouton annuler au jpanel
		bouton.add(annuler);
		annuler.setBackground(Color.RED);

		
		// création bouton valider
		JButton valider=new JButton("Valider");
		valider.addActionListener(act);
		valider.setBackground(Color.GREEN);
		// ajout du bouton validerau jpanel
		bouton.add(valider);
	
		
	}
}
