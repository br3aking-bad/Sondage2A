package Test;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;








public class Tableau  {
	  private  JTable tableau;  // le tableau
	  private String[] comboData = {"Choix unique","Choix multiple","Classement", "Note", "Réponse libre"}; // le combobox pour le choix type du question
	  private JComboBox combo;
	  private  JButton supp=new JButton(""); // bouton pour supprimer une ligne
	  private  JButton modif=new JButton("");//bouton pour modifier une ligne
	  private panelQuestion panel; //la vue des types de questions 
	  private ZModel model; //le model du tableau
	  private AccesBD bd; // la base de donner
	  private CreerQuestionnaireVue creerQuestionnaireVue;
	  public String  typeQuestion;
	  private ModelQuestionnaire modelques;
	  private Questionnaire questionnaire;
	  
	  
	 // public CreerQuestionnaireVue vue;
	  
	  public Tableau(AccesBD bd,panelQuestion p, CreerQuestionnaireVue creerQuestionnaireVue, ModelQuestionnaire modelques, Questionnaire questionnaire){
		// this.vue=vue;
		  typeQuestion="Choix Simple";
	    //Les données du tableau
	     this.bd=bd;
	  this.modelques=modelques;
	    this.panel=p;
	    JButton b=new JButton();
	    b.setIcon(new ImageIcon("Actions-edit-delete-icon.png"));
	    this.creerQuestionnaireVue=creerQuestionnaireVue;
	    this.questionnaire=questionnaire;


	    
	    combo =new  JComboBox(comboData);
	    //Les titres des colonnes
	    String  title[] = {"ID", "Titre", "Type","Sup"};
	    
	    
	    
	     model = new ZModel(bd, title,tableau,comboData,this.modelques,this.questionnaire);
	    
	    this.tableau = new JTable(model);
	    TableColumn col = this.tableau.getColumnModel().getColumn(0);
	    TableColumn col2 = this.tableau.getColumnModel().getColumn(2);
	    TableColumn col6 = this.tableau.getColumnModel().getColumn(1);
	    
	    TableColumn col7 = this.tableau.getColumnModel().getColumn(3);
	    col6.setPreferredWidth(350);
	    col.setPreferredWidth(69);
	    col2.setPreferredWidth(100);
	  
	    col7.setPreferredWidth(10);
	    
	    this.tableau.setBackground(new Color(230, 230, 230));
	    
	   this.tableau.setBorder(BorderFactory.createLoweredBevelBorder());

	   
	  
	  

	  this.tableau.getColumn("Sup").setCellEditor(new DeleteButtonEditor(new JCheckBox()));
	  this.tableau.getColumn("Sup").setCellRenderer(new ButtonRenderer());
	  
	 

	  
	   
	  this.tableau.getColumn("Type").setCellEditor(new DefaultCellEditor(combo));
	  DefaultTableCellRenderer renderer =new DefaultTableCellRenderer();
      renderer.setToolTipText("Click pour choisir");
   
	  this.tableau.getColumn("Type").setCellRenderer(renderer);
	    
	    this.tableau.setRowHeight(20);
	   
	    ListenerTableau lis1 = new ListenerTableau(this,panel,bd,creerQuestionnaireVue,modelques);
        tableau.getSelectionModel().addListSelectionListener(lis1);
        tableau.addFocusListener(lis1) ;
        
        ListenerCombobox lis=new ListenerCombobox(this,creerQuestionnaireVue,this.modelques) ;               		
        this.combo.addActionListener(lis);	                  		
         this.combo.addFocusListener(lis); 
         
                        
           this.tableau.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);         
           
           this.tableau.getTableHeader().setReorderingAllowed(false);
           
           
           
           
           
           
           
	  
	  }
	  
	  
	

	public JTable getTableau() {
		return tableau;
	}

	public void setTableau(JTable tableau) {
		this.tableau = tableau;
	}

	public String[] getComboData() {
		return comboData;
	}

	public void setComboData(String[] comboData) {
		this.comboData = comboData;
	}

	public JComboBox getCombo() {
		return combo;
	}

	public void setCombo(JComboBox combo) {
		this.combo = combo;
	}

	public JButton getSupp() {
		return supp;
	}

	public void setSupp(JButton supp) {
		this.supp = supp;
	}

	public JButton getModif() {
		return modif;
	}

	public void setModif(JButton modif) {
		this.modif = modif;
	}

	public panelQuestion getPanel() {
		return panel;
	}

	public void setPanel(panelQuestion panel) {
		this.panel = panel;
	}

	public ZModel getModel() {
		return model;
	}

	public void setModel(ZModel model) {
		this.model = model;
	}

	public AccesBD getBd() {
		return bd;
	}

	public void setBd(AccesBD bd) {
		this.bd = bd;
	}
}