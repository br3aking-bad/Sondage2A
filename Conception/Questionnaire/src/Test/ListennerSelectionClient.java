package Test;

import java.util.Collections;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class ListennerSelectionClient implements ListSelectionListener  {

	public Application application;


	ListennerSelectionClient(Application application){
		this.application=application;

	}

	public void valueChanged(ListSelectionEvent arg0) {
		int i = application.vuePrinc.list_Client.getSelectedIndex();
		application.vuePrinc.selected=i;
		this.application.vuePrinc.lQl.clear();
		
		Client c=application.vuePrinc.lCl.get(i);
		
		List<Questionnaire> ques=application.a.getQuestionnaires(c);
		
		
		for (Questionnaire q : ques){
			if(q.getClient().getRaisonSoc().equals(c.getRaisonSoc())){
				if(q.getEtat().equals('E') || q.getEtat().equals('C')){
					this.application.vuePrinc.lQl.addElement(q);
				}
			}
		}
		application.vuePrinc.selected=i;
		application.repaint();
		
		
		
	}
}
