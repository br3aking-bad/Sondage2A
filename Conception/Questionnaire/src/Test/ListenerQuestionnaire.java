package Test;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;


public class ListenerQuestionnaire implements ActionListener {

	static final int SAVE=0;
	static final int CANCEL=1;
	static final int VALIDE=2;
	static final int TEXTE=4;
	private int idBouton;
	private CreerQuestionnaireVue creerQuestionnaireVue;
	private ModelQuestionnaire modelques;
	private AccesBD bd;
	private Questionnaire questionnaire;
	private Application ap;
	
	
	public ListenerQuestionnaire(String nomBouton, CreerQuestionnaireVue creerQuestionnaireVue, ModelQuestionnaire modelques, AccesBD bd, Questionnaire questionnaire, Application ap) {
		super();
		this.bd=bd;
		this.creerQuestionnaireVue=creerQuestionnaireVue;
		this.modelques=modelques;
		this.questionnaire=questionnaire;
		this.ap=ap;
	
		switch (nomBouton){
		case "Save": this.idBouton=SAVE;break;
		case "Cancel": this.idBouton=CANCEL;break;
		case "Valide": this.idBouton=VALIDE;break;
		case "Texte": this.idBouton=TEXTE;break;
		default: this.idBouton=-1;
	
		}
	}

		


	
	
	// méthodes utilitaires pour les boutons
	private void Save(){
		
		
			
		
		if(this.creerQuestionnaireVue.getTxtTitre().getText().isEmpty()){
			
			this.creerQuestionnaireVue.getTxtTitre().setBorder(BorderFactory.createLineBorder(Color.RED));
			

		}else{
			this.questionnaire.setTitre(this.creerQuestionnaireVue.getTxtTitre().getText());
			
			this.bd.updateQuestionnaire(this.questionnaire);
			bd.viderQuestions(this.questionnaire.getIdQ());
		this.creerQuestionnaireVue.getTxtTitre().setBorder(BorderFactory.createLineBorder(Color.green));
		

			
			List<Question> listQ=bd.getQuestions(this.questionnaire);
			
			int cpt=bd.getMaxQuestion();
			int ind;
			cpt++;
			
			
			for(int i=0;i<this.modelques.getListeQuestion().size();i++){
				Question qe=this.type(this.modelques.getListeQuestion().get(i));
				bd.insertQuestion(qe);
				
				if(this.modelques.getListeVal().get(this.modelques.getListeQuestion().get(i).getNumQ()).size()>0){
					
					ind=1;
					for (ValPossible val:this.modelques.getListeVal().get(this.modelques.getListeQuestion().get(i).getNumQ())){
						
						ValPossible v=val;
						v.setIdV(ind);
						
						bd.insertValPossible(v);
						ind++;
					}
				}
				
				
				cpt++;
			}
			this.ap.changerVue(this.ap.vuePrinc, 880,401);
		JOptionPane.showMessageDialog(null, "Le questionnaire est bien enregistrer");
		
		}
	
		
	}
	
	public Question type(Question q){
		Question nvl=q;
		if(q.getTypeQuestion().getIntituleT().equals("Choix unique")){
			nvl.getTypeQuestion().setIdT('u');
			nvl.getTypeQuestion().setTypeReponse("Entier");
			nvl.setMaxVal(1);
		}
		else if(q.getTypeQuestion().getIntituleT().equals("Choix multiple")){
			nvl.getTypeQuestion().setIdT('m');
			nvl.getTypeQuestion().setTypeReponse("Caractères");
			if(nvl.getMaxVal()==1){
				nvl.setMaxVal(2);
			}
		}
		else if(q.getTypeQuestion().getIntituleT().equals("Classement")){
			nvl.getTypeQuestion().setIdT('c');
			nvl.getTypeQuestion().setTypeReponse("Caractères");
		}
	else if(q.getTypeQuestion().getIntituleT().equals("Note")){
		nvl.getTypeQuestion().setIdT('n');
		nvl.getTypeQuestion().setTypeReponse("Entier");
}
	else if(q.getTypeQuestion().getIntituleT().equals("Réponse libre")){
		nvl.getTypeQuestion().setIdT('l');
		nvl.getTypeQuestion().setTypeReponse("Caractères");
		nvl.setMaxVal(0);
}
		return nvl;
	}
	
	
	private void Cancel(){
		Object[] options = {"Oui, continuer",
                "Non, merci" };

	int n = JOptionPane.showOptionDialog(null,
			"si vous continuez vous perdrez toutes vos  modification",
		    "Information",
		    JOptionPane.YES_NO_CANCEL_OPTION,
		    JOptionPane.QUESTION_MESSAGE,
		    null,
		    options,
		    options[1]);
			
	if(n==0){
		this.ap.changerVue(this.ap.vuePrinc, 880,401);
	}

	}
	private void Valide(){
		
if(this.creerQuestionnaireVue.getTxtTitre().getText().isEmpty()){
			
			this.creerQuestionnaireVue.getTxtTitre().setBorder(BorderFactory.createLineBorder(Color.RED));
			

		}else{
			this.questionnaire.setTitre(this.creerQuestionnaireVue.getTxtTitre().getText());
			
			this.bd.updateQuestionnaire(this.questionnaire);
			bd.viderQuestions(this.questionnaire.getIdQ());
		this.creerQuestionnaireVue.getTxtTitre().setBorder(BorderFactory.createLineBorder(Color.green));
		

			
			List<Question> listQ=bd.getQuestions(this.questionnaire);
			
			int cpt=bd.getMaxQuestion();
			int ind;
			cpt++;
			
			
			for(int i=0;i<this.modelques.getListeQuestion().size();i++){
				Question qe=this.type(this.modelques.getListeQuestion().get(i));
				bd.insertQuestion(qe);
				
				if(this.modelques.getListeVal().get(this.modelques.getListeQuestion().get(i).getNumQ()).size()>0){
					
					ind=1;
					for (ValPossible val:this.modelques.getListeVal().get(this.modelques.getListeQuestion().get(i).getNumQ())){
						
						ValPossible v=val;
						v.setIdV(ind);
						
						bd.insertValPossible(v);
						ind++;
					}
				}
				
				
				cpt++;
				
			
			}
			
			this.creerQuestionnaireVue.activeDesactiveHaut(false);
			this.creerQuestionnaireVue.activeDesactiveBas(true);
		}
	
		
		

			
	}

	
	
	// événement de type actionPerformed sur les boutons
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		switch (idBouton){
		case SAVE: Save();break;
		case CANCEL: Cancel();break;
		case VALIDE: Valide();break;
		}
	}

	

}
