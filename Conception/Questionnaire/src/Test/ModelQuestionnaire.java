package Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class ModelQuestionnaire {
	private ArrayList<Question> listeQuestion;
	private Map<Integer, List<ValPossible>> listeVal;
	private AccesBD bd;
	private Questionnaire ques;
	
	
	public ModelQuestionnaire(AccesBD bd, Questionnaire ques){
		
		listeVal=new HashMap<Integer, List<ValPossible>>();
		
		listeQuestion=(ArrayList<Question>) bd.getQuestions(ques);
		this.ques=ques;
		
		for(Question q:listeQuestion){
			
			listeVal.put(q.getNumQ(), bd.getValPossibles(q));
		
		}

	}
	public int maxv(){
		int max=0;
		for (Integer q:listeVal.keySet()){
			if(max<q){
				max=q;
			}
		}
		return max;
	}

	
	
	public void Affiche1(){
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("---------------------------------------------------");
		for (Integer q:listeVal.keySet()){
			System.out.println(q+" ====>  "+listeVal.get(q));
		}
	}
	
	public void Affiche2(){
		for (Question q:listeQuestion){
			System.out.println(q.getNumQ()+"    "+q.getTypeQuestion().getIntituleT());
		}
	}
	
	public void  remove(int q){
		this.listeVal.remove(q);
	}

	public ArrayList<Question> getListeQuestion() {
		return listeQuestion;
	}


	public void setListeQuestion(ArrayList<Question> listeQuestion) {
		this.listeQuestion = listeQuestion;
	}





	public AccesBD getBd() {
		return bd;
	}


	public void setBd(AccesBD bd) {
		this.bd = bd;
	}


	public Questionnaire getQues() {
		return ques;
	}


	public void setQues(Questionnaire ques) {
		this.ques = ques;
	}
	public Map<Integer, List<ValPossible>> getListeVal() {
		return listeVal;
	}
	public void setListeVal(Map<Integer, List<ValPossible>> listeVal) {
		this.listeVal = listeVal;
	}

}
