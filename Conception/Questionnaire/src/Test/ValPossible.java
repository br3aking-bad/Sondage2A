package Test;
public class ValPossible {

	private Question question;
	private int idV;
	private String valeur;
	
	/**
	 * @param question
	 * @param idV
	 * @param valeur
	 */
	public ValPossible(Question question,int idV,String valeur)
	{
		this.question=question;
		this.idV=idV;
		this.valeur=valeur;
	}
	public ValPossible(Question question,String valeur)
	{
		this.question=question;
		this.idV=-1;
		this.valeur=valeur;
	}

	/**
	 * @return
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/**
	 * @return
	 */
	public int getIdV() {
		return idV;
	}

	/**
	 * @param idV
	 */
	public void setIdV(int idV) {
		this.idV = idV;
	}

	/**
	 * @return
	 */
	public String getValeur() {
		return valeur;
	}

	/**
	 * @param valeur
	 */
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "       " + valeur ;
	}
	
	

}
