package Test;

import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;



public class CreerQuestionnaireVue extends JPanel{
	private  AccesBD bd; // modèle du formulaire
	private  JTextField txtTitre; // titre du questionnaire
	private  JTable table;  // le tableau des questions
	private  panelQuestion typeQuestion;
	private JButton btnadd;
	private Questionnaire questionnaire;
	private ModelQuestionnaire modelques;
	public JPanel panel_tableau;
	private Application ap;
	public  Tableau t;
	public JButton btnValider ;
	public JButton btnSuspendre;
	public JButton btnannuler;
	public JPanel panel;
	public DefaultListModel<Panel> model_Panel;
	public JList<Panel> list_panel;

	
	public CreerQuestionnaireVue(AccesBD bd,Questionnaire ques,Application ap){
		this.ap=ap;
		this.bd = bd; // modèle du formulaire
		// Le header de la fenetre
		ModelQuestionnaire modelques=new ModelQuestionnaire(this.bd,ques);
		//super("RapidSond - Conception |Creation - Questionnaire |Login : ELghonnaji");
		this.modelques=modelques;
		this.questionnaire=ques;
		
		// la taille de la fenetre
		//this.setBounds(70, 50, 1150, 616);
		
		// Le couleur de la fenetre
		this.setBackground(new Color(211, 211, 211));
		
	
		
	
		
		
		//desactiver le layout
		this.setLayout(null);
		
		
		
		
		
		
		
		
		
		
		
		
		// Panel pour le bas de la page
				// ajouter un panel
				 panel = new JPanel();
				
		
				
				// le border du panel
				panel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
				//La taille du panel
				panel.setBounds(0, 433, 1150, 145);
				//ajouter le panel dans la fenetre
				this.add(panel);
				//desactiver le layout
				panel.setLayout(null);
				
				
				
				
				// un scollpane pour liste des panels 
				JScrollPane scrollPane2 = new JScrollPane();
				scrollPane2.setBounds(291, 29, 295, 100);
				panel.add(scrollPane2);
				
				
				model_Panel=new DefaultListModel<Panel>() ;
				for (Panel p : bd.getPanels()){
					model_Panel.addElement(p);
				}

				
				 list_panel = new JList<Panel>(model_Panel);
				
				
				list_panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
				scrollPane2.setViewportView(list_panel);
				
				
				
				
				
				
				// creer Le header ou le titre de la liste Panels
					JLabel labelPanel = new JLabel("Liste des Panels ");
					scrollPane2.setColumnHeaderView(labelPanel);
					labelPanel.setFont(new Font("DejaVu Serif Condensed", Font.PLAIN, 17));
					labelPanel.setBackground(Color.BLACK);
		
		
		
		
		
		// ajouter Les 2 boutons (Terminer ou Annuler)
		
					
					
					JButton btnTerminer = new JButton("Terminer");
					btnTerminer.setIcon(new ImageIcon("Ok-icon.png"));
					btnTerminer.setBounds(680, 31, 161, 39);
					panel.add(btnTerminer);
					
					JButton btnAnnuler = new JButton("Annuler");
					btnAnnuler.setIcon(new ImageIcon("Button-Delete-icon.png"));
					btnAnnuler.setBounds(680, 88, 161, 39);
					panel.add(btnAnnuler);
		
				btnTerminer.addActionListener(new ControleurValidationQuest("Terminer",this,this.bd,this.questionnaire,this.ap));
				btnAnnuler.addActionListener(new ControleurValidationQuest("Annuler",this,this.bd,this.questionnaire,this.ap));
				// faut desactiver tous les composents dans le panel en bas 
					
					for (int i=0;i<panel.getComponentCount();i++){
						panel.getComponent(i).setEnabled(false);
	
					}
						
						
					
						
						
						
		
		
		// un panel pour le titre de questionnaire 
		
				// creer le panel
				JPanel panel_titre = new JPanel();
				
				// la couleur du panel
				panel_titre.setBackground(new Color(211, 211, 211));
				
				// Le border du panel
				panel_titre.setBorder(new TitledBorder(new CompoundBorder(new BevelBorder(BevelBorder.LOWERED, new Color(240, 240, 240), new Color(255, 255, 255), new Color(105, 105, 105), new Color(105, 105, 105)), new LineBorder(new Color(180, 180, 180))), "Titre du Questionnaire", TitledBorder.CENTER, TitledBorder.ABOVE_TOP, null, null));
				
				//la taille du panel
				panel_titre.setBounds(750, 11, 325, 64);
				
				// ajouter le panel dans la fenetre
				this.add(panel_titre);
				
				// desactiver le layout par defaut
				panel_titre.setLayout(null);
				
				
				//ajouter un JText Field ou l'utilisateur va entrer le titre 
				Font font1 = new Font("SansSerif", Font.BOLD, 20);
				txtTitre = new JTextField();
				txtTitre.setText(this.questionnaire.getTitre()+"");
				txtTitre.setHorizontalAlignment(JTextField.CENTER);
				txtTitre.setFont(font1);
				
				// la taille du Jtexfield 
				txtTitre.setBounds(10, 21, 305, 32);
				// l'ajouter dans le panel
				panel_titre.add(txtTitre);
				
				//quand le curseur de la souris se met sur le textField ça met une ecriture qui importe une information importante
				txtTitre.setToolTipText("Titre du questionnaire ");
				// la taille du Jtextfield
				txtTitre.setColumns(20);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				// creer un panel ou on va metre les types de question
				
				
				
				 typeQuestion=new panelQuestion(bd,this.modelques);
				
				
				
				
				
				
				this.add(typeQuestion);
			
		
		
		
		// un panel pour ajouter un tableau
		
		
		
		 panel_tableau = new JPanel();
		panel_tableau.setLayout(null);
		
		panel_tableau.setBackground(new Color(211, 211, 211));
		
		panel_tableau.setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		panel_tableau.setBounds(10, 30, 650, 314);
		
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 30, 630, 250);
		panel_tableau .add(scrollPane);
		
		 t=new Tableau(this.bd,typeQuestion,this,this.modelques,this.questionnaire);
		table = t.getTableau();
		scrollPane.setViewportView(table);
		
		
		 btnadd = new JButton("Ajouter Question");
			btnadd.setBounds(200, 280, 220, 30);
			panel_tableau.add(btnadd);
			btnadd.setIcon(new ImageIcon("Actions-list-add-icon.png"));
		
			AjouterLigneListener lis=new AjouterLigneListener(t);
			btnadd.addActionListener(lis);
		
			
			
			typeQuestion.setTab(t);
			
			
			this.add(panel_tableau);
		
		
		
			
		
		
		
		
		
		
		
		
		
		
		// ajouter 2 boutons pour Terminer ou suspendre Questionnaire
		
			
		 btnValider = new JButton("Valider");
		 btnSuspendre= new JButton("Enregistrer");
		 btnannuler=new JButton("Annuler");
		
		btnValider.addActionListener(new ListenerQuestionnaire("Valide",this,this.modelques,this.bd,this.questionnaire,this.ap));
		btnannuler.addActionListener(new ListenerQuestionnaire("Cancel",this,this.modelques,this.bd,this.questionnaire,this.ap));
		btnSuspendre.addActionListener(new ListenerQuestionnaire("Save",this,this.modelques,this.bd,this.questionnaire,this.ap));
		
		btnannuler.setIcon(new ImageIcon("Actions-edit-delete-icon.png"));
		btnValider.setIcon(new ImageIcon("Ok-icon.png"));
		btnValider.setFont(new Font("Stencil Std", Font.PLAIN, 12));
		btnannuler.setFont(new Font("Stencil Std", Font.PLAIN, 12));
		btnValider.setBounds(50, 370, 165, 40);
		btnannuler.setBounds(450, 370, 165, 40);
		btnSuspendre.setIcon(new ImageIcon("Apps-session-suspend-icon.png"));
		btnSuspendre.setFont(new Font("Stencil Std", Font.PLAIN, 12));
		btnSuspendre.setBounds(250, 370, 165, 40);
		this.add(btnValider);
		this.add(btnSuspendre);
		this.add(btnannuler);
		
	}
	
	
	
	public void misajourpanel(panelQuestion p){
		this.typeQuestion.removeAll();
		this.add(p);
		this.revalidate();
		this.repaint();
	}

	public AccesBD getBd() {
		return bd;
	}

	public void setBd(AccesBD bd) {
		this.bd = bd;
	}

	public JTextField getTxtTitre() {
		return txtTitre;
	}

	public void setTxtTitre(JTextField txtTitre) {
		this.txtTitre = txtTitre;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public panelQuestion getTypeQuestion() {
		return typeQuestion;
	}

	public void setTypeQuestion(panelQuestion typeQuestion) {
		this.typeQuestion = typeQuestion;
	}
	
	public void activeDesactiveHaut(boolean etat){
		
		if(this.getTypeQuestion().getComponents().length==3){
			this.getTypeQuestion().getComponent(2).setVisible(etat);
			this.revalidate();
			this.repaint();
			
		}
		
		this.getTxtTitre().setEnabled(etat);
		
		
		
		this.t.getTableau().setEnabled(etat);
		this.panel_tableau.getComponent(1).setEnabled(etat);
		
		this.btnannuler.setEnabled(etat);
		this.btnSuspendre.setEnabled(etat);
		this.btnValider.setEnabled(etat);
	}
	public void activeDesactiveBas(boolean etat){
		
		for (int i=0;i<this.panel.getComponentCount();i++){
			this.panel.getComponent(i).setEnabled(etat);

		}
		
		
	}

}
