package Test;
public class Caracteristique {

	private String idC;
	private Character sexe;
	private Tranche tranche;
	private Categorie categorie;
	
	/**
	 * @param idC
	 * @param sexe
	 * @param tranche
	 * @param categorie
	 */
	public Caracteristique(String idC,Character sexe,Tranche tranche,Categorie categorie)
	{
		this.idC=idC;
		this.sexe=sexe;
		this.tranche=tranche;
		this.categorie=categorie;
	}

	/**
	 * @return
	 */
	public String getIdC() {
		return idC;
	}

	/**
	 * @param idC
	 */
	public void setIdC(String idC) {
		this.idC = idC;
	}

	/**
	 * @return
	 */
	public Character getSexe() {
		return sexe;
	}

	/**
	 * @param sexe
	 */
	public void setSexe(Character sexe) {
		this.sexe = sexe;
	}

	/**
	 * @return
	 */
	public Tranche getTranche() {
		return tranche;
	}

	/**
	 * @param tranche
	 */
	public void setTranche(Tranche tranche) {
		this.tranche = tranche;
	}

	/**
	 * @return
	 */
	public Categorie getCategorie() {
		return categorie;
	}

	/**
	 * @param categorie
	 */
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Caracteristique [idC=" + idC + ", sexe=" + sexe + ", tranche="
				+ tranche + ", categorie=" + categorie + "]";
	}
	
}
