package Test;
public class Tranche {

	private Character idTr;
	private int valDebut;
	private int valFin;
	
	/**
	 * @param idTr
	 * @param valDebut
	 * @param valFin
	 */
	public Tranche(Character idTr,int valDebut,int valFin)
	{
		this.idTr=idTr;
		this.valDebut=valDebut;
		this.valFin=valFin;
	}

	/**
	 * @return
	 */
	public Character getIdTr() {
		return idTr;
	}

	/**
	 * @param idTr
	 */
	public void setIdTr(Character idTr) {
		this.idTr = idTr;
	}

	/**
	 * @return
	 */
	public int getValDebut() {
		return valDebut;
	}

	/**
	 * @param valDebut
	 */
	public void setValDebut(int valDebut) {
		this.valDebut = valDebut;
	}

	/**
	 * @return
	 */
	public int getValFin() {
		return valFin;
	}

	/**
	 * @param valFin
	 */
	public void setValFin(int valFin) {
		this.valFin = valFin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tranche [idTr=" + idTr + ", valDebut=" + valDebut + ", valFin="
				+ valFin + "]";
	}
	
	
	
}
