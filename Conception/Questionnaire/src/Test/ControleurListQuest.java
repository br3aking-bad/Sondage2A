package Test;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.ListModel;


public class ControleurListQuest implements ActionListener, FocusListener{
	static final int SUPPRIMER=1;
	static final int MODIFIER=2;
	
	int idBouton;
	Application application;
	AcceuilVue vue;
	
	
	public ControleurListQuest(int idBouton, Application application2, AcceuilVue vue) {
		super();
		this.idBouton = idBouton;
		this.application = application2;
		this.vue=vue;
	}
	
	ControleurListQuest(String nomBouton, AcceuilVue vue, Application application){
		super();
		this.vue=vue;
		this.application = application;
		switch (nomBouton){
		case "Supprimer": this.idBouton=SUPPRIMER;break;
		case "Modifier": this.idBouton=MODIFIER;break;
		default: this.idBouton=-1;
		}
		
	}
	
	private void supprimer(){

		try{
			int indSelectionCl=vue.list_Client.getSelectedIndex();
			int indSelectionQu=vue.list_Ques.getSelectedIndex();
			if(indSelectionQu==-1){
				Object[] options = {"Oui",
				"Non" };
				int n = JOptionPane.showOptionDialog(null,
						"Voulez vous supprimer le Client avec tous ses questionnaires ?",
						"Information",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[1]);
				

				if(n==0){
					
					if (indSelectionCl!=-1){
						boolean saute=true;
							for (Questionnaire q:application.a.getQuestionnaires(this.vue.lCl.get(indSelectionCl))){
								if(q.getEtat().equals('S') || q.getEtat().equals('P')){
									saute=false;
								}
							}
							if(saute){
								System.out.println(this.vue.lCl.get(indSelectionCl).getNumC());
								application.a.deleteClient(this.vue.lCl.get(indSelectionCl).getNumC());
								vue.lCl.remove(indSelectionCl);
								vue.list_Client.repaint();
								indSelectionCl=-1;
							
								this.vue.quesDeClient.setVisible(true);
								this.application.repaint();
							}
							else{
								JOptionPane.showMessageDialog(null, "Le Client contient des Questionnaire Qui dont dans le module sondage");
							}
					}

				}

				else{//n!=0
					this.application.changerVue(this.application.vuePrinc, 880,401);
				}
			}
			else{//indSelectionQu!=-1
				Object[] options = {"Oui",
				"Non" };
				int n = JOptionPane.showOptionDialog(null,
						"Voulez vous supprimer le questionnaire selectionné ?",
						"Information",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[1]);

				if(n==0){
					application.a.deleteQuestionnaire(this.vue.lQl.get(indSelectionQu).getIdQ());
					vue.lQl.remove(indSelectionCl);
					vue.repaint();
					indSelectionQu=-1;
				}
			}
		}
		catch(Exception e){

		}
		vue.list_Client.repaint();
	}
	
	private void modifier(){
		int indSelectionQ=vue.list_Ques.getSelectedIndex();
		int indSelectionC=vue.list_Client.getSelectedIndex();
		if (indSelectionQ!=-1){
			Questionnaire ques=vue.list_Ques.getSelectedValue();
			
			CreerQuestionnaireVue q=new CreerQuestionnaireVue(application.a, ques,this.application);
			application.changerVue(q, 1150, 616);
		}
		else{
			if(indSelectionC!=-1){
				this.application.vuePrinc.etat="Modifier";
				ClientVue vue = new ClientVue(this.vue.application);
				vue.raison.setText(this.vue.lCl.get(indSelectionC).getRaisonSoc());
				vue.adrs1.setText(this.vue.lCl.get(indSelectionC).getAdresse1());
				vue.adrs2.setText(this.vue.lCl.get(indSelectionC).getAdresse2());
				vue.code.setText(this.vue.lCl.get(indSelectionC).getCodePostal()+"");
				vue.mail.setText(this.vue.lCl.get(indSelectionC).getEmail());
				vue.num.setText(this.vue.lCl.get(indSelectionC).getNumC()+"");
				vue.tel.setText(this.vue.lCl.get(indSelectionC).getTelephone());
				vue.ville.setText(this.vue.lCl.get(indSelectionC).getVille());
				application.changerVue(vue, 480, 580);
			}
		}
	}

	public void actionPerformed(ActionEvent arg0) {
		switch (idBouton){
		case SUPPRIMER: supprimer();break;
		case MODIFIER: modifier();break;
		}
		
	}

	public void focusGained(FocusEvent arg0){
		vue.list_Client.setSelectedIndex(-1);
		this.vue.quesDeClient.setVisible(false);
	}
	
	public void focusLost(FocusEvent arg0){
	

	}
}
