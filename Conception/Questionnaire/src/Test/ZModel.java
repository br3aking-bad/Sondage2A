package Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;


class ZModel extends AbstractTableModel{
	  
	   
	   public Object[][] data;
	   public AccesBD bd;
	   private String[] title;
	    private JTable tabl;
	    private String[] comboData={"Choix Simple","Choix Multiple","Choix Classement", "Note", "Libre"};
	    private ModelQuestionnaire modelques;
	    private Questionnaire questionnaire;
	    
	    
	    
	   //Constructeur
	   public ZModel(AccesBD q, String[] title,JTable t, String[] comboData, ModelQuestionnaire modelques, Questionnaire questionnaire){
	      this.bd=q;
	      this.title = title;
	      tabl=t;
	      this.modelques=modelques;
	      this.questionnaire=questionnaire;
	      

	      this.misajour();
	    
	      
	   }
	   
	
		   
		   
		 
	   
	   
	   public void misajour(){
		   List<Question> ques =this.modelques.getListeQuestion();
		   data=new Object[ques.size()][5];
		   int cpt=1;
		   for (int i=0;i<ques.size();i++){
			   
			   	data[i][0]="Question : "+cpt;
			   	data[i][1]=ques.get(i).getTexteQ();
			   	data[i][2]=ques.get(i).getTypeQuestion().getIntituleT();
			    data[i][3]=new Boolean(false);
			   	data[i][4]=new JButton(); 
			   	cpt++;
		    	 }
		   

		   
		    	 
	   }
	    
	   //Retourne le titre de la colonne à l'indice spécifié
	   public String getColumnName(int col) {
		   
	     return this.title[col];
	   }
	 
	   //Retourne le nombre de colonnes
	   public int getColumnCount() {
	      return this.title.length;
	   }
	    
	   //Retourne le nombre de lignes
	   public int getRowCount() {
	      return this.data.length;
	   }
	    
	   //Retourne la valeur à l'emplacement spécifié
	   public Object getValueAt(int row, int col) {
	      return this.data[row][col];
	   }
	    
	   //Définit la valeur à l'emplacement spécifié
	   public void setValueAt(Object value, int row, int col) {
	      //On interdit la modification sur certaines colonnes !
		  
		   		if(col==1 || col==2   ){	
		   			this.data[row][col] = value;
		   		}
	   }
	          
	  //Retourne la classe de la donnée de la colonne
	   public Class getColumnClass(int col){
	      //On retourne le type de la cellule à la colonne demandée
	      //On se moque de la ligne puisque les données sont les mêmes
	      //On choisit donc la première ligne
	      return this.data[0][col].getClass();
	   }
	 
	   //Méthode permettant de retirer une ligne du tableau
	   public void removeRow(int position){
		   
		   Question q=this.modelques.getListeQuestion().get(position);
		   this.modelques.getListeQuestion().remove(position);
		   this.modelques.remove(q.getNumQ());
		   this.misajour();
		   this.fireTableDataChanged();
	   }
	    
	   //Permet d'ajouter une ligne dans le tableau
	   public void addRow(){
	      
		   
			   Question q=new Question(this.questionnaire,"",1, new TypeQuestion('u', "Choix unique","Entier"));
			   q.setNumQ(this.modelques.maxv()+1);
			   this.modelques.getListeQuestion().add(q);
			   List<ValPossible> listval=new ArrayList<ValPossible>();
			   this.modelques.getListeVal().put(this.modelques.maxv()+1,listval);
			   this.misajour();
			   
			   this.fireTableDataChanged();
			   
		   
		   
		   
		   
		   
		   
	      //Cette méthode permet d'avertir le tableau que les données
	      //ont été modifiées, ce qui permet une mise à jour complète du tableau
	      this.fireTableDataChanged();
	      
	      
	      
	      
	   }
	    
	   public boolean est_Vide(){
		   return this.getRowCount()==0;
	   }
	   
	    
	   public boolean isCellEditable(int row, int col){
		   if(this.getColumnName(col).equals("ID") || this.getColumnName(col).equals("modif ?")){
			   return false;
		   }
		  
		  
	      return true;
	   }







	public Object[][] getData() {
		return data;
	}







	public void setData(Object[][] data) {
		this.data = data;
	}
	}