package Test;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.SwingConstants;
import javax.swing.JToggleButton;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

public class Note extends JPanel {
	private JTextField textField;

	/**
	 * Create the panel.
	 */
	public Note() {
		this.setBackground(new Color(211, 211, 211));
		this.setToolTipText("");
		this.setBorder(new TitledBorder(new MatteBorder(3, 2, 1, 2, (Color) new Color(128, 128, 128)), "Type question : Choix Note", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		this.setBounds(630, 100, 286, 113);
		this.setLayout(null);
		setLayout(null);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(97, 38, 166, 20);
		add(textField);
		
		JLabel intitule = new JLabel("Intituler");
		intitule.setFont(new Font("Segoe Script", Font.BOLD, 16));
		intitule.setBounds(10, 38, 77, 18);
		add(intitule);
		
		JLabel lblNoteMax = new JLabel("Note Max");
		lblNoteMax.setFont(new Font("Segoe Script", Font.BOLD, 16));
		lblNoteMax.setBounds(10, 75, 98, 18);
		add(lblNoteMax);
		
		JSpinner noteMax = new JSpinner();
		noteMax.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		noteMax.setToolTipText("Nombre de choix possible");
		noteMax.setBounds(107, 75, 49, 20);
		add(noteMax);
			
	}
}
