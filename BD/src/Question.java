
public class Question {

	int numQ;
	String texteQ;
	int MaxVal;
	Questionnaire q;
	TypeQuestion type;
	
	public Question(int numQ, String texteQ, int maxVal,Questionnaire q,TypeQuestion t) {
		super();
		this.numQ = numQ;
		this.texteQ = texteQ;
		MaxVal = maxVal;
		this.q=q;
		type=t;
		
	}

	public int getNumQ() {
		return numQ;
	}

	public void setNumQ(int numQ) {
		this.numQ = numQ;
	}

	public String getTexteQ() {
		return texteQ;
	}

	public void setTexteQ(String texteQ) {
		this.texteQ = texteQ;
	}

	public int getMaxVal() {
		return MaxVal;
	}

	public void setMaxVal(int maxVal) {
		MaxVal = maxVal;
	}

	@Override
	public String toString() {
		return "Question [numQ=" + numQ + ", texteQ=" + texteQ + ", MaxVal="
				+ MaxVal + "]";
	}

}
