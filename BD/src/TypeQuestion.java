
public class TypeQuestion {
	

	int idT;
	String  intituleT;
	String typeQuestion;
	
	
	public TypeQuestion(int idT, String intituleT, String typeQuestion) {
		super();
		this.idT = idT;
		this.intituleT = intituleT;
		this.typeQuestion = typeQuestion;
	}
	
	public int getIdT() {
		return idT;
	}
	public void setIdT(int idT) {
		this.idT = idT;
	}
	public String getIntituleT() {
		return intituleT;
	}
	public void setIntituleT(String intituleT) {
		this.intituleT = intituleT;
	}
	public String getTypeQuestion() {
		return typeQuestion;
	}
	public void setTypeQuestion(String typeQuestion) {
		this.typeQuestion = typeQuestion;
	}

	@Override
	public String toString() {
		return "TypeQuestion [idT=" + idT + ", intituleT=" + intituleT
				+ ", typeQuestion=" + typeQuestion + "]";
	}
	
	
	
	

}
