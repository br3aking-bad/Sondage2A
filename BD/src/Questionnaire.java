import java.sql.Connection;


public class Questionnaire {
	

	int idQ;
	String Titre;
	String Etat;
	Client client;
	Connection mysql;
	public Questionnaire(int idQ, String titre, String etat,Client c) {
		super();
		this.idQ = idQ;
		Titre = titre;
		Etat = etat;
		client=c;
	}
	
	public Questionnaire( String titre, String etat) {
		super();
		
		Titre = titre;
		Etat = etat;
	}
	public int getIdQ() {
		return idQ;
	}
	public void setIdQ(int idQ) {
		this.idQ = idQ;
	}
	public String getTitre() {
		return Titre;
	}
	public void setTitre(String titre) {
		Titre = titre;
	}
	public String getEtat() {
		return Etat;
	}
	public void setEtat(String etat) {
		Etat = etat;
	}

	@Override
	public String toString() {
		return "Questionnaire [idQ=" + idQ + ", Titre=" + Titre + ", Etat="
				+ Etat + ", mysql=" + mysql + "]";
	}

}
