
public class Commander {
	
	
	public Commander(int numC, int idQ) {
		super();
		this.numC = numC;
		this.idQ = idQ;
	}
	int numC;
	int idQ;
	public int getNumC() {
		return numC;
	}
	public void setNumC(int numC) {
		this.numC = numC;
	}
	public int getIdQ() {
		return idQ;
	}
	public void setIdQ(int idQ) {
		this.idQ = idQ;
	}
	@Override
	public String toString() {
		return "Commander [numC=" + numC + ", idQ=" + idQ + "]";
	}

}
